/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Kartenspiel
 * <p>
 * Das Kartenspiel ist eine simple, prototypenhafte Implementierung eines
 * Pokerspiels, wobei lediglich das ausgeben und aufdecken der Karten
 * implementiert sind. Das Kartenspiel selbst teilt sich in drei Pakete auf.
 * {@code .karten} welche alle Klassen fuer das Abbilden von Spielkarten
 * enthaelt. {@code .spiel} welche alle Klassen fuer das Abbilden der
 * Spielelogik selbst enthaelt. Im Hauptpaket befindet sich der
 * Haupteinstiegspunkt und der Ablauf des Spiels selbst.
 * <p>
 * Die Karten und die Spieler werden dabei aus einfachen Konfigurationsdateien
 * gelesen.
 * 
 * @see <a href=
 *      "https://www.java-forum.org/thema/einfaches-kartenspiel-spieler-klasse.177323/">Java-Forum.org:
 *      Einfaches Kartenspiel (Spieler Klasse)</a>
 */

package org.bonsaimind.javaforumorgbeispiele.kartenspiel;