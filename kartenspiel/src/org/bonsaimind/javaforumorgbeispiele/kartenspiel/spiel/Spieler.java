/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.kartenspiel.spiel;

import org.bonsaimind.javaforumorgbeispiele.kartenspiel.karten.Kartenstapel;

/**
 * Der {@link Spieler} ist eine simple Klasse welche einen Spieler im Spiel
 * darstellt.
 */
public class Spieler {
	/** Der {@link Kartenstapel} fuer diesen Spieler. */
	protected Kartenstapel kartenstapel = new Kartenstapel();
	/** Der Name von diesem Spieler. */
	protected String name = null;
	
	/**
	 * Erstellt eine neue {@link Spieler} Instanz.
	 * 
	 * @param name Der Name fuer diesen {@link Spieler}.
	 */
	public Spieler(String name) {
		super();
		
		this.name = name;
	}
	
	/**
	 * Der {@link Kartenstapel} fuer die Hand dieses {@link Spieler}s.
	 * 
	 * @return Der {@link Kartenstapel} fuer die Hand dieses {@link Spieler}s.
	 */
	public Kartenstapel getKartenstapel() {
		return kartenstapel;
	}
	
	/**
	 * Der Name.
	 * 
	 * @return Der Name.
	 */
	public String getName() {
		return name;
	}
}
