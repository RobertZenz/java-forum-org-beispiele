/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.kartenspiel.spiel;

import java.util.List;

import org.bonsaimind.javaforumorgbeispiele.kartenspiel.karten.Kartenstapel;

/**
 * Das {@link Spiel} enthaelt die Hauptlogik fuer den Ablauf des Spiels.
 * <p>
 * <i>Hinweis:</i> Diese Klasse selbst setzt eine bestimmte Abfolge an Aufrufen
 * voraus damit sie korrekt funtioniert. In der Praxis sollte man dies vermeiden
 * da ein Aufrufer sich dieser Reihenfolge eventuell nicht bewusst ist, und auch
 * einfach Fehler passieren koennen so dass diese in falscher Reihenfolge
 * aufgerufen werden. Zumindest sollte man entsprechende Pruefungen und
 * Fehlerverhalten imlementieren um solche falsche Verwendung und Fehler
 * abzufangen und zu verhindern. Idealerweise restruktiert man die Klasse so
 * dass sie nicht falsch aufgerufen werden kann, dies geht aber meist mit
 * weitaus mehr Code einher. Zum Beispiel in dem man eigene Klassen oder
 * Schnittstellen fuer jeden einzelnen Schritt zur verfuegung stellt, und jeder
 * Schritt die Klasse/Schnittstelle fuer den naechsten retourniert.
 */
public class Spiel {
	/** Der {@link Kartenstapel} fuer den Dealer. */
	protected Kartenstapel dealerKartenstapel = null;
	/** Der {@link Kartenstapel} der offenen Karten welche am Tisch liegen. */
	protected Kartenstapel offenerKartenstapel = new Kartenstapel();
	/** Die {@link Spieler} im Spiel. */
	protected List<Spieler> spieler = null;
	
	/**
	 * Erstellt eine neue {@link Spiel} Instanz.
	 */
	public Spiel() {
		super();
	}
	
	/**
	 * Der {@link Kartenstapel} an Karten welche offen auf dem Tisch liegen.
	 * <p>
	 * <i>Hinweis:</i> Der hier reoturnierte {@link Kartenstapel} koennte vom
	 * Aufrufer veraendert werden. Um dies zu verhindern gibt es zwei
	 * Moeglichkeiten, entweder man retourniert (jedes mal eine neue) Kopie des
	 * Stapels, oder man implementiert eine "AnsichtsKartenstapel" Klasse welche
	 * es zwar erlaubt den darunterliegenden {@link Kartenstapel} zu betrachten,
	 * aber alle Aenderungen ausschlieszt.
	 * 
	 * @return Der {@link Kartenstapel} an Karten welche offen auf dem Tisch
	 *         liegen.
	 */
	public Kartenstapel getOffenerKartenstapel() {
		return offenerKartenstapel;
	}
	
	/**
	 * Die {@link Spieler} welche am Spiel teilnehmen.
	 * <p>
	 * <i>Hinweis:</i> Wir retournieren hier die veraenderbare {@link List} an
	 * {@link Spieler}n welche wir intern verwenden. Das bedeutet ein Aufrufer
	 * koennte unseren internen Zustand veraendern in dem diese {@link List}
	 * veraendert wird. Entweder retourniert man hier eine Kopie, oder eine
	 * unveraenderbare Ansicht auf die interne {@link List}.
	 * 
	 * @return Die {@link Spieler} welche am Spiel teilnehmen.
	 */
	public List<Spieler> getSpieler() {
		return spieler;
	}
	
	/**
	 * Beginnt das Spiel mit dem gegebenen {@link Kartenstapel} fuer den Dealer
	 * und den gegebenen {@link Spieler}n.
	 * <p>
	 * <i>Hinweis:</i> Der {@link Kartenstapel} fuer den Dealer wird hier direkt
	 * uebernommen. Der Aufrufer koennte also unseren internen Zustand
	 * veraendern in dem er einen Verweis auf diesen {@link Kartenstapel}
	 * behaelt und diesen einfach veraendert. Um dies zu verhindern koennte man
	 * diesen {@link Kartenstapel} kopieren und eine eigene Instanz intern
	 * halten.
	 * <p>
	 * <i>Hinweis:</i> Die {@link List} an {@link Spieler}n wird einfach so von
	 * uns uebernommen. Der Aufrufer koennte also unseren internen Zustand
	 * veraendern in dem er einen Verweis auf diese {@link List} an
	 * {@link Spieler}n behaelt und diese einfach veraendert. Um dies zu
	 * verhindern koennte man diese {@link List} an {@link Spieler}n kopieren
	 * und eine eigene Instanz intern halten.
	 * 
	 * @param dealerKartenstapel Der {@link Kartenstapel} welchen der Dealer
	 *        verwenden soll.
	 * @param spieler Die {@link List} an {@link Spieler}n.
	 * @return Diese {@link Spiel} Instanz.
	 */
	public Spiel spielBeginnen(Kartenstapel dealerKartenstapel, List<Spieler> spieler) {
		this.dealerKartenstapel = dealerKartenstapel;
		this.spieler = spieler;
		
		// Ausgeben der initialen zwei Karten fuer jeden Spieler.
		for (Spieler einzelnerSpieler : spieler) {
			einzelnerSpieler.getKartenstapel()
					.karteHinzufuegen(dealerKartenstapel.obersteKarteNehmen())
					.karteHinzufuegen(dealerKartenstapel.obersteKarteNehmen());
		}
		
		return this;
	}
	
	/**
	 * Spielt die dritte Runde.
	 * 
	 * @return Diese {@link Spiel} Instanz.
	 */
	public Spiel spieleDritteRunde() {
		// Offen legen der naechsten Karte.
		offenerKartenstapel
				.karteHinzufuegen(dealerKartenstapel.obersteKarteNehmen());
		
		return this;
	}
	
	/**
	 * Spielt die erste Runde.
	 * 
	 * @return Diese {@link Spiel} Instanz.
	 */
	public Spiel spieleErsteRunde() {
		// Offen legen der naechsten drei Karten.
		offenerKartenstapel
				.karteHinzufuegen(dealerKartenstapel.obersteKarteNehmen())
				.karteHinzufuegen(dealerKartenstapel.obersteKarteNehmen())
				.karteHinzufuegen(dealerKartenstapel.obersteKarteNehmen());
		
		return this;
	}
	
	/**
	 * Spielt die zweite Runde.
	 * 
	 * @return Diese {@link Spiel} Instanz.
	 */
	public Spiel spieleZweiteRunde() {
		// Offen legen der naechsten Karte.
		offenerKartenstapel
				.karteHinzufuegen(dealerKartenstapel.obersteKarteNehmen());
		
		return this;
	}
}
