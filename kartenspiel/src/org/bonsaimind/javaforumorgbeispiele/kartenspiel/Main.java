/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.kartenspiel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.bonsaimind.javaforumorgbeispiele.kartenspiel.karten.Farbe;
import org.bonsaimind.javaforumorgbeispiele.kartenspiel.karten.Karte;
import org.bonsaimind.javaforumorgbeispiele.kartenspiel.karten.Kartenstapel;
import org.bonsaimind.javaforumorgbeispiele.kartenspiel.karten.Wert;
import org.bonsaimind.javaforumorgbeispiele.kartenspiel.spiel.Spiel;
import org.bonsaimind.javaforumorgbeispiele.kartenspiel.spiel.Spieler;

/**
 * Hauptklasse welche die Hauptprogrammlogik enthaelt.
 * <p>
 * {@code final} damit sich niemand von der Klasse ableitet.
 */
public final class Main {
	/**
	 * {@code private} Konstruktor damit niemand die Klasse instanziert.
	 */
	private Main() {
	}
	
	/**
	 * Haupteinstiegpunkt des Programms.
	 * 
	 * @param args Die Argumente welche dem Programm uebergeben wurden. Werden
	 *        in diesem Fall nicht verwendet (muessen aber angegeben werden
	 *        wegen der Signatur).
	 */
	public static final void main(String[] args) {
		// Erstellen einer neuen Spiel Instanz.
		Spiel spiel = new Spiel();
		
		// Das Spiel beginnen in dem wir die Karten und Spieler laden.
		// Man beachte dass wir hier die Karten mischen.
		spiel.spielBeginnen(
				erstelleKartenstapel().mischen(),
				erstelleSpieler());
		
		// Runde 1
		rundeAusgeben("1");
		spiel.spieleErsteRunde();
		kartenstapelAusgeben("Offen:", spiel.getOffenerKartenstapel());
		
		// Runde 2
		rundeAusgeben("2");
		spiel.spieleZweiteRunde();
		kartenstapelAusgeben("Offen:", spiel.getOffenerKartenstapel());
		
		// Runde 3
		rundeAusgeben("3");
		spiel.spieleZweiteRunde();
		kartenstapelAusgeben("Offen:", spiel.getOffenerKartenstapel());
		
		// Ende des Spiels.
		rundeAusgeben("Ende");
		kartenstapelAusgeben("Offen:", spiel.getOffenerKartenstapel());
		
		for (Spieler einzelnerSpieler : spiel.getSpieler()) {
			kartenstapelAusgeben(einzelnerSpieler.getName() + ":", einzelnerSpieler.getKartenstapel());
		}
	}
	
	/**
	 * Erstellt einen neuen {@link Kartenstapel} in dem alle Karten geladen
	 * werden.
	 * 
	 * @return Der neu erstellte {@link Kartenstapel}.
	 */
	private static final Kartenstapel erstelleKartenstapel() {
		// Laden der Farben und der Werte aus den Dateien.
		List<Farbe> farben = ladeFarben();
		List<Wert> werte = ladeWerte();
		
		// Erzeugen der Instanz welche wir erstellen.
		Kartenstapel kartenstapel = new Kartenstapel();
		
		// Erstellen der einzelnen Karten aus allen Farben und Werten, und
		// direkt diese zum Stapel hinzufuegen.
		for (Farbe farbe : farben) {
			for (Wert wert : werte) {
				kartenstapel.karteHinzufuegen(new Karte(farbe, wert));
			}
		}
		
		// Retournieren des erstellen Stapels.
		return kartenstapel;
	}
	
	/**
	 * Erstelle eine neue {@link List} an {@link Spieler}n in dem die Spieler
	 * geladen werden.
	 * 
	 * @return Die neu erstellte {@link List} an {@link Spieler}n.
	 */
	private static final List<Spieler> erstelleSpieler() {
		// Laden der Spieler aus der Datei.
		List<String> spielerNamen = ladeAusKonfigurationsDatei("spieler");
		
		// Erzeugen der List welcher wir erstellen.
		List<Spieler> spieler = new LinkedList<>();
		
		// Erstellen und hinzufuegen der Spieler in die Liste.
		for (String spielerName : spielerNamen) {
			spieler.add(new Spieler(spielerName));
		}
		
		// Retournieren der erstellten Liste.
		return spieler;
	}
	
	/**
	 * Gibt den gegebenen {@link String Text} und den gegebenen
	 * {@link Kartenstapel} hintereinander aus.
	 * 
	 * @param text Der vorangestellte {@link String Text} zum ausgeben.
	 * @param kartenstapel Der {@link Kartenstapel} zum ausgeben.
	 */
	private static final void kartenstapelAusgeben(String text, Kartenstapel kartenstapel) {
		// Ausgeben des Textes.
		System.out.print(text);
		
		// Ausgeben alle Karten.
		for (Karte karte : kartenstapel.getKarten()) {
			System.out.print(" (");
			System.out.print(karte.getFarbe().getName());
			System.out.print(" ");
			System.out.print(karte.getWert().getName());
			System.out.print(")");
		}
		
		// Abschlieszen der Zeile.
		System.out.println();
	}
	
	/**
	 * Laedt alle Werte aus der Konfigurationsdatei mit dem gegebenen Namen.
	 * 
	 * @param dateiName Der Name der Konfigurationsdatei (ohne Dateiendung oder
	 *        Pfad).
	 * @return Die {@link List} welche alle Werte aus der Konfigurationsdatei
	 *         enthaelt.
	 */
	private static final List<String> ladeAusKonfigurationsDatei(String dateiName) {
		// Auslesen der Datei mit Hilfe eines BufferedReaders, welcher es uns
		// erlaubt Zeilenweise zu lesen.
		//
		// Durch das schlieszen des BufferedReaders wird auch der
		// darunterliegende Stream der Resource geschlossen.
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(
				Main.class.getResourceAsStream("konfiguration/" + dateiName + ".text"),
				StandardCharsets.UTF_8))) {
			// Die Liste an Werten welche wir erzeugen.
			List<String> zeilen = new ArrayList<>();
			
			// Zwischenspeicher fuer die aktuelle Zeile.
			String zeile = null;
			
			// Auslesen aller Zeilen bis es keine weitere mehr gibt.
			while ((zeile = reader.readLine()) != null) {
				// Wir uebernehmen nur nicht leere Zeilen.
				if (!zeile.isEmpty()) {
					// Zeile hinzufuegen.
					zeilen.add(zeile);
				}
			}
			
			// Retournieren der gelesenen Werte.
			return zeilen;
		} catch (IOException e) {
			// Weiterwerfen der Ausnahme als RuntimeException.
			throw new IllegalStateException(String.format("Konfigurationsdatei <%s> konnte nicht gelesen werden.",
					dateiName));
		}
	}
	
	/**
	 * Laedt alle {@link Farbe}n aus der Konfigurationsdatei.
	 * 
	 * @return Die {@link List} an allen konfigurierten {@link Farbe}n.
	 */
	private static final List<Farbe> ladeFarben() {
		// Laden der Farben aus der Datei.
		List<String> farbenNamen = ladeAusKonfigurationsDatei("farben");
		
		// Erstellen der Liste welche wir retournieren.
		List<Farbe> farben = new ArrayList<>();
		
		// Erstellen und hinzufuegen der Farben in die Liste.
		for (String farbenName : farbenNamen) {
			farben.add(new Farbe(farbenName));
		}
		
		// Retournieren der erstellen Liste.
		return farben;
	}
	
	/**
	 * Laedt alle {@link Wert}e aus der Konfigurationsdatei.
	 * 
	 * @return Die {@link List} an allen konfigurierten {@link Wert}en.
	 */
	private static final List<Wert> ladeWerte() {
		// Laden der Werte aus der Datei.
		List<String> wertNamen = ladeAusKonfigurationsDatei("werte");
		
		// Erzeugen der List welcher wir erstellen.
		List<Wert> werte = new ArrayList<>();
		
		// Erstellen und hinzufuegen der Werte in die Liste.
		for (String wertName : wertNamen) {
			werte.add(new Wert(wertName));
		}
		
		// Retournieren der erstellen Liste.
		return werte;
	}
	
	/**
	 * Gibt die gegebene Runde aus.
	 * 
	 * @param runde Die Runde welche ausgegeben werden soll.
	 */
	private static final void rundeAusgeben(String runde) {
		// Eine Zeile Abstand.
		System.out.println();
		// Ausgeben der Runde.
		System.out.print("Runde: ");
		System.out.print(runde);
		// Abschlieszen der Runde.
		System.out.println();
	}
}
