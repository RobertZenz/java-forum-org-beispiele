/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.kartenspiel.karten;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Der {@link Kartenstapel} ist eine einfache Klasse welche einen Stapel, sprich
 * mehrere, {@link Karte}n zu halten.
 */
public class Kartenstapel {
	/** Die {@link List} an {@link Karte}n welche gehalten werden. */
	protected List<Karte> karten = new LinkedList<>();
	/** Die {@link #karten List an Karten} als unveraenderbare Ansicht. */
	protected List<Karte> kartenReadonly = Collections.unmodifiableList(karten);
	
	/**
	 * Erstellt eine neue {@link Kartenstapel} Instanz.
	 */
	public Kartenstapel() {
		super();
	}
	
	/**
	 * Die {@link Karte}n auf diesem {@link Kartenstapel} als {@link List}.
	 * <p>
	 * Die retournierte {@link List} ist unveraenderbar.
	 * <p>
	 * <i>Hinweis:</i> Wir retournieren hier eine unveraenderliche Instanz zu
	 * verhindern dass ein Verwender den Zustand direkt veraendern kann ohne
	 * ueber unsere Methoden zu gehen. Dies hat den Vorteil dass ein Aufrufer
	 * alle Moeglichkeiten einer {@link List} bekommt (suchen, iterieren, ...)
	 * aber wir mit Sicherheit annahmen ueber den Zustand der {@link List}
	 * treffen koennen, als auch die Manipulationen mit, zum Beispiel,
	 * Validierungs- und Fehlercode ausstatten koennen welcher nicht umgangen
	 * werden kann.
	 * 
	 * @return Die (unveraenderbare) {@link List} an {@link Karte}n auf diesem
	 *         {@link Kartenstapel}.
	 */
	public List<Karte> getKarten() {
		return kartenReadonly;
	}
	
	/**
	 * Fuegt die gegebene {@link Karte} oben auf diesen {@link Kartenstapel}
	 * hinzu.
	 * 
	 * @param karte Die {@link Karte} welche oben auf den {@link Kartenstapel}
	 *        hinzugefuegt werden soll.
	 * @return Diese {@link Kartenstapel} Instanz.
	 */
	public Kartenstapel karteHinzufuegen(Karte karte) {
		karten.add(karte);
		
		return this;
	}
	
	/**
	 * Mischt diesen {@link Kartenstapel} zufaellig durch.
	 * 
	 * @return Diese {@link Kartenstapel} Instanz.
	 */
	public Kartenstapel mischen() {
		Collections.shuffle(karten);
		
		return this;
	}
	
	/**
	 * Nimmt die oberste {@link Karte} von diesem {@link Kartenstapel} und
	 * retourniert sie. Diese {@link Karte} wird von diesem {@link Kartenstapel}
	 * dabei entfernt.
	 * <p>
	 * <i>Hinweis:</i> Hier fehlen Pruefungen ob sich noch {@link Karte}n auf
	 * diesem {@link Kartenstapel} befinden und eine entsprechende
	 * Fehlerbehandlung. Moegliche waere entweder eine Ausnahme zu werfen, oder
	 * einfach {@code null} zu retournieren.
	 * 
	 * @return Die oberste {@link Karte} von diesem {@link Kartenstapel}.
	 */
	public Karte obersteKarteNehmen() {
		return karten.remove(karten.size() - 1);
	}
}
