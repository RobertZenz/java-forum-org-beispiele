/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.deprecation.api;

import java.util.logging.Logger;

/**
 * Der {@link Adder} erlaubt es Zahlen zu addieren.
 * <p>
 * <b>Deprecated:</b>
 * 
 * @since 1.0
 * @deprecated {@link Adder} wurde mit 1.4 abgeloest durch {@link Calculator}
 *             welcher eine bessere API bereitstellt. Fuer die Migration von
 *             {@link #add(int, int)} muss lediglich die Instanz ausgetauscht
 *             werden, der Quellcode muss nicht geaendert werden.
 */
@Deprecated
public class Adder {
	/** Der {@link Logger} fuer diese Klasse. */
	private static final Logger LOGGER = Logger.getLogger(Adder.class.getName());
	
	/** Die {@link Calculator} Instanz auf welche wir Aufrufe weiterleiten. */
	protected Calculator calculator = new Calculator();
	
	/**
	 * Erstellt eine neue {@link Adder} Instanz.
	 */
	public Adder() {
		super();
	}
	
	/**
	 * Addiert die beiden gegebenen Zahlen und retourniert das Ergebnis.
	 * 
	 * @param a Die erste Zahl.
	 * @param b Die zweite Zahl.
	 * @return Das Ergebnis der Addition der beiden gegebenen Zahlen.
	 * @deprecated {@link #add(int, int)} ist ab 1.4 abgeloest durch
	 *             {@link Calculator#add(int...)}. Fuer Migration muss die
	 *             {@link Adder} Instanz durch eine {@link Calculator} Instanz
	 *             ausgetauscht werden, der Aufruf der Methode selbst aendert
	 *             sich im Quellcode nicht.
	 * 
	 *             <pre>
	 * <code>
	 * // Bisherige Verwendung
	 * Adder adder = new Adder();
	 * int result = adder.add(1, 2);
	 * 
	 * // Neue API
	 * Calculator calculator = new Calculator();
	 * int result = calculator.add(1, 2);
	 * </code>
	 *             </pre>
	 * 
	 *             Wenn man mehrere verkette Aufrufe hat kann man diese
	 *             {@link Calculator} zu einem vereinfacht werden.
	 * 
	 *             <pre>
	 * <code>
	 * // Bisherige Verwendung
	 * Adder adder = new Adder();
	 * int result = adder.add(1, adder.add(2, adder.add(3, 4)));
	 * 
	 * // Neue API
	 * Calculator calculator = new Calculator();
	 * int result = calculator.add(1, 2, 3, 4);
	 * </code>
	 *             </pre>
	 * 
	 *             Diese Methode gibt eine Information aus wenn sie aufgerufen
	 *             wird um darauf hinzuweisen dass diese ersetzt werden soll.
	 *             Diese Informationsmeldung kann ueber die allgemeine
	 *             {@link Logger} Konfiguration abgeschaltet werden.
	 */
	@Deprecated
	public int add(int a, int b) {
		// Originale Implementierung:
		//
		// return a + b;
		
		LOGGER.info("Adder.add(int, int) ist abgeloest durch Calculator.add(int...)! Diese Klasse wird in einer zukuenftigen Version eventuell entfernt.");
		
		// Intern leiten wir weiter auf die neue Klasse/Funktionalitaet um die
		// Komplexitaet gering zu halten. Man koennte auch die Logik hier
		// erhalten, dann muss man aber eventuell Korrekturen in der Logik auch
		// hier nachziehen anstatt nur in der neuen Logik.
		//
		// Zusaetzlich hat das interne Weiterleiten den Vorteil dass man direkt
		// merkt ob bestimmte Funktionalitaet in der neuen API verfuegbar ist.
		return calculator.add(a, b);
	}
}
