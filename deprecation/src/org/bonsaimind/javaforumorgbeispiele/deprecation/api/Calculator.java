/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.deprecation.api;

/**
 * Der {@link Calculator} stellt unterschiedliche Rechenoperationen bereit.
 * 
 * @since 1.4
 */
public class Calculator {
	/**
	 * Erstellt eine neue {@link Calculator} Instanz.
	 */
	public Calculator() {
		super();
	}
	
	/**
	 * Addiert die gegebenen Werte und retourniert das Ergebnis.
	 * 
	 * @param values Die Werte welche addiert werden sollen. Wenn dies
	 *        {@code null} oder leer ist, wird {@code 0} retourniert.
	 * @return Der Ergebnis von der Addition aller gegebenen Werte.
	 */
	public int add(int... values) {
		int result = 0;
		
		if (values != null && values.length > 0) {
			for (int value : values) {
				result = result + value;
			}
		}
		
		return result;
	}
	
	// Weiter Methoden welche in dieser Klasse waeren:
	//
	// public int divide(int... values)
	// public int multiply(int... values)
	// public int subtract(int... values)
}
