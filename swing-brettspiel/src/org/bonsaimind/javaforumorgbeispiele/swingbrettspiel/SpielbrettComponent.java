/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingbrettspiel;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * Die {@link SpielbrettComponent} ist eine {@link JComponent} Erweiterung
 * welche unser Spielbrett darstellt.
 * <p>
 * <i>Hinweis:</i> Diese Klasse mischt Model und Repraesentation, im Idealfall
 * gebe es eine {@code Spielbrett} Klasse welche die Informationen ueber das
 * Feld und den Spieler/Farbe haelt, und diese Klasse hier diese Informationen
 * von dort bekommt.
 */
public class SpielbrettComponent extends JComponent {
	/** Die Anzahl der Felder in der horizontalen und vertikalen. */
	protected static final int FELD_ANZAHL = 8;
	/** Die Dicke der Linien zwischen den Feldern. */
	protected static final int LINIEN_DICKE = 2;
	
	/**
	 * Die {@link SpielsteinComponent} welche gerade gezogen wird mit der Maus.
	 */
	protected SpielsteinComponent gezogeneSpielsteinComponent = null;
	/**
	 * Der Versatz in der horizontalen der {@link #gezogeneSpielsteinComponent}
	 * zur Maus.
	 */
	protected int gezogeneSpielsteinVersatzX = 0;
	/**
	 * Der Versatz in der vertikalen der {@link #gezogeneSpielsteinComponent}
	 * zur Maus.
	 */
	protected int gezogeneSpielsteinVersatzY = 0;
	/**
	 * Die {@link JComponent}s welche als horizontale Linien verwendet werden.
	 */
	protected List<JComponent> horizontaleLinieComponents = null;
	/** Die {@link SpielsteinComponent}s auf diesem Brett. */
	protected List<SpielsteinComponent> spielsteinComponents = null;
	/** Die {@link JComponent}s welche als vertikale Linien verwendet werden. */
	protected List<JComponent> vertikaleLinienComponents = null;
	
	/**
	 * Erstellt eine neue {@link SpielbrettComponent} Instanz.
	 */
	public SpielbrettComponent() {
		super();
		
		// Erstellen der horizontalen und vertikalen JComponents fuer die
		// Linien.
		horizontaleLinieComponents = new ArrayList<>();
		vertikaleLinienComponents = new ArrayList<>();
		
		for (int linienZaehler = 0; linienZaehler <= FELD_ANZAHL; linienZaehler++) {
			JPanel horizontaleLinieComponent = new JPanel();
			horizontaleLinieComponent.setBackground(Color.BLACK);
			horizontaleLinieComponents.add(horizontaleLinieComponent);
			
			JPanel vertikaleLinieComponent = new JPanel();
			vertikaleLinieComponent.setBackground(Color.BLACK);
			vertikaleLinienComponents.add(vertikaleLinieComponent);
		}
		
		// Erstellen der Spielsteine fuer das Brett.
		spielsteinComponents = new ArrayList<>();
		
		for (int spielsteinZaehler = 0; spielsteinZaehler < 8; spielsteinZaehler++) {
			SpielsteinComponent spielsteinBlauComponent = new SpielsteinComponent(Color.BLUE);
			spielsteinBlauComponent.setFeld(
					spielsteinZaehler,
					0);
			spielsteinComponents.add(spielsteinBlauComponent);
			
			SpielsteinComponent spielsteinRotComponent = new SpielsteinComponent(Color.RED);
			spielsteinRotComponent.setFeld(
					spielsteinZaehler,
					FELD_ANZAHL - 1);
			spielsteinComponents.add(spielsteinRotComponent);
		}
		
		// Einhaengen der Mouse*Listener welche verwendet werden um das Ziehen
		// eines Spielsteins zu ermoeglichen.
		addMouseListener(new SpielsteinZiehenderMouseListener());
		addMouseMotionListener(new SpielsteinZiehenderMouseMotionListener());
		// Setzen des "null-Layouts" weil wir die Komponenten alle selbst
		// positionieren.
		setLayout(null);
		
		// Hinzufuegen aller Spielsteine zu diesem Brett.
		for (SpielsteinComponent spielsteinComponent : spielsteinComponents) {
			add(spielsteinComponent);
		}
		
		// Hinzufuegen aller horizontalen Linien zu diesem Brett.
		for (JComponent horizontaleLinie : horizontaleLinieComponents) {
			add(horizontaleLinie);
		}
		
		// Hinzufuegen aller vertikalen Linien zu diesem Brett.
		for (JComponent vertikaleLinie : vertikaleLinienComponents) {
			add(vertikaleLinie);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void doLayout() {
		// Positinieren der Linien.
		layoutLinien();
		// Positinieren der Spielsteine.
		layoutSpielsteine();
	}
	
	/**
	 * Positioniert die {@link SpielbrettComponent#horizontaleLinieComponents
	 * horizontalen} und {@link #vertikaleLinienComponents vertikalen} Linien.
	 */
	protected void layoutLinien() {
		// Berechnen der Grosze eines einzelnen Feldes.
		int horizontaleFeldGroesze = (getWidth() / FELD_ANZAHL);
		int vertikaleFeldGroesze = (getHeight() / FELD_ANZAHL);
		
		// Damit wir immer gleich grosze Felder haben, muessen wir das gesamte
		// Brett mittig in unserer Component positionieren. Dieser Versatz gibt
		// den Rand des Spielfelds zum Rand der Component wider.
		int horizontalerSpielfeldVersatz = (getWidth() - (horizontaleFeldGroesze * FELD_ANZAHL)) / 2;
		int vertikalerSpielfeldVersatz = (getHeight() - (vertikaleFeldGroesze * FELD_ANZAHL)) / 2;
		
		// Fuer jede horizontale und vertikale Linie.
		for (int linienIndex = 0; linienIndex <= FELD_ANZAHL; linienIndex++) {
			// Holen der Component fuer die horizontale Linie.
			JComponent horizontaleLinieComponent = horizontaleLinieComponents.get(linienIndex);
			// Ausrichten der Component vom linken zum rechten Rand, und mit dem
			// entsprechenden Versatz von oben.
			horizontaleLinieComponent.setBounds(
					horizontalerSpielfeldVersatz,
					vertikaleFeldGroesze * linienIndex - (LINIEN_DICKE / 2) + vertikalerSpielfeldVersatz,
					getWidth() - (horizontalerSpielfeldVersatz * 2),
					LINIEN_DICKE);
			
			// Holen der Component fuer die vertikale Linie.
			JComponent vertikaleLinieComponent = vertikaleLinienComponents.get(linienIndex);
			// Ausrichten der Component vom oberen zum unteren Rand, und mit dem
			// entsprechenden Versatz von links.
			vertikaleLinieComponent.setBounds(
					horizontaleFeldGroesze * linienIndex - (LINIEN_DICKE / 2) + horizontalerSpielfeldVersatz,
					vertikalerSpielfeldVersatz,
					LINIEN_DICKE,
					getHeight() - (vertikalerSpielfeldVersatz * 2));
		}
	}
	
	/**
	 * Positioniert die {@link #spielsteinComponents Spielsteine}.
	 */
	protected void layoutSpielsteine() {
		// Berechnen der Grosze eines einzelnen Feldes.
		int horizontaleFeldGroesze = (getWidth() / FELD_ANZAHL);
		int vertikaleFeldGroesze = (getHeight() / FELD_ANZAHL);
		
		// Damit wir immer gleich grosze Felder haben, muessen wir das gesamte
		// Brett mittig in unserer Component positionieren. Dieser Versatz gibt
		// den Rand des Spielfelds zum Rand der Component wider.
		int horizontalerSpielfeldVersatz = (getWidth() - (horizontaleFeldGroesze * FELD_ANZAHL)) / 2;
		int vertikalerSpielfeldVersatz = (getHeight() - (vertikaleFeldGroesze * FELD_ANZAHL)) / 2;
		
		// Durchgehen alles Spielsteine.
		for (SpielsteinComponent spielsteinComponent : spielsteinComponents) {
			// Wenn dieser Spielstein der aktuell gezogene Stein ist duerfen wir
			// diesen nicht neu positionieren da er seine eigene Position
			// auszerhalb der Felder hat.
			if (spielsteinComponent != gezogeneSpielsteinComponent) {
				// Positionieren des Spielsteins in das Feld zu welchen er
				// gehoert.
				spielsteinComponent.setBounds(
						(horizontaleFeldGroesze * spielsteinComponent.getFeldX()) + horizontalerSpielfeldVersatz,
						(vertikaleFeldGroesze * spielsteinComponent.getFeldY()) + vertikalerSpielfeldVersatz,
						horizontaleFeldGroesze,
						vertikaleFeldGroesze);
			}
		}
	}
	
	/**
	 * Der {@link SpielsteinZiehenderMouseListener} ist eine
	 * {@link MouseAdapter} Erweiterung welche die Basis-Funktionalitaet fuer
	 * das Ziehen eines Spielsteins bereitstellt.
	 */
	protected class SpielsteinZiehenderMouseListener extends MouseAdapter {
		/**
		 * Erstellt eine neue {@link SpielsteinZiehenderMouseListener} Instanz.
		 */
		public SpielsteinZiehenderMouseListener() {
			super();
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void mousePressed(MouseEvent e) {
			// Wenn die Maus gedrueckt wird, pruefen wir ob dies auf einem
			// Spielstein passiert, und wenn ja wird dieser gezogen.
			
			// Finden der Component welche sich gerade unter Maus befindet.
			Component componentUnterMaus = getComponentAt(e.getX(), e.getY());
			
			// Pruefen ob diese Component ein Spielstein ist.
			if (componentUnterMaus instanceof SpielsteinComponent) {
				// Wir uebernehmen diesen Spielstein als den aktuell gezogenen.
				gezogeneSpielsteinComponent = (SpielsteinComponent)componentUnterMaus;
				// Und setzen den Versatz entsprechende der aktuellen
				// Mausposition zum Spielstein.
				gezogeneSpielsteinVersatzX = gezogeneSpielsteinComponent.getX() - e.getX();
				gezogeneSpielsteinVersatzY = gezogeneSpielsteinComponent.getY() - e.getY();
			}
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void mouseReleased(MouseEvent e) {
			// Wird die Maus losgelassen koennen wir auch den Spielstein "fallen
			// lassen".
			
			// Entfernen des gezogenen Steins so dass dieser wieder automatisch
			// in sein aktuelles Feld positioniert wird.
			gezogeneSpielsteinComponent = null;
			gezogeneSpielsteinVersatzX = 0;
			gezogeneSpielsteinVersatzY = 0;
			
			// Einmal das Layout anstoszen damit der gezogene Stein korrekt
			// liegt.
			layoutSpielsteine();
		}
	}
	
	/**
	 * Der {@link SpielsteinZiehenderMouseListener} ist eine
	 * {@link MouseAdapter} Erweiterung welche die Basis-Funktionalitaet fuer
	 * das Ziehen eines Spielsteins bereitstellt.
	 */
	protected class SpielsteinZiehenderMouseMotionListener extends MouseMotionAdapter {
		/**
		 * Erstellt eine neue {@link SpielsteinZiehenderMouseMotionListener}
		 * Instanz.
		 */
		public SpielsteinZiehenderMouseMotionListener() {
			super();
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void mouseDragged(MouseEvent e) {
			// Wenn die Maus bewegt wird bewegen wir den aktuell gezogenen
			// Spielstein mit ihr.
			
			// Pruefen ob derzeit ein Spielstein gezogen wird.
			if (gezogeneSpielsteinComponent != null) {
				// Setzen der Position des Spielsteins zu der der Maus.
				gezogeneSpielsteinComponent.setLocation(
						e.getX() + gezogeneSpielsteinVersatzX,
						e.getY() + gezogeneSpielsteinVersatzY);
				
				// Berechnen der horizontalen und vertikalen Groesze eines
				// Feldes.
				int horizontaleFeldGroesze = (getWidth() / FELD_ANZAHL);
				int vertikaleFeldGroesze = (getHeight() / FELD_ANZAHL);
				
				// Setzen des neuen Feldes fuer diesen Spielstein basierend auf
				// der aktuellen Mausposition.
				//
				// Hinweis: Die Mausposition ist der "naive" Ansatz, je nachdem
				// welches Bedienungsgefuehl man will koennte man auch nachdem
				// gehen in welchem Feld der Groszteil des Spielsteins sich
				// befindet.
				gezogeneSpielsteinComponent.setFeld(
						e.getX() / horizontaleFeldGroesze,
						e.getY() / vertikaleFeldGroesze);
			}
		}
	}
}
