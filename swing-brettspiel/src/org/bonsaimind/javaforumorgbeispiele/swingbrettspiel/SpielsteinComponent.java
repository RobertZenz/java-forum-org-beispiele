/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingbrettspiel;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JComponent;

/**
 * Die {@link SpielsteinComponent} ist eine {@link JComponent} Erweiterung
 * welche einen einzelnen Spielstein darstellt.
 * <p>
 * <i>Hinweis:</i> Diese Klasse mischt Model und Repraesentation, im Idealfall
 * gebe es eine {@code Spielstein} Klasse welche die Informationen ueber das
 * Feld und den Spieler/Farbe haelt, und diese Klasse hier diese Informationen
 * von dort bekommt.
 */
public class SpielsteinComponent extends JComponent {
	/** Die Breite des Randes des Steins in Pixel. */
	protected static final int RAND_BREITE = 2;
	
	/** Die Farbe dieses Spielsteins. */
	protected Color farbe = null;
	/**
	 * Die X-Koordinate von dem Feld auf welchem sich dieser Spielstein
	 * befindet.
	 * <p>
	 * <i>Hinweis:</i> Dieses Feld sollte in einer geeigneten Model-Klasse sein.
	 */
	protected int feldX = 0;
	/**
	 * Die Y-Koordinate von dem Feld auf welchem sich dieser Spielstein
	 * befindet.
	 * <p>
	 * <i>Hinweis:</i> Dieses Feld sollte in einer geeigneten Model-Klasse sein.
	 */
	protected int feldY = 0;
	
	/**
	 * Erstellt eine neue {@link SpielsteinComponent} Instanz.
	 * 
	 * @param farbe Die {@link Color Farbe} fuer diesen Spielstein.
	 */
	public SpielsteinComponent(Color farbe) {
		super();
		
		// Uebernehmen der Parameter.
		this.farbe = farbe;
		
		// Wir brauchen keinen LayoutManager also setzen wir auch keinen.
		setLayout(null);
	}
	
	/**
	 * Retourniert die Farbe dieses {@link SpielsteinComponent Spielsteins}.
	 * 
	 * @return Die Farbe dieses {@link SpielsteinComponent Spielsteins}.
	 */
	public Color getFarbe() {
		return farbe;
	}
	
	/**
	 * Retourniert die X-Koordinate des Feldes von diesem
	 * {@link SpielsteinComponent Spielstein}.
	 * 
	 * @return Die X-Koordinate des Feldes von diesem {@link SpielsteinComponent
	 *         Spielstein}.
	 */
	public int getFeldX() {
		return feldX;
	}
	
	/**
	 * Retourniert die Y-Koordinate des Feldes von diesem
	 * {@link SpielsteinComponent Spielstein}.
	 * 
	 * @return Die Y-Koordinate des Feldes von diesem {@link SpielsteinComponent
	 *         Spielstein}.
	 */
	public int getFeldY() {
		return feldY;
	}
	
	/**
	 * Setzen des Feldes auf welchem dieser {@link SpielsteinComponent
	 * Spielstein} sein soll.
	 * 
	 * @param feldX Die X-Koordinate des Feldes.
	 * @param feldY Die Y-Koordinate des Feldes.
	 */
	public void setFeld(int feldX, int feldY) {
		this.feldX = feldX;
		this.feldY = feldY;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void paintComponent(Graphics graphics) {
		// Zuerst zeichnen wir eine grosze Scheibe mit der Randfarbe um den Rand
		// zu erhalten.
		//
		// Hinweis: Wir koennten auch einen Kreis mit entsprechender Dicke
		// zeichnen, aber meistens ist diese Methode hier einfacher um grafische
		// Artefakte zu vermeiden.
		graphics.setColor(Color.DARK_GRAY);
		graphics.fillOval(
				0,
				0,
				getWidth(),
				getHeight());
		
		// Dann zeichnen wir eine kleinere Scheibe mit der eigentlichen Farben.
		graphics.setColor(farbe);
		graphics.fillOval(
				RAND_BREITE,
				RAND_BREITE,
				getWidth() - (RAND_BREITE * 2),
				getHeight() - (RAND_BREITE * 2));
	}
}
