/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Swing Brettspiel
 * <p>
 * Demonstriert wie man ein sehr einfaches Brettspiel umsetzen kann in Swing bei
 * welchem die einzelnen Spielsteine gezogen werden koennen.
 * <p>
 * Hier wird lediglich der "Zieh"-Mechanismus demonstriert. Was fehlt ist sowohl
 * ein korrektes Model als auch eine Regelmaschine um die Spielregeln selbst um-
 * und durchzusetzen. Die
 * {@link org.bonsaimind.javaforumorgbeispiele.swingbrettspiel.SpielsteinComponent}
 * uebernimmt dabei einen Teil der Aufgabe des Models. Idealerweise waere
 * natuerlich die Information in welchem Spielfeld sich der Spielstein befindet
 * in einem Model getrennt von der Klasse welche fuer die Anzeige der
 * Oberflaeche verwendet wird.
 */

package org.bonsaimind.javaforumorgbeispiele.swingbrettspiel;