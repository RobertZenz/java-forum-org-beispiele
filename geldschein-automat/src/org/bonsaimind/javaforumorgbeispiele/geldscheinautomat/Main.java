/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.geldscheinautomat;

import java.util.Arrays;

/**
 * Hauptklasse welche die Hauptprogrammlogik enthaelt.
 * <p>
 * {@code final} damit sich niemand von der Klasse ableitet.
 */
public final class Main {
	/**
	 * {@code private} Konstruktor damit niemand die Klasse instanziert.
	 */
	private Main() {
	}
	
	/**
	 * Haupteinstiegpunkt des Programms.
	 * 
	 * @param args Die Argumente welche dem Programm uebergeben wurden. Werden
	 *        in diesem Fall nicht verwendet (muessen aber angegeben werden
	 *        wegen der Signatur).
	 */
	public static final void main(String[] args) {
		// Die Werte der Geldscheine welche verfuegbar sind.
		final int[] geldscheine = new int[] { 5, 10, 20, 50, 100, 200 };
		// Der Betrage welcher gestueckelt werden soll.
		final int betrag = 987;
		
		// Stueckeln des Betrags.
		Stueckelung stueckelung = Stueckelung.betragStueckeln(betrag, geldscheine);
		
		// Ausgeben der Stueckelung aller Geldscheine.
		//
		// Hinweis: In diesem Fall muss man die "Eingabevariable" von allen
		// verfuegbaren Geldscheinen zur Verfuegung haben wenn man diese
		// Abfragen will. Eine andere Moeglichkeit waere diese Information
		// direkt in der Stueckelung zu hinterlegen, entweder in dem an dort das
		// Array an Geldscheinen dupliziert und abfragbar macht, oder in dem
		// man eine Liste mit einer eigenen "GeldscheinBuendel" Klasse als
		// Ergebnis hat.
		for (int geldschein : geldscheine) {
			// Wir geben nur Geldscheine aus welche auf enthalten sind.
			if (stueckelung.getAnzahl(geldschein) > 0) {
				// Ausgabe des jeweiligen Geldscheins und die Anzahl.
				System.out.format("%der: %d\n",
						Integer.valueOf(geldschein),
						Integer.valueOf(stueckelung.getAnzahl(geldschein)));
			}
		}
		
		// Es kann sein dass es noch einen Restbetrag gibt welcher in Muenzen
		// ausgezahlt werden muss.
		if (stueckelung.getMuenzenWert() > 0) {
			// Ausgabe des Restbetrags.
			System.out.format("Rest in Muenzen: %d\n",
					Integer.valueOf(stueckelung.getMuenzenWert()));
		}
	}
	
	/**
	 * Die {@link Stueckelung} haelt die Stueckelung eines Betrags in bestimmte
	 * Geldscheine.
	 * <p>
	 * Eine Instanz kann nur ueber die Methode
	 * {@link #betragStueckeln(int, int[])} erstellt werden.
	 * <p>
	 * <i>Hinweis:</i> Die Klasse selbst ist nur durch Konvention der Namen auf
	 * Geldscheine beschraenkt, man koennte diese auch direkt fuer Muenzen
	 * verwenden.
	 */
	public static class Stueckelung {
		/** Die Anzahlen der einzelnen Geldscheine. */
		protected int[] geldscheineAnzahl = null;
		/** Die Werte der einzelnen Geldscheine. */
		protected int[] geldscheineWerte = null;
		/** Der Restbetrag in Muenzen. */
		protected int muenzenWert = 0;
		
		/**
		 * Erstellt eine neue {@link Stueckelung} Instanz mit dem gegebenen
		 * Zustand.
		 * <p>
		 * Die Parameter {@code geldscheineAnzahl} und {@code geldscheineWerte}
		 * muessen uebereinstimmen als dass zum Beispiel der Index "3" in beiden
		 * Arrays den selben Geldschein referenziert. Es finden keine Pruefungen
		 * auf diesen Parametern statt.
		 * <p>
		 * <i>Hinweis:</i> Der Konstruktor ist `protected` um zu verhindern dass
		 * jemand die Klasse instanziert und verwendet ohne dass diese durch die
		 * {@link #betragStueckeln(int, int[])} Fabriksmethode erstellte wurde.
		 * Dies ist "notwendig" da hier tatsaechlich der interne Zustand der
		 * Instanz uebergeben und nicht zusaetzlich geprueft wird. Ein
		 * Aussenstehender koennte einen inkonsistenten Zustand uebergeben oder
		 * diesen zur Laufzeit veraendern (da die Arrays nicht kopiert werden).
		 * Dennoch wollen wir die Moeglichkeit erhalten dass sich jemand von
		 * dieser Klasse ableiten kann, um zum Beispiel diese zu erweitern
		 * (dafuer muss dann natuerlich eine eigene Fabriksmethode
		 * bereitgestellt werden).
		 * 
		 * @param geldscheineAnzahl Die Anzahlen der einzelnen Geldscheine.
		 * @param geldscheineWerte Die Werte der einzelnen Geldscheine.
		 * @param muenzenWert Der Resdtwert in Muenzen.
		 */
		protected Stueckelung(
				int[] geldscheineAnzahl,
				int[] geldscheineWerte,
				int muenzenWert) {
			super();
			
			// Uebernehmen der Parameter.
			//
			// Hinweis: Als defensive Masznahme kann man die Arrays an dieser
			// Stelle kopieren mit Arrays.copyOf. Die haette den Vorteil dass
			// der Aufrufer nicht in den internen Zustand unserer Klasse
			// veraendern koennte in dem man Referenzen auf diese Arrays behaelt
			// und diese dann die Werte darin einfach veraendert. Wir verzichten
			// hier darauf da wir dem Aufrufe (uns selbst) vertrauen und dieses
			// Verhalten dokumentiert haben.
			this.geldscheineAnzahl = geldscheineAnzahl;
			this.geldscheineWerte = geldscheineWerte;
			this.muenzenWert = muenzenWert;
		}
		
		/**
		 * Erstellt eine {@link Stueckelung} Instanz welche die Stueckelung fuer
		 * den gegebenen Betrag und die gegebenen Geldscheine enthaelt.
		 * 
		 * @param betrag Der Geldbetrag welcher gestueckelt werden soll.
		 * @param geldscheine Die Liste an Geldscheinen in welche die
		 *        Stueckelung erfolgen soll.
		 * @return Eine {@link Stueckelung} Instanz welche die Stueckelung fuer
		 *         den gegebenen Betrag und die gegebenen Geldscheine enthaelt.
		 */
		public static final Stueckelung betragStueckeln(int betrag, int[] geldscheine) {
			// Wir kopieren und sortieren das Array an Geldscheinen, damit wir
			// es (umgekehrt) vom groszten zum kleinsten Geldschein durchgehen
			// koennen.
			int[] sortierteGeldscheine = Arrays.copyOf(geldscheine, geldscheine.length);
			Arrays.sort(sortierteGeldscheine);
			
			// Dir Variablen welche unser Ergebnis (Stueckelung) halten.
			int[] geldscheineAnzahl = new int[geldscheine.length];
			int[] geldscheineWerte = new int[geldscheine.length];
			int muenzenWert = betrag;
			
			// Wir gehen das Array umgekehrt durch, da dieses vom kleinsten zum
			// groszten Geldschein sortiert ist, wir aber vom groszten zum
			// kleinsten arbeiten wollen, um so wenig Geldscheine wie moeglich
			// auszugeben.
			for (int index = sortierteGeldscheine.length - 1; index >= 0; index--) {
				// Der Wert des aktuellen Geldscheins.
				int geldscheinWert = geldscheine[index];
				
				// Die folgende Logik hat keine Abfrage ob der Geldschein
				// ueberhaupt ausgegeben werden kann oder nicht, also ob ein
				// 200er in 60 vorkommt. Dies ist nicht notwendig da die
				// Operationen den muenzenWert nicht veraendern wuerden in
				// diesem Fall.
				
				// Setzen des Werts in den Zustand.
				geldscheineWerte[index] = geldscheinWert;
				// Errechnen der Menge fuer diesen Geldschein.
				//
				// Hinweis: Hier haben wir eine int-Division. Wenn man zwei ints
				// dividiert erhaelt man einen weiteren int. Dabei wird der
				// Betrag abgeschnitten, also wuerde das Ergebnis "4.9" sein,
				// wird dies auf "4" abgeschnitten.
				geldscheineAnzahl[index] = muenzenWert / geldscheinWert;
				
				// Mit dem Modulo koennen wir nun den "Restwert" nach dem Abzug
				// des aktuellen Geldscheins ausrechnen.
				muenzenWert = muenzenWert % geldscheinWert;
			}
			
			// Reoturnieren einer neuen Stueckelung Instanz mit unserem
			// erzeugten Zustand.
			return new Stueckelung(
					geldscheineAnzahl,
					geldscheineWerte,
					muenzenWert);
		}
		
		/**
		 * Retourniert die Anzahl der Geldscheine mit dem gegebenen Wert.
		 * {@code -1} wenn es diesen Geldschein nicht gibt.
		 * 
		 * @param geldscheinWert Der Wert der Geldschein fuer welche die Anzahl
		 *        retourniert werden soll.
		 * @return Die Anzahl der Geldscheine fuer den gegebenen Wert,
		 *         {@code -1} wenn es diesen Geldschein nicht gibt.
		 */
		public int getAnzahl(int geldscheinWert) {
			// Index des entsprechenden Geldscheins finden.
			int geldscheinIndex = indexOfGeldschein(geldscheinWert);
			
			// Pruefen ob es den Geldschein in dieser Stueckelung gibt.
			if (geldscheinIndex >= 0) {
				// Wenn Ja, die Anzahl retournieren.
				return geldscheineAnzahl[geldscheinIndex];
			} else {
				// Wenn Nein, dann einen "Fehlerwert" retournieren.
				return -1;
			}
		}
		
		/**
		 * Retourniert den Wert welcher in Muenzen ausgegeben werden muss.
		 * 
		 * @return Der Wert welcher in Muenzen ausgegeben werden muss.
		 */
		public int getMuenzenWert() {
			// Reoturnieren des Werts in Muenzen.
			return muenzenWert;
		}
		
		/**
		 * Retourniert den Index fuer den gegebenen Geldschein. {@code -1} wenn
		 * der Geldschein in dieser Stueckelung nicht existiert.
		 * 
		 * @param geldscheinWert Der Wert des Geldscheins fuer welchen der Index
		 *        retourniert werden soll.
		 * @return Der Index fuer den gegebenen Geldschein, {@code -1} wenn der
		 *         Geldschein in dieser Stueckelung nicht existeirt.
		 */
		protected int indexOfGeldschein(int geldscheinWert) {
			// Durchgehen aller Geldscheine.
			for (int index = 0; index < geldscheineWerte.length; index++) {
				// Wenn es dieser Geldschein ist.
				if (geldscheineWerte[index] == geldscheinWert) {
					// Dann den Index retournieren.
					return index;
				}
			}
			
			// Retournieren eines Fehlerwerts wenn dieser Geldschein nicht
			// existiert.
			return -1;
		}
	}
}
