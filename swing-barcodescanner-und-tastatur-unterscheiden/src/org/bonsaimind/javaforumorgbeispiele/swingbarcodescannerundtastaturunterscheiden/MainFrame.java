/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingbarcodescannerundtastaturunterscheiden;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.lang.reflect.InvocationTargetException;
import java.time.Duration;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.text.JTextComponent;

/**
 * {@link MainFrame} ist eine {@link JFrame}-Erweiterung welche das Hauptfenster
 * implementiert.
 */
public class MainFrame extends JFrame {
	/**
	 * Das {@link JLabel} in welchem wir anzeigen von wem wir die Eingabe
	 * annehmen.
	 */
	protected JLabel infoLabel = null;
	/** Das {@link JTextField} welches uns als Eingabefeld dient. */
	protected JTextField inputTextField = null;
	/**
	 * Der {@link TimeoutBuffer} in welchen wir die getippten Zeichen
	 * verschieben.
	 */
	protected TimeoutBuffer timeoutBuffer = null;
	
	/**
	 * Erstellt eine neue {@link MainFrame} Instanz.
	 */
	public MainFrame() {
		super();
		
		// Erstellen eines neuen TimeoutBuffer mit einem Timeout von 30ms.
		// Wenn diese Zeit ueberschritten wird, wird der gebufferte Wert an die
		// gegebene Funktion uebergeben.
		timeoutBuffer = new TimeoutBuffer(
				Duration.ofMillis(30l),
				this::handleBufferTimedout);
		
		// Erstellen des JTextField welches uns als Eingabefeld dient.
		inputTextField = new JTextField();
		
		// Erstellen des JLabel in welchem wir anzeigen werden ob wir den
		// Benutzer oder den Scanner annehmen.
		infoLabel = new JLabel();
		infoLabel.setText("Scanner oder eintippen...");
		
		// Aufsetzen und konfigurieren des Fensters.
		setTitle("Swing Barcode-Scanner und Tastatur unterscheiden");
		setLayout(new BorderLayout());
		add(inputTextField, BorderLayout.NORTH);
		add(infoLabel, BorderLayout.SOUTH);
		
		// Da wir keine (Mindest-)Groesze gesetzt haben, muessen wir einmal
		// dafuer sorgen dass der Layout-Manager die ideale Grosze des Fensters
		// berechnet und setzt.
		pack();
		
		// Hierfuer registrieren wir einen globalen KeyEvent-Handler welcher es
		// uns erlaubt alle Tastenanschlage applikationsweit abzufangen und zu
		// pruefen.
		//
		// Hinweis: Man koennte dies auch auf einzelne Eingabefelder haengen.
		KeyboardFocusManager.getCurrentKeyboardFocusManager()
				.addKeyEventDispatcher(this::handleGlobalKeyEvent);
	}
	
	/**
	 * Handhabt wenn der {@link #timeoutBuffer} seinen Buffer ausleert.
	 * 
	 * @param string Der {@link String} Wert welcher im Buffer war.
	 */
	protected void handleBufferTimedout(String string) {
		// Pruefen ob wir uns auf dem Swing-Thread befinden.
		if (!SwingUtilities.isEventDispatchThread()) {
			try {
				// Wenn nicht muessen wir uns auf diesen synchronisieren.
				SwingUtilities.invokeAndWait(() -> {
					handleBufferTimedout(string);
				});
			} catch (InterruptedException | InvocationTargetException e) {
				// Wir ignorieren den Fehler fuer diese Beispiel effektiv.
				e.printStackTrace();
			}
		} else {
			// Pruefen ob es sich um einen Barcode handelt.
			if (isBarcode(string)) {
				// Wenn ja zeigen wieder diesen zusammen mit der Information in
				// unserem Label an.
				infoLabel.setText("Barcode-Scanner: " + string);
			} else {
				// Da es sich nicht um einen Barcode handelt, muessen wir die
				// abgefangenen Zeichen an die jeweilige Komponente weitergeben.
				
				// Holen der aktuell fokussierten Komponente.
				Component focusedComponent = getFocusOwner();
				
				// Hinweis: Da wir "nur" einen "fertigen" String in unserem
				// Buffer speichern muessen wir die Tastenanschlaege
				// "emulieren". Wenn die fokusierte Komponente zum Beispiel ein
				// Textfeld ist, muessen wir den Text an der richtigen Stelle
				// einfuegen.
				
				// Pruefen ob es eine Text-Komponente ist.
				if (focusedComponent instanceof JTextComponent) {
					// Umwandlung in eine Text-Komponente.
					JTextComponent focusedTextComponent = (JTextComponent)focusedComponent;
					
					// Bestimmen wo die Selektion endet und beginnt.
					//
					// Da die Selektion in beide Richtungen gehen (Links nach
					// Rechts und Rechts nach Links) muessen wir uns den
					// korrekten Start und Ende herausfinden.
					int spliceStartIndex = Math.min(
							focusedTextComponent.getSelectionStart(),
							focusedTextComponent.getSelectionEnd());
					int spliceEndIndex = Math.max(
							focusedTextComponent.getSelectionStart(),
							focusedTextComponent.getSelectionEnd());
					
					// Dann bauen wir uns aus diesem Wissen den neuen Text
					// zusammen.
					String newText = focusedTextComponent.getText().substring(0, spliceStartIndex)
							+ string
							+ focusedTextComponent.getText().substring(spliceEndIndex);
					
					// Und setzen diesen wieder auf Komponente.
					focusedTextComponent.setText(newText);
					// Zuletzte muessen wir die Position des Einfguegezeichens
					// korrigieren.
					focusedTextComponent.setCaretPosition(spliceStartIndex + string.length());
				}
				
				// Wir zeigen an dass wir dies als Tastatureingabe verarbeitet
				// haben.
				infoLabel.setText("Tastatur");
			}
		}
	}
	
	/**
	 * Handhabe das globale {@link KeyEvent}, und wenn es sich dabei um einen
	 * "Kandidaten" fuer den Scanner handelt, wird dies in den Buffer
	 * verschoben.
	 * <p>
	 * <i>Hinweis:</i> Es fehlen in diesem Beispiel Sonderfaelle, zum Beispiel
	 * wird "Strg+C" als "C" erkannt.
	 * <p>
	 * <i>Hinweis:</i> Es wird das getippte Zeichen in den Buffer verschoben.
	 * Eine andere Moeglichkeit waere das {@link KeyEvent} selbst in den Buffer
	 * zu schieben und dann nach der Zeitueberschreitung diese {@link KeyEvent}s
	 * wieder "einzuspielen".
	 * 
	 * @param keyEvent Das {@link KeyEvent}.
	 * @return {@code true} wenn die Verarbeitung von diesem Ereignis beendet
	 *         werden soll weil es verarbeitet wurde..
	 */
	protected boolean handleGlobalKeyEvent(KeyEvent keyEvent) {
		// Pruefen ob es sich um einen Tastenanschlag (Taste hinunter und wieder
		// hinauf) handelt.
		if (keyEvent.getID() == KeyEvent.KEY_TYPED) {
			// Umwandeln des getippten Zeichens in einen String.
			String typedCharacter = new String(new char[] { keyEvent.getKeyChar() });
			
			// Pruefen ob dieser String unseren Kriterien entspricht.
			if (isAlphaNumeric(typedCharacter)) {
				// Wir markieren das Ereignis als verarbeitet.
				keyEvent.consume();
				
				// Hinzufuegen des getippten Zeichens zum Buffer.
				//
				// Hinweis: Hier koennte man auch das gesamte Ereignis einfuegen
				// um dieses dann spaeter wieder abzuspielen.
				timeoutBuffer.append(typedCharacter);
				
				// Beenden der Verarbeitung von diesem Ereginis.
				return true;
			}
		}
		
		// Hinweis: Je nach Anwendungsszenario koennte es hier noch notwendig
		// sein dass man den Buffer haendisch ausleert.
		
		// Das Ereignis weiterverarbeiten lassen.
		return false;
	}
	
	/**
	 * Prueft ob der gegebene {@link String} nur aus
	 * {@link Character#isAlphabetic(int) Buchstaben} und
	 * {@link Character#isDigit(int) Zahlen} besteht.
	 * 
	 * @param string Der {@link String} welcher geprueft werden soll, kann leer
	 *        oder {@code null} sein.
	 * @return {@code true} wenn der der gegebene {@link String} nur aus
	 *         {@link Character#isAlphabetic(int) Buchstaben} und
	 *         {@link Character#isDigit(int) Zahlen} besteht.
	 */
	protected boolean isAlphaNumeric(String string) {
		// Pruefung ob wir ueberhaupt etwas machen muessen.
		if (string == null
				|| string.length() == 0) {
			return false;
		}
		
		// Wir testen jeden CodePoint in dem String.
		//
		// Hinweis: Realistischerweise wird diese Funktion nur mit Strings mit
		// einer Laenge von 1 aufgerufen, da die Funktion aber recht allgemein
		// ist, pruefen wir dem gesamten String.
		for (int codePoint : string.codePoints().toArray()) {
			// Pruefen ob der CodePoint ein Buchstabe oder eine Zahl ist.
			if (!Character.isAlphabetic(codePoint)
					&& !Character.isDigit(codePoint)) {
				// Und wenn nicht koennen wir aufhoeren zu pruefen.
				return false;
			}
		}
		
		// Alle Zeichen sind Buchstaben oder Zahlen.
		return true;
	}
	
	/**
	 * Prueft ob der gegebene {@link String} ein Barcode ist.
	 * 
	 * @param string Der {@link String} welcher geprueft werden soll.
	 * @return {@code true} wenn der gegebene {@link String} ein Barcode ist.
	 */
	protected boolean isBarcode(String string) {
		// Pruefen ob es sich um einen Barcode handelt.
		//
		// Hinweis: Wir gehen hier von alphanumerischen Barcodes aus, daher
		// findet hier keine Pruefung auf die Zeichen statt.
		return string != null
				&& string.length() == 13;
	}
}
