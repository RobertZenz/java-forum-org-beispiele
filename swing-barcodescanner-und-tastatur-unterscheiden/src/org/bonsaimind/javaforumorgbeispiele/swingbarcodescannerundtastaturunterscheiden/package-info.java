/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Swing Barcode-Scanner und Tastatur unterscheiden
 * <p>
 * Demonstriert wie man einen (USB) Barcode-Scanner und eine Eingabe durch den
 * Benutzer auf der Tastatur unterscheiden koennte. Hierbei macht man sich
 * zunutze dass der Barcode-Scanner die Zeichen um ein vielfaches schneller
 * eingibt als das der Benutzer jemals koennte.
 * <p>
 * Alle gedrueckten Zeichen werden in einem Buffer geschoben welcher erst nach
 * einer bestimmten Zeitspanne geleert wird. Anhand dessen wieviel sich im
 * Buffer befindet kann man dann darauf schlieszen ob es sich um eine
 * Benutzereingabe oder um eine Interaktion vom Barcode-Scanner handelt.
 */

package org.bonsaimind.javaforumorgbeispiele.swingbarcodescannerundtastaturunterscheiden;