/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingbarcodescannerundtastaturunterscheiden;

import java.time.Duration;
import java.util.function.Consumer;

/**
 * Der {@link TimeoutBuffer} kann {@link String}s sammeln und fuer eine
 * bestimmte Zeitspanne zwischenspeichern bevor diese gesammelt an eine gegebene
 * Funktion uebergeben werden.
 */
public class TimeoutBuffer {
	/** Der {@link StringBuilder} welchen wir als Buffer verwenden. */
	protected StringBuilder buffer = new StringBuilder();
	/**
	 * Der Zeitstempel des letzten Hinzufuegen. {@code -1} wenn nichts im Buffer
	 * ist.
	 */
	protected long lastAppendTimestamp = -1l;
	/**
	 * Die Zeit welche ueberschritten werden muss bevor der Buffer geleert wird.
	 * In Nanosekunden.
	 */
	protected long timeoutInNanoSeconds = -1l;
	/**
	 * Der {@link Thread} welcher verwendet wird um die Zeitueberschreitung zu
	 * pruefen.
	 */
	protected Thread timeoutThread = null;
	/**
	 * Ein {@code boolean} welcher verwendet wird um den {@link #timeoutThread}
	 * stillzulegen solange es nichts im Buffer gibt.
	 */
	protected volatile boolean timingThreadSuspended = true;
	/**
	 * Der {@link Consumer} an welchen der gebufferte Wert nach der
	 * Zeitueberschreitung uebergeben wird.
	 */
	protected Consumer<String> valueConsumer = null;
	
	/**
	 * Erstellt eine neue {@link TimeoutBuffer} Instanz.
	 * 
	 * @param timeout Die {@link Duration Zeitueberschreitung} nach welcher der
	 *        Buffer geleert wird.
	 * @param valueConsumer Der {@link Consumer} an welchen der Wert aus dem
	 *        Buffer uebergeben wird.
	 */
	public TimeoutBuffer(
			Duration timeout,
			Consumer<String> valueConsumer) {
		super();
		
		// Setzen der Parameter.
		this.timeoutInNanoSeconds = timeout.toNanos();
		this.valueConsumer = valueConsumer;
		
		// Erstellen des Hintergrung-Threads.
		timeoutThread = new Thread(this::runBackgroundTimeout);
		// Wir setzen diesen als "Daemon" damit dieser nicht das beenden der JVM
		// verhindert.
		timeoutThread.setDaemon(true);
		// Zusaetzlich setzen wir einen Namen.
		timeoutThread.setName("timeoutbuffer-timoutthread");
		// Und starten den Thread direkt.
		timeoutThread.start();
	}
	
	/**
	 * Fuegt den gegebenen {@link String} zum Buffer hinzu und startet oder
	 * setzen den Timeout zurueck.
	 * 
	 * @param string Der {@link String} welcher dem Buffer hinzugefuegt werden
	 *        soll.
	 * @return Diese Instanz.
	 */
	public TimeoutBuffer append(String string) {
		// Synchronisieren auf diese Instanz um gleichzeitige Aenderungen an
		// mehreren Feldern zu verhindern.
		synchronized (this) {
			// Den String dem Buffer hinzufuegen.
			buffer.append(string);
			
			// Setzen des Zeitstempels auf den aktuellen Zeitpunkt.
			lastAppendTimestamp = System.nanoTime();
			
			// Sicherstellen dass der Thread auch laeuft.
			timingThreadSuspended = false;
			synchronized (timeoutThread) {
				timeoutThread.notifyAll();
			}
		}
		
		// Retournieren der aktuellen Instanz.
		return this;
	}
	
	/**
	 * Leert den Buffer aus.
	 */
	protected synchronized void flush() {
		// Definieren des Werts welchen wir halten.
		String value = null;
		
		// Synchronisieren auf diese Instanz um gleichzeitige Aenderungen an
		// mehreren Feldern zu verhindern.
		synchronized (this) {
			// Holen des Werts aus dem Buffer.
			value = buffer.toString();
			// Ausleeren des Buffers.
			buffer.delete(0, buffer.length());
			
			// Zuruecksetzen des Zeitstempels.
			lastAppendTimestamp = -1l;
			
			// Stoppen des Threads.
			timingThreadSuspended = true;
		}
		
		// Uebergeben des Werts aus dem Buffer an die Funktion.
		valueConsumer.accept(value);
	}
	
	/**
	 * Die Hauptschleife des {@link #timeoutThread}.
	 */
	protected void runBackgroundTimeout() {
		// Wir laufen fuer immer in diesem Beispiel.
		while (true) {
			// Synchronisieren auf diese Instanz um gleichzeitige Aenderungen an
			// mehreren Feldern zu verhindern.
			synchronized (this) {
				// Pruefen ob es einen Zeitstempel gibt.
				if (lastAppendTimestamp > 0) {
					// Wenn je errechnen wir wieviel Zeit seit dem letzten
					// Hinzufuegen vergangen ist.
					long elapsedTimeInNanoSeconds = System.nanoTime() - lastAppendTimestamp;
					
					// Wenn diese Zeitspanne groeszer ist als unser Timeout,
					// koennen wir den Buffer ausleeren.
					if (elapsedTimeInNanoSeconds >= timeoutInNanoSeconds) {
						// Ausleeren des Buffers.
						flush();
					}
				}
			}
			
			try {
				// Ausbremsen der Schleife so das wir nicht 100% von einem Kern
				// belegen.
				Thread.sleep(1l);
				
				// Wir "pausieren" diesen Thread solange dieser Wert true ist.
				while (timingThreadSuspended) {
					// Hierbei verwenden wir nicht eine Schleife, sondern warten
					// darauf dass wir benachrichtigt werden dass es wieder
					// etwas zum arbeiten gibt.
					synchronized (timeoutThread) {
						// Warten auf die Benachrichtigung.
						timeoutThread.wait();
					}
				}
			} catch (InterruptedException e) {
				// Wir ignorieren den Fehler fuer diese Beispiel effektiv.
				e.printStackTrace();
			}
		}
	}
}
