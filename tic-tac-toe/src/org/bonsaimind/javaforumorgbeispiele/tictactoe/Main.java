/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.tictactoe;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Hauptklasse welche die Hauptprogrammlogik enthaelt.
 * <p>
 * {@code final} damit sich niemand von der Klasse ableitet.
 */
public final class Main {
	/**
	 * {@code private} Konstruktor damit niemand die Klasse instanziert.
	 */
	private Main() {
	}
	
	/**
	 * Haupteinstiegpunkt des Programms.
	 * 
	 * @param args Die Argumente welche dem Programm uebergeben wurden. Werden
	 *        in diesem Fall nicht verwendet (muessen aber angegeben werden
	 *        wegen der Signatur).
	 */
	public static final void main(String[] args) {
		// Erstellen eines neuen Scanners mit welchem wir die Benutzereingaben
		// von stdin lesen werden.
		Scanner scanner = new Scanner(System.in);
		
		// Eine neue Instanz des Spiels.
		Spiel spiel = new Spiel();
		
		// Solang das Spiel nicht zu Ende ist, laeuft die Hauptlogik weiter.
		//
		// Hinweis: Es gibt keine Moeglichkeit im Moment das Spiel vorzeitig
		// abzubrechen, dafuer muss das Programm beendet werden.
		while (!spiel.istSpielZuEnde()) {
			// Anzeige des Spielfelds zu Beginn von jedem Zug.
			spielfeldAusgeben(spiel);
			
			// Das Feld auf welchem der naechste Zug erfolgen soll.
			Feld naechsterZugFeld = null;
			
			// Wir werden solange den Benutzer um ein Feld fragen, bis ein
			// gueltiges Feld eingegeben wurde.
			do {
				// Den Index des Felds vom Benutzer erfragen.
				int naechsterZugIndex = leseInt(
						scanner,
						"Naechster Zug, Spieler '" + spiel.getNaechstenSpieler().getZeichen() + "'");
				// Dann das Feld zu diesem Inden holen.
				naechsterZugFeld = Feld.nachIndex(naechsterZugIndex);
				
				// Wenn es kein Feld mit diesem Index gab, liefert die obig
				// Methode null, und wir wissen dass der Benutzer keinen
				// gueltigen Index eingegeben hat.
				if (naechsterZugFeld != null) {
					// Pruefen ob das Feld war gueltig, aber bereits belegt ist.
					if (spiel.getSpieler(naechsterZugFeld) != null) {
						// Den Benutzer muessen wir darueber informieren dass
						// das Feld bereits belegt ist.
						meldungAusgeben("Das gewaehlte Feld <%s> is bereits belegt durch <%s>.\n",
								naechsterZugFeld.name(),
								spiel.getSpieler(naechsterZugFeld));
						
						// Die Variable wieder auf null setzen damit die
						// Schleife wieder von vorne beginnt.
						naechsterZugFeld = null;
					}
				} else {
					// Den Benutzer informieren dass das kein gueltiger Index
					// war.
					meldungAusgeben("Die Eingabe <%s> ist kein gueltiges Feld.\n",
							Integer.toString(naechsterZugIndex));
				}
			} while (naechsterZugFeld == null);
			
			// Im Spiel den Zug durchfuehren.
			spiel.naechstesZeichenSetzen(naechsterZugFeld);
		}
		
		// Das Spiel ist vorbei, damit der letzte Zug nocht sichtbar wird, und
		// damit auch der letzte Stand des Spiels, muessen wir das Spielfeld
		// noch einmal ausgeben.
		spielfeldAusgeben(spiel);
		
		// Den Gewinner des Spiels holen.
		Spieler gewinner = spiel.getGewinner();
		
		// Die obige Methode kann null liefern wenn es ein unentschieden war.
		if (gewinner != null) {
			// Dem Benutzer den Gewinner anzeigen.
			meldungAusgeben("Gewinner: '%s'\n",
					spiel.getGewinner().getZeichen());
		} else {
			// Dem Benutzer informieren dass niemand gewonnen hat.
			meldungAusgeben("Unentschieden\n");
		}
	}
	
	/**
	 * Liest einen {@code int} von dem gegebenen {@link Scanner} ein, gibt zuvor
	 * aber die gegebene Eingabeaufforderung aus.
	 * <p>
	 * Zur einfacheren Verwendung und damit die Hauptlogik besser lesbar und
	 * strukturiert ist, ist das lesen von der Eingabe hier weggekapselt in
	 * dieser Methode.
	 * 
	 * @param scanner Der {@link Scanner} welcher verwendet wird um den
	 *        {@code int} zu lesen.
	 * @param eingabeaufforderung Die Eingabeaufforderung welche zuvor
	 *        ausgegeben wird.
	 * @return Den gelesenen {@code int}.
	 */
	private static final int leseInt(Scanner scanner, String eingabeaufforderung) {
		// Bei erfolgreicher Eingabe retournieren wir aus der Funktion und
		// springen damit aus der Funktion. Ansonsten wollen wir den
		// Benutzer wieder und wieder aufforderen eine Eingabe zu machen.
		while (true) {
			// Eingabeaufforderungen dem Benutzer zeigen.
			//
			// Wir fuegen den Doppelpunkt und das Leerzeichen an dieser
			// Stelle ein um zu verhindern dass wir diesen bei jeder
			// Eingabeaufforderung wiederholen muessen. Solche Wiederholungen
			// haben immer das Potenzial dass man auf diese vergisst, daher ist
			// es gut solche Sachen an einer zentralen Stelle zu machen damit
			// der verwendente Code besser lesbar und wartbar ist.
			meldungAusgeben(eingabeaufforderung + ": ");
			
			try {
				// Lesen des "naechsten" ints vom InputStream.
				//
				// Dieser Aufruf kann eine InputMismatchException werfen
				// wenn die eingegebene Zahl kein int ist.
				return scanner.nextInt();
			} catch (InputMismatchException e) {
				// Es wurde kein gueltiger int eingegeben. Um den Scanner weiter
				// verwenden zu koennen muessen alles was bisher vom Benutzer
				// eingegeben wurde "verwerfen". Wir tun dies in dem wir es als
				// String lesen, und das notwendige direkt mit dem praktischen
				// verbinden und dies dann dem Benutzer zusammen mit der
				// Fehlermeldung ausgeben.
				String ungueltigeEingabe = scanner.nextLine();
				
				// Fehlermeldung ausgeben, zusammen mit dem Wert welcher der
				// Benutzer eingegeben hat.
				meldungAusgeben("Die Eingabe <%s> ist keine gueltige Zahl.\n",
						ungueltigeEingabe);
			}
		}
	}
	
	/**
	 * Gibt die gegebene Meldung an den Benutzer aus.
	 * 
	 * @param format Die Meldung.
	 * @param arguments Die Argumente fuer die Formatierung der Nachricht.
	 * @see String#format(String, Object...)
	 */
	private static final void meldungAusgeben(String format, Object... arguments) {
		System.out.format(format, arguments);
	}
	
	/**
	 * Gibt das gesamte Spielfeld an den Benutzer aus.
	 * 
	 * @param spiel Das Spiel mit dem Spielfeld welches ausgegeben werden soll.
	 */
	private static final void spielfeldAusgeben(Spiel spiel) {
		// Wir geben das Spielfeld in einer einfachen ASCII-Grafik aus welche
		// (ohne getaetigte Zuege) so aussieht:
		//
		// +-+-+-+
		// |7|8|9|
		// +-+-+-+
		// |4|5|6|
		// +-+-+-+
		// |1|2|3|
		// +-+-+-+
		//
		// Wenn ein Zug gemacht wurde, wird das entsprechende Feld mit dem Zug
		// befuellt anstelle des Index.
		meldungAusgeben("+-+-+-+\n");
		meldungAusgeben("|%s|%s|%s|\n",
				spiel.getSpielerZeichenOderIndex(Feld.LINKS_OBEN),
				spiel.getSpielerZeichenOderIndex(Feld.MITTE_OBEN),
				spiel.getSpielerZeichenOderIndex(Feld.RECHTS_OBEN));
		meldungAusgeben("+-+-+-+\n");
		meldungAusgeben("|%s|%s|%s|\n",
				spiel.getSpielerZeichenOderIndex(Feld.LINKS_MITTE),
				spiel.getSpielerZeichenOderIndex(Feld.MITTE_MITTE),
				spiel.getSpielerZeichenOderIndex(Feld.RECHTS_MITTE));
		meldungAusgeben("+-+-+-+\n");
		meldungAusgeben("|%s|%s|%s|\n",
				spiel.getSpielerZeichenOderIndex(Feld.LINKS_UNTEN),
				spiel.getSpielerZeichenOderIndex(Feld.MITTE_UNTEN),
				spiel.getSpielerZeichenOderIndex(Feld.RECHTS_UNTEN));
		meldungAusgeben("+-+-+-+\n");
	}
	
	/**
	 * Das {@link Feld} ist ein enum welcher ein Feld auf dem Spielfeld
	 * darstellt.
	 * <p>
	 * Dieses Feld hat eine {@link #getX() horizontale} und eine {@link #getY()
	 * vertikale} Koordinate als auch einen {@link #getIndex() Index}.
	 * <p>
	 * <i>Hinweis:</i> Da das Spielfeld sehr klein ist (3x3) macht es Sinn die
	 * einzelnen Felder per Hand hier zu erfassen. Wenn es Spielfeld groeszer
	 * waere, waere eine dynamischere Variante natuerlich besser welche rein auf
	 * den Koordinaten der Felder basiert.
	 */
	public static enum Feld {
		/**
		 * Das Feld in der linken Spalte in der Mitte des Spielfelds.
		 * 
		 * <pre>
		 * <code>
		 *   0 1 2
		 *  +-+-+-+
		 * 0| | | |
		 *  +-+-+-+
		 * 1|X| | |
		 *  +-+-+-+
		 * 2| | | |
		 *  +-+-+-+
		 * </code>
		 * </pre>
		 */
		LINKS_MITTE(0, 1),
		
		/**
		 * Das Feld in der linken Spalte am oberen Rand des Spielfelds.
		 * 
		 * <pre>
		 * <code>
		 *   0 1 2
		 *  +-+-+-+
		 * 0|X| | |
		 *  +-+-+-+
		 * 1| | | |
		 *  +-+-+-+
		 * 2| | | |
		 *  +-+-+-+
		 * </code>
		 * </pre>
		 */
		LINKS_OBEN(0, 0),
		
		/**
		 * Das Feld in der linken Spalte am unteren Rand des Spielfelds.
		 * 
		 * <pre>
		 * <code>
		 *   0 1 2
		 *  +-+-+-+
		 * 0| | | |
		 *  +-+-+-+
		 * 1| | | |
		 *  +-+-+-+
		 * 2|X| | |
		 *  +-+-+-+
		 * </code>
		 * </pre>
		 */
		LINKS_UNTEN(0, 2),
		
		/**
		 * Das Feld in der in der Mitte des Spielfelds.
		 * 
		 * <pre>
		 * <code>
		 *   0 1 2
		 *  +-+-+-+
		 * 0| | | |
		 *  +-+-+-+
		 * 1| |X| |
		 *  +-+-+-+
		 * 2| | | |
		 *  +-+-+-+
		 * </code>
		 * </pre>
		 */
		MITTE_MITTE(1, 1),
		
		/**
		 * Das Feld in der Mitte am oberen Rand des Spielfelds.
		 * 
		 * <pre>
		 * <code>
		 *   0 1 2
		 *  +-+-+-+
		 * 0| |X| |
		 *  +-+-+-+
		 * 1| | | |
		 *  +-+-+-+
		 * 2| | | |
		 *  +-+-+-+
		 * </code>
		 * </pre>
		 */
		MITTE_OBEN(1, 0),
		
		/**
		 * Das Feld in der Mitte am unteren Rand des Spielfelds.
		 * 
		 * <pre>
		 * <code>
		 *   0 1 2
		 *  +-+-+-+
		 * 0| | | |
		 *  +-+-+-+
		 * 1| | | |
		 *  +-+-+-+
		 * 2| |X| |
		 *  +-+-+-+
		 * </code>
		 * </pre>
		 */
		MITTE_UNTEN(1, 2),
		
		/**
		 * Das Feld in der rechten Spalte in der Mitte des Spielfelds.
		 * 
		 * <pre>
		 * <code>
		 *   0 1 2
		 *  +-+-+-+
		 * 0| | | |
		 *  +-+-+-+
		 * 1| | |X|
		 *  +-+-+-+
		 * 2| | | |
		 *  +-+-+-+
		 * </code>
		 * </pre>
		 */
		RECHTS_MITTE(2, 1),
		
		/**
		 * Das Feld in der rechten Spalte am oberen Rand des Spielfelds.
		 * 
		 * <pre>
		 * <code>
		 *   0 1 2
		 *  +-+-+-+
		 * 0| | |X|
		 *  +-+-+-+
		 * 1| | | |
		 *  +-+-+-+
		 * 2| | | |
		 *  +-+-+-+
		 * </code>
		 * </pre>
		 */
		RECHTS_OBEN(2, 0),
		
		/**
		 * Das Feld in der rechten Spalte am unteren Rand des Spielfelds.
		 * 
		 * <pre>
		 * <code>
		 *   0 1 2
		 *  +-+-+-+
		 * 0| | | |
		 *  +-+-+-+
		 * 1| | | |
		 *  +-+-+-+
		 * 2| | |X|
		 *  +-+-+-+
		 * </code>
		 * </pre>
		 */
		RECHTS_UNTEN(2, 2);
		
		/** Der Index von diesem Feld. */
		private int index = 0;
		/** Die horizontale Koordinate von diesem Feld. */
		private int x = 0;
		/** Die vertikale Koordinate von diesem Feld. */
		private int y = 0;
		
		/**
		 * Erstellt eine neue {@link Feld} Instanz mit den gegebenen
		 * Koordinaten.
		 * 
		 * @param x Die horizontale Koordinate fuer dieses Feld.
		 * @param y Die vertikale Koordinate fuer dieses Feld.
		 */
		private Feld(int x, int y) {
			this.x = x;
			this.y = y;
			
			// Wir erechnen fuer jedes Feld einen Index ueber welchen dieses
			// Feld ebenfalls angesprochen werden kann. In diesem Fall erzeugen
			// wir Index in der Form:
			//
			// 789
			// 456
			// 123
			//
			// Dies entspricht dem Nummernblock auf der Tastatur, was das
			// spielen einfacher macht. Wenn man Links Oben mit "1" beginnen
			// will, muss einfach nur das "2 - " entfernt werden:
			//
			// (y * 3) + x + 1
			index = ((2 - y) * 3) + x + 1;
		}
		
		/**
		 * Liefert das {@link Feld} fuer den gegebenen Index, {@code null} wenn
		 * dieser keinem {@link Feld} entspricht.
		 * <p>
		 * <i>Hinweis:</i> Bei derartigen Hilfsmethoden ist immer die Frage ob
		 * man {@code null} retournieren oder stattdessen eine Ausnahme werfen
		 * sollte. Die grundlegende Frage ist, ob der aufrufende Code den Wert
		 * vorher validieren und pruefen soll, oder ob dies anhand dieser
		 * Methode passieren soll.
		 * 
		 * @param index Der Index fuer welchen das {@link Feld} retourniert
		 *        werden soll.
		 * @return Das {@link Feld} fuer den gegebenen Index, {@code null} wenn
		 *         dieser keinem {@link Feld} entspricht.
		 */
		public static final Feld nachIndex(int index) {
			// Wir iterieren durch alle Felder und retournieren dass welches
			// dem gegebenen Index entspricht.
			for (Feld feld : values()) {
				if (feld.getIndex() == index) {
					return feld;
				}
			}
			
			// Wenn keines passt, geben wir null zurueck um dies zu
			// signalisieren.
			return null;
		}
		
		/**
		 * Liefert das {@link Feld} fuer die gegebenen Koordinaten, {@code null}
		 * wenn diese keinem {@link Feld} entspricht.
		 * <p>
		 * <i>Hinweis:</i> Bei derartigen Hilfsmethoden ist immer die Frage ob
		 * man {@code null} retournieren oder stattdessen eine Ausnahme werfen
		 * sollte. Die grundlegende Frage ist, ob der aufrufende Code den Wert
		 * vorher validieren und pruefen soll, oder ob dies anhand dieser
		 * Methode passieren soll.
		 * 
		 * @param x Die horizontale Koordinate des Felds.
		 * @param y Die vertikale Koordinate des Felds.
		 * @return Das {@link Feld} fuer die gegebenen Koordinaten, {@code null}
		 *         wenn dieser keinem {@link Feld} entspricht.
		 */
		public static final Feld nachKoordinaten(int x, int y) {
			// Wir iterieren durch alle Felder und retournieren dass welches
			// den gegebenen Koordinaten entspricht.
			for (Feld feld : values()) {
				if (feld.getX() == x && feld.getY() == y) {
					return feld;
				}
			}
			
			// Wenn keines passt, geben wir null zurueck um dies zu
			// signalisieren.
			return null;
		}
		
		/**
		 * Retourniert den Index fuer dieses {@link Feld}.
		 * <p>
		 * <i>Hinweis:</i> Dieser Index vermischt etwas den Zustand mit der
		 * Represaentation. Man koennte dies auch an eine Zwischenschicht
		 * delegieren, zum Beispiel "FeldIndexStrategie" welches diese errechnet
		 * fuer jedes Feld. Dann kann die Praesentation die Index der Felder
		 * bestimmen je nachdem welche Strategie verwendet wird.
		 * 
		 * @return Der Index fuer dieses {@link Feld}.
		 */
		public int getIndex() {
			return index;
		}
		
		/**
		 * Retourniert die horizontal Koordinate fuer dieses {@link Feld}.
		 * 
		 * @return Die horizontal Koordinate fuer dieses {@link Feld}.
		 */
		public int getX() {
			return x;
		}
		
		/**
		 * Retourniert die vertikale Koordinate fuer dieses {@link Feld}.
		 * 
		 * @return Die vertikale Koordinate fuer dieses {@link Feld}.
		 */
		public int getY() {
			return y;
		}
	}
	
	/**
	 * Das {@link Spiel} ist eine Klasse welche die grundlegenden Regeln und
	 * Logik fuer das Tic-Tac-Toe abbildet. Es kapselt neben den grundlegenden
	 * Regeln (Felder koennen nicht ueberschrieben werden) auch den Ablauf
	 * (welcher Spieler an der Reihe ist) und ob ein Spiel zu Ende ist.
	 * <p>
	 * <i>Hinweis:</i> Das Ende eines Spiels wird nicht in den einzelnen
	 * Methoden erzwungen. Theoretisch kann man mit dieser Klasse ein Spiel
	 * normal fortsetzen nachdem eine Partei gewonnen hat. Um dies zu verhindern
	 * muesste man entsprechende Pruefungen und Ausnahmen hinzufuegen welche
	 * geworfen werden sobald das Ende erreicht ist und jemand versucht noch
	 * einen Zug zu machen.
	 */
	public static class Spiel {
		/**
		 * Der naechste {@link Spieler} welcher an der Reihe ist.
		 * <p>
		 * <i>Hinweis:</i>: Hier haben wir das Feld mit dem entsprechenden
		 * Startspieler vorbefuellt. Diesen koennte man aber auch im Konstruktor
		 * uebernehmen von Aussen.
		 */
		protected Spieler naechsterSpieler = Spieler.X;
		/** Das Spielfeld auf welchem gespielt wird. */
		protected Spieler[][] spielfeld = new Spieler[3][3];
		
		/**
		 * Erstellt eine neue {@link Spiel} Instanz.
		 */
		public Spiel() {
			super();
		}
		
		/**
		 * Ermittel und retourniert den {@link Spieler Gewinner}, {@code null}
		 * wenn es keinen gibt.
		 * 
		 * @return Der {@link Spieler Gewinner}, {@code null} wenn es keinen
		 *         gibt.
		 */
		public Spieler getGewinner() {
			// 1. -X- ..2. --- ..3. X-- ..4. --X
			// .. -X- .... XXX .... -X- .... -X-
			// ,, -X- .... --- .... --X .... X--
			if (gleicherSpieler(Feld.MITTE_OBEN, Feld.MITTE_MITTE, Feld.MITTE_UNTEN)
					|| gleicherSpieler(Feld.LINKS_MITTE, Feld.MITTE_MITTE, Feld.RECHTS_MITTE)
					|| gleicherSpieler(Feld.LINKS_OBEN, Feld.MITTE_MITTE, Feld.RECHTS_UNTEN)
					|| gleicherSpieler(Feld.RECHTS_OBEN, Feld.MITTE_MITTE, Feld.LINKS_UNTEN)) {
				return getSpieler(Feld.MITTE_MITTE);
			}
			
			// 1. XXX ..2. X--
			// .. --- .... X--
			// .. --- .... X--
			if (gleicherSpieler(Feld.LINKS_OBEN, Feld.MITTE_OBEN, Feld.RECHTS_OBEN)
					|| gleicherSpieler(Feld.LINKS_OBEN, Feld.LINKS_MITTE, Feld.LINKS_UNTEN)) {
				return getSpieler(Feld.LINKS_OBEN);
			}
			
			// 1. --- ..2. --X
			// .. --- .... --X
			// .. XXX .... --X
			if (gleicherSpieler(Feld.RECHTS_UNTEN, Feld.MITTE_UNTEN, Feld.LINKS_UNTEN)
					|| gleicherSpieler(Feld.RECHTS_UNTEN, Feld.RECHTS_MITTE, Feld.RECHTS_OBEN)) {
				return getSpieler(Feld.RECHTS_UNTEN);
			}
			
			return null;
		}
		
		/**
		 * Retourniert den {@link Spieler} welcher als naechstes an der Reihe
		 * ist.
		 * 
		 * @return deR {@link Spieler} welcher als naechstes an der Reihe ist
		 */
		public Spieler getNaechstenSpieler() {
			return naechsterSpieler;
		}
		
		/**
		 * Retourniert den {@link Spieler} welcher in das gegebene {@link Feld}
		 * gesetzt hat, {@code null} falls das {@link Feld} frei ist.
		 * 
		 * @param feld Das {@link Feld} von welchem der {@link Spieler} geholt
		 *        werden soll.
		 * @return Der {@link Spieler} welcher in das gegebene {@link Feld}
		 *         gesetzt hat, {@code null} falls das {@link Feld} frei ist.
		 */
		public Spieler getSpieler(Feld feld) {
			return spielfeld[feld.getX()][feld.getY()];
		}
		
		/**
		 * Retourniert das {@link Spieler#getZeichen() Zeichen des Spielers}
		 * welcher in das gegebene {@link Feld} gesetzt hat, wenn das
		 * {@link Feld} frei ist, wird stattdessen der {@link Feld#getIndex()
		 * Index des Feldes} retourniert.
		 * 
		 * @param feld Das {@link Feld} welches abgefragt werden soll.
		 * @return Das {@link Spieler#getZeichen() Zeichen des Spielers} welcher
		 *         in das gegebene {@link Feld} gesetzt hat, wenn das
		 *         {@link Feld} frei ist stattdessen der {@link Feld#getIndex()
		 *         Index des Feldes}.
		 */
		public String getSpielerZeichenOderIndex(Feld feld) {
			Spieler spieler = getSpieler(feld);
			
			if (spieler != null) {
				return spieler.getZeichen();
			} else {
				return Integer.toString(feld.getIndex());
			}
		}
		
		/**
		 * Retourniert ob alle {@link Feld}er belegt sind und somit kein Zug
		 * mehr moeglich ist.
		 * 
		 * @return Ob alle {@link Feld}er belegt sind und somit kein Zug mehr
		 *         moeglich ist.
		 */
		public boolean istKeinZugMehrMoeglich() {
			// Wir koennte auch ueber das Array iterieren und die Felder direkt
			// pruefen. Diese Loesung ist aber etwas angemehmer zu lesen.
			for (Feld feld : Feld.values()) {
				if (getSpieler(feld) == null) {
					// Wenn das Feld keinen Spieler hat ist es frei, in dem Fall
					// koennen wir direkt retournieren weil es noch zumindest
					// ein freies Feld gibt.
					return false;
				}
			}
			
			return true;
		}
		
		/**
		 * Retourniert ob das Spiel zu Ende ist, also ob es einen
		 * {@link #getGewinner() Gewinner} gibt oder ob
		 * {@link #istKeinZugMehrMoeglich() kein Zug mehr moeglich} ist.
		 * 
		 * @return Ob das Spiel zu Ende ist.
		 */
		public boolean istSpielZuEnde() {
			return getGewinner() != null
					|| istKeinZugMehrMoeglich();
		}
		
		/**
		 * Macht den naechsten Zug und setzt den {@link #getNaechstenSpieler()
		 * naechsten Spieler} in das gegebene {@link Feld}. Retourniert
		 * {@code true} wenn der Zug ausgefuehrt werden konnte, ansonsten
		 * {@code false}.
		 * 
		 * @param feld Das {@link Feld} in welches der
		 *        {@link #getNaechstenSpieler() naechste Spieler} setzt.
		 * @return {@code true} wenn der Zug ausgefuehrt werden konnte,
		 *         ansonsten {@code false}.
		 */
		public boolean naechstesZeichenSetzen(Feld feld) {
			// Pruefung ob das Feld noch frei ist.
			if (spielfeld[feld.x][feld.y] != null) {
				return false;
			}
			
			// Setzen des Zugs.
			spielfeld[feld.x][feld.y] = naechsterSpieler;
			
			// Umschalten auf den anderen Spieler.
			if (naechsterSpieler == Spieler.X) {
				naechsterSpieler = Spieler.O;
			} else {
				naechsterSpieler = Spieler.X;
			}
			
			// Operation erfolgreich.
			return true;
		}
		
		/**
		 * Hilfsmethode zum pruefen ob die drei gegebenen {@link Feld}er von dem
		 * selben {@link Spieler} besetzt sind.
		 * 
		 * @param feld1 Das erste {@link Feld}.
		 * @param feld2 Das zweite {@link Feld}.
		 * @param feld3 Das dritte {@link Feld}.
		 * @return {@code true} wenn alle drei gegebenen {@link Feld}er vom
		 *         selben {@link Spieler} besetzt sind.
		 */
		protected boolean gleicherSpieler(Feld feld1, Feld feld2, Feld feld3) {
			// Wenn das erste Feld bereits leer ist, brauchen wir auch nichts
			// weiter pruefen. Diese Pruefung verhindert auch ein falsches
			// erkennen falls alle drei Felder leer sind (null == null == null).
			if (getSpieler(feld1) == null) {
				return false;
			}
			
			// Pruefen ob die drei Felder den selben Spieler enthalten.
			return getSpieler(feld1) == getSpieler(feld2)
					&& getSpieler(feld2) == getSpieler(feld3);
		}
	}
	
	/**
	 * Der {@link Spieler} ist ein enum welcher einen Spieler darstellt.
	 */
	public static enum Spieler {
		/** Der Spieler "O". */
		O("O"),
		
		/** Der Spieler "X". */
		X("X");
		
		/** Das Zeichen des Spielers. */
		private String zeichen = null;
		
		/**
		 * Erstellt eine neue {@link Spieler} Instanz mit dem gegebenen Zeichen.
		 * 
		 * @param zeichen Das Zeichen fuer den Spieler.
		 */
		private Spieler(String zeichen) {
			this.zeichen = zeichen;
		}
		
		/**
		 * Reoturniert das Zeichen fuer den {@link Spieler}.
		 * <p>
		 * <i>Hinweis:</i> Das Zeichen hier zu haben bricht etwas die Trennung
		 * zwischen Zustand und Praesentation. Eigentlich sollte die
		 * Praesentation dies auswaehlen, wir haben es hier jetzt der
		 * Einfachheit halber aufgenommen.
		 * 
		 * @return Das Zeichen fuer den {@link Spieler}.
		 */
		public String getZeichen() {
			return zeichen;
		}
	}
}
