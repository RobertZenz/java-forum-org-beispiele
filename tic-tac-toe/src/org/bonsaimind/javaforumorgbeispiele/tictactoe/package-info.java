/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Tic-Tac-Toe
 * <p>
 * Das Tic-Tac-Toe besteht aus einer
 * {@link org.bonsaimind.javaforumorgbeispiele.tictactoe.Main Main} Klasse
 * welche die gesamte Logik und alle weiteren Klassen enthaelt. Die Hauptlogik
 * befindet sich direkt in
 * {@link org.bonsaimind.javaforumorgbeispiele.tictactoe.Main#main(String[])
 * Main.main(String[])}, dort wir das Spiel gestartet und die Hauptschleife des
 * Spiels laeuft dort. Die Benutzereingaben als auch die Ausgabe des Spiels
 * selbst sind in Hilfsfunktionen gekapselt. Dabei wurde darauf geachtet dass
 * die eigentliche Programmlogik einfach zu lesen udn nachzuvollziehen ist.
 * 
 * @see <a href="https://de.wikipedia.org/wiki/Tic-Tac-Toe">Wikipedia.de:
 *      Tic-Tac-Toe</a>
 */

package org.bonsaimind.javaforumorgbeispiele.tictactoe;