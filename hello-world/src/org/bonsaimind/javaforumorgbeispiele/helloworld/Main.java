/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.helloworld;

/**
 * Hauptklasse welche alle anderen Klassen und die Hauptprogrammlogik enthaelt.
 * <p>
 * {@code final} damit sich niemand von der Klasse ableitet.
 */
public final class Main {
	/**
	 * {@code private} Konstruktor damit niemand die Klasse instanziert.
	 */
	private Main() {
	}
	
	/**
	 * Haupteinstiegpunkt des Programms.
	 * 
	 * @param args Die Argumente welche dem Programm uebergeben wurden. Werden
	 *        in diesem Fall nicht verwendet (muessen aber angegeben werden
	 *        wegen der Signatur).
	 */
	public static final void main(String[] args) {
		// Ausgeben des Strings "Hello World" auf stdout.
		System.out.println("Hello World!");
	}
}
