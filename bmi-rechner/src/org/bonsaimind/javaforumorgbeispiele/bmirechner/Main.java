/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.bmirechner;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Hauptklasse welche alle anderen Klassen und die Hauptprogrammlogik enthaelt.
 * <p>
 * {@code final} damit sich niemand von der Klasse ableitet.
 */
public final class Main {
	/**
	 * {@code private} Konstruktor damit niemand die Klasse instanziert.
	 */
	private Main() {
	}
	
	/**
	 * Haupteinstiegpunkt des Programms.
	 * 
	 * @param args Die Argumente welche dem Programm uebergeben wurden. Werden
	 *        in diesem Fall nicht verwendet (muessen aber angegeben werden
	 *        wegen der Signatur).
	 */
	public static final void main(String[] args) {
		// Instanzieren eines neuen Eingabelesers welchen wir verwenden werden
		// um die Benutzereingaben zu erhalten.
		Eingabeleser eingabeleser = new Eingabeleser(System.out, System.in);
		
		// Lesen der Parameter vom Benutzer.
		double koerpergroeszeInMeter = eingabeleser.leseDouble("Körpergröße (cm)") / 100.0d;
		double gewichtInKilogramm = eingabeleser.leseDouble("Gewicht (kg)");
		
		// Instanzieren eines neuen BmiRechners.
		BmiRechner bmiRechner = new BmiRechner();
		
		// Berechnen des BMIs und holen der zugehoerigen Klassifikation.
		double bmi = bmiRechner.berechne(gewichtInKilogramm, koerpergroeszeInMeter);
		GewichtsKlassifikation gewichtsKlassifikation = bmiRechner.getGewichtsKlassifikation(bmi);
		
		// Ausgeben des BMI und der Klassifikation.
		System.out.println("BMI: " + String.format("%,.1f", Double.valueOf(bmi)));
		System.out.println("Gruppe: " + gewichtsKlassifikation.getGruppe());
		System.out.println("Beschreibung: " + gewichtsKlassifikation.getBeschreibung());
	}
	
	/**
	 * Die {@link BmiRechner} Klasse dient der Kapselung der Berechnung eines
	 * BMIs.
	 * <p>
	 * Zusaetzlich erlaubt sie es zusaetzliche Informationen ueber den BMI zu
	 * bekommen, in diesem Fall die Gewichts-Klassifikation basierend auf einem
	 * bereits berechneten BMI.
	 * <p>
	 * <i>Hinweis:</i> Die Kapselung in einer Instanz-Klasse ist besser als in
	 * einer Statischen-Klasse da man dann die verwendete Instanz austauschen
	 * kann. Zum Beispiel bei Tests koennte man diese Instanz mocken, was um
	 * einiges einfacher und deterministischer geht als bei einer
	 * Statischen-Klasse, ebenso koennte man dadurch eine Klasse fuer Maenner
	 * und fuer Frauen mit unterschiedlichen Klassifikationen ableiten.
	 */
	public static class BmiRechner {
		/**
		 * Die {@link GewichtsKlassifikation}en welche bekannt sind.
		 * <p>
		 * <i>Hinweis:</i> Ein Array kann nicht vor Veraenderungen geschuetzt
		 * werden, auch nicht wenn es als {@code final} definiert wurde. Dies
		 * liegt daran dass bei einer {@code final} Variable der Inhalt der
		 * Variable nicht veraendert werden kann, das Objekt welches dahinter
		 * liegt aber sehr wohl, weil es nichts unmittelbar mit der Variable zu
		 * tun hat. Man kann also effektiv nichts zuweisen, aber sehr wohl den
		 * Zustand veraendern. Ein ableitende Klasse koennte also Eintraege in
		 * diesem Array ersetzen. Um sich vor solchen Veraenderungen zu
		 * schuetzen kann man anstelle eines Arrays auch eine {@link List}
		 * einsetzen, man erzeugt sich dann eine {@link List} Instanz und setzt
		 * diese als unveraenderbare {@link List} auf die {@code final}
		 * Variable.
		 * 
		 * <pre>
		 * <code>
		 * protected static final List<GewichtsKlassifikation> GEWICHTS_KLASSIFIKATIONEN;
		 * 
		 * static {
		 * 	List<GewichtsKlassifikation> gewichtsKlassifikationen = new ArrayList();
		 * 	gewichtsKlassifikationen.add(new GewichtsKlassifikation(10.0d, "Tod", "meist nicht mit Überleben vereinbar"));
		 * 	// ...
		 * 	
		 * 	GEWICHTS_KLASSIFIKATIONEN = Collections.unmodifiableList(gewichtsKlassifikationen);
		 * }
		 * </code>
		 * </pre>
		 * 
		 * Damit hat man eine unveraenderbare Variable deren Inhalt auch nicht
		 * mehr veraendert werden kann.
		 */
		protected static final GewichtsKlassifikation[] GEWICHTS_KLASSIFIKATIONEN = new GewichtsKlassifikation[] {
				new GewichtsKlassifikation(10.0d, "Tod", "meist nicht mit Überleben vereinbar"),
				new GewichtsKlassifikation(12.0d, "Untergewicht", "akute Lebensgefahr"),
				new GewichtsKlassifikation(13.0d, "Untergewicht", "hochgradiges Untergewicht Grad II"),
				new GewichtsKlassifikation(16.0d, "Untergewicht", "hochgradiges Untergewicht Grad I"),
				new GewichtsKlassifikation(17.0d, "Untergewicht", "Mäßiges Untergewicht"),
				new GewichtsKlassifikation(17.5d, "Untergewicht", "Anorektisches Gewicht"),
				new GewichtsKlassifikation(18.5d, "Untergewicht", "Leichtes Untergewicht"),
				new GewichtsKlassifikation(25.0d, "Normalgewicht", "Normalgewicht"),
				new GewichtsKlassifikation(30.0d, "Übergewicht", "Präadipositas"),
				new GewichtsKlassifikation(35.0d, "Adipositas behandlungsbedürftig", "Adipositas Grad I"),
				new GewichtsKlassifikation(40.0d, "Adipositas behandlungsbedürftig", "Adipositas Grad II"),
				new GewichtsKlassifikation(Double.MAX_VALUE, "Adipositas behandlungsbedürftig", "Adipositas Grad III")
		};
		
		/**
		 * Erstelle eine neue Instanz von {@link BmiRechner}.
		 */
		public BmiRechner() {
			super();
		}
		
		/**
		 * Berechnet den BMI aus den gegebenen Parametern.
		 * 
		 * @param gewichtInKilogramm Das Gewicht der Person in Kilogramm, zum
		 *        Beispiel {@code 82.3d}. Muss eine positive Zahl sein.
		 * @param koerpergroeszeInMeter Die Koerpergroesze der Person in Meter,
		 *        zum Beispiel {@code 1.90d}. Muss eine positive Zahl sein.
		 * @return Den berechneten BMI, immer eine positive Zahl.
		 * @throws IllegalArgumentException Wenn {@code gewichtInKilogramm} oder
		 *         {@code koerpergroeszeInMeter} keine positive Zahl oder Null
		 *         ist.
		 */
		public double berechne(double gewichtInKilogramm, double koerpergroeszeInMeter) {
			// Parameter-Pruefungen sollten immer als erstes in einer Funktion
			// stattfinden, zum einen um den Code uebersichtlich zu halten (alle
			// Pruefungen sind immer am Anfang) und zum anderen um das unnoetige
			// oder sogar schadhafte ausfuehren von Logik zu verhindern.
			
			if (gewichtInKilogramm <= 0.0d) {
				// Bie Fehlermeldungen ist es wichtig immer so explizit wie
				// moeglich zu sein. Im Falle von IllegalArgumentException ist
				// es eine sehr grosze Hilfe wenn man sowohl den Namen des
				// Parameters als auch dessen tatsaechlichen Wert, zusammen mit
				// dem erwarteten Wert, angibt. Damit verkuerzt man deutlich die
				// Zeit welche ein Entwickler braucht um die Fehlermeldung zu
				// verstehen und die moeglichen Ursachen dafuer einzugrenzen.
				//
				// Die Verwendung von Anfuehrungszeichen ("") um den Namen des
				// Parameters und von spitzen Klammern (<>) um den Wert ist eine
				// persoenliche Praeferenz von meiner Seite. Das "umklammern"
				// des Wertes hat den Vorteil das eventuelle Weiszzeichen auch
				// immer "erkennbar" gemacht werden.
				throw new IllegalArgumentException(String.format("Der Parameter \"gewichtInKilogramm\" muss eine positive Zahl sein, war aber <%f>.",
						Double.valueOf(gewichtInKilogramm)));
			}
			
			// Wenn sich viele Pruefungen wiederholen, bietet es sich an eine
			// eigene Klasse fuer diese anzulegen. Zum Beispiel mit dem Namen
			// "Require" und dann mit der Funktion "positiveNonZero" welche
			// den Parameternamen und den Parameterwert uebernimmt.
			if (koerpergroeszeInMeter <= 0.0d) {
				throw new IllegalArgumentException(String.format("Der Parameter \"koerpergroeszeInMeter\" muss eine positive Zahl sein, war aber <%f>.",
						Double.valueOf(koerpergroeszeInMeter)));
			}
			
			// Die Formel fur dem BMI ist bmi=m/l^2.
			return gewichtInKilogramm / (koerpergroeszeInMeter * koerpergroeszeInMeter);
		}
		
		/**
		 * Retourniert die {@link GewichtsKlassifikation} fuer den gegebenen
		 * BMI.
		 * 
		 * @param bmi Der BMI fuer welchen die {@link GewichtsKlassifikation}
		 *        geholt werden soll.
		 * @return Die {@link GewichtsKlassifikation} fuer den gegebenen BMI,
		 *         nie {@code null}.
		 */
		public GewichtsKlassifikation getGewichtsKlassifikation(double bmi) {
			for (GewichtsKlassifikation gewichtsKlassifikation : GEWICHTS_KLASSIFIKATIONEN) {
				// Die Gewichtsklassifikationen sind alle aufsteigend sortiert,
				// daher koennen wir diese in Reihe durchgehen und muessen nur
				// kontrollieren ob unser Wert kleiner als der aktuelle ist.
				if (bmi < gewichtsKlassifikation.getMaximalerBmi()) {
					return gewichtsKlassifikation;
				}
			}
			
			// Sollte nicht erreichbar sein solange es eine
			// GewichtsKlassifikation mit einem maximalen BMI von
			// Double.MAX_VALUE gibt. Der Compiler kann dies aber natuerlich
			// nicht wissen, daher muessen wir hier etwas retournieren.
			//
			// Alternativ koennte man hier auch eine IllegalStateException
			// werfen falls dies als Fehlerzustand angesehen werden sollte.
			return GEWICHTS_KLASSIFIKATIONEN[GEWICHTS_KLASSIFIKATIONEN.length - 1];
		}
	}
	
	/**
	 * Der {@link Eingabeleser} ist eine Hilfsklasse fuer das lesen von
	 * Benutzereingaben von der Konsole.
	 * <p>
	 * Durch das Kapseln der Eingabe in eine eigene Klasse erreicht man zum
	 * einen eine Gruppierung von Code welcher nicht unmittelbar etwas mit der
	 * eigentlichen Programmlogik zu tun hat, als auch die Moeglichkeit den
	 * eigentlichen Programmcode sauberer und besser lesbar zu gestalten.
	 * <p>
	 * <i>Hinweis:</i> Man koennte die Kapselung der Benutzereingaben auch
	 * verstaerken in dem man hier explizite Methoden fuer das lesen von Gewicht
	 * und Koerpergroesze einbaut sowie die Streams nicht mehr als Parameter
	 * uebernimmt. Die Klasse wuerde dann in etwa so aussehen:
	 * 
	 * <pre>
	 * <code>
	 * public static class EingabeLeser {
	 * 	public Eingabeleser();
	 * 	
	 * 	public double leseGewicht();
	 * 	public double leseKoerpergroesze();
	 * }
	 * </code>
	 * </pre>
	 * 
	 * Dies haette dann den Vorteil dass die Eingaben nicht von der Konsole
	 * kommen muessen, sondern man koennte dann eine Klasse machen um von der
	 * Konsole zu lesen, und eine um die Parameter aus den Argumenten des
	 * Programms zu holen.
	 */
	public static class Eingabeleser {
		/**
		 * Der {@link Scanner} welcher aus dem gegebenen {@link InputStream}
		 * liest.
		 */
		protected Scanner inputStreamScanner = null;
		/**
		 * Der {@link PrintStream} auf welcher die Eingabeaufforderungen und die
		 * Fehlermeldungen ausgegeben werden.
		 */
		protected PrintStream outputPrintStream = null;
		
		/**
		 * Erstellt eine neue Instance von {@link Eingabeleser}.
		 * 
		 * @param outputStream Der {@link OutputStream} welcher fuer die Ausgabe
		 *        von Eingabeaufforderungen und Fehlermeldungen verwendet wird.
		 *        Darf nicht {@code null} sein.
		 * @param inputStream Der {@link InputStream} welcher fuer das lesen der
		 *        Benutzereingaben verwendet wird. Darf nicht {@code null} sein.
		 * @throws IllegalArgumentException Wenn {@code outputStream} oder
		 *         {@code inputStream} {@code null} ist.
		 */
		public Eingabeleser(OutputStream outputStream, InputStream inputStream) {
			super();
			
			// Auch diese Pruefungen koennte man schon ein einer eigene Klasse
			// dafuer ablegen, und dann zum Beispiel durch Require.nonNull(...)
			// ersetzen.
			//
			// Derartige Pruefungen sind meistens redundant, da die Klassen
			// weiter unten bereits einen Fehler werfen wuerden wenn diese
			// Parameter null sind. Aber es ist sehr hilfreich wenn man
			// derartige Fehler fruehzeitig abfaengt und somit dem Programmierer
			// auch die Moeglichkeit gibt schnell und einfach zu erkennen wo der
			// Fehler auftritt und weshalb. Je weiter unten im Stapel der Fehler
			// geworfen wird, desto mehr Stufen muss der Programmierer bei der
			// Suche nach der Ursache zuruecksteigen um diese zu finden.
			if (outputStream == null) {
				throw new IllegalArgumentException("Der Parameter \"outputStream\" muss einen Wert haben, war aber <null>.");
			}
			
			if (inputStream == null) {
				throw new IllegalArgumentException("Der Parameter \"inputStream\" muss einen Wert haben, war aber <null>.");
			}
			
			this.outputPrintStream = new PrintStream(outputStream);
			this.inputStreamScanner = new Scanner(inputStream);
		}
		
		/**
		 * Liest einen {@code double} vom Benutzer.
		 * <p>
		 * Diese Funktion fordert den Benutzer zur Eingabe auf bis dieser einen
		 * gueltigen {@code double} eingegeben hat.
		 * <p>
		 * <i>Hinweis:</i> Obwohl die nachfolgende Logik in {@link BmiRechner}
		 * positive Zahlen braucht kann diese Funktion auch negative Zahlen
		 * zurueckliefern. Es wuerde sich daher anbieten diese Funktion so
		 * abzuaendern dass sie nur positive Zahlen liefern kann. In dem Fall
		 * sollte sie dann umbenannt werden in zum Beispiel
		 * {@code lesePositivenDouble} um dies zu verdeutlichen.
		 * 
		 * @param eingabeaufforderung Die Eingabeaufforderungen welche angezeigt
		 *        werden soll bevor der Benutzer die Eingabe macht.
		 * @return Den vom Benutzer eingegebenen Wert.
		 */
		public double leseDouble(String eingabeaufforderung) {
			// Bei erfolgreicher Eingabe retournieren wir aus der Funktion und
			// springen damit aus der Funktion. Ansonsten wollen wir den
			// Benutzer wieder und wieder aufforderen eine Eingabe zu machen.
			while (true) {
				// Eingabeaufforderungen dem Benutzer zeigen.
				//
				// Wir fuegen den Doppelpunkt und das Leerzeichen an dieser
				// Stelle ein um zu verhindern dass wir diesen bei jeder
				// Eingabeaufforderung wiederholen muessen. Solche
				// Wiederholungen haben immer das Potenzial dass man auf diese
				// vergisst, daher ist es gut solche Sachen an einer zentralen
				// Stelle zu machen damit der verwendente Code besser lesbar und
				// wartbar ist.
				outputPrintStream.print(eingabeaufforderung + ": ");
				
				try {
					// Lesen des "naechsten" doubles vom InputStream.
					//
					// Dieser Aufruf kann eine InputMismatchException werfen
					// wenn die eingegebene Zahl kein double ist.
					return inputStreamScanner.nextDouble();
				} catch (InputMismatchException e) {
					// Es wurde kein gueltiger double eingegeben. Um den Scanner
					// weiter verwenden zu koennen muessen alles was bisher vom
					// Benutzer eingegeben wurde "verwerfen". Wir tun dies in
					// dem wir es als String lesen, und das notwendige direkt
					// mit dem praktischen verbinden und dies dann dem Benutzer
					// zusammen mit der Fehlermeldung ausgeben.
					String ungueltigeEingabe = inputStreamScanner.nextLine();
					
					// Fehlermeldung ausgeben, zusammen mit dem Wert welcher der
					// Benutzer eingegeben hat.
					outputPrintStream.println("Die Eingabe <" + ungueltigeEingabe + "> ist keine gueltige Zahl.");
				}
			}
		}
	}
	
	/**
	 * Die {@link GewichtsKlassifikation} ist ein einfaches POJO (Plain Old Java
	 * Object) welches die Eigenschaften einer Klassifikation passend zu einem
	 * BMI haelt. Die Instanzen koennen nicht veraendert werden.
	 */
	public static class GewichtsKlassifikation {
		/** Die Beschreibung der Klassifikation. */
		protected String beschreibung = null;
		/** Die Gruppe der Klassifikation. */
		protected String gruppe = null;
		/** Der maximale BMI Wert fuer diese Klassifikation (exklusive). */
		protected double maximalerBmi = 0.0d;
		
		/**
		 * Erstellt eine neue Instanz von {@link GewichtsKlassifikation}.
		 * 
		 * @param maximalerBmi Der maximale BMI Wert fuer diese Klassifikation
		 *        (exklusive).
		 * @param gruppe Die Gruppe welcher diese Klassifikation angehoert.
		 * @param beschreibung Die Beschreibung fuer diese Klassifikation.
		 */
		public GewichtsKlassifikation(double maximalerBmi, String gruppe, String beschreibung) {
			super();
			
			// Wir verzichten hier auf eine Pruefung der Parameter da es sich um
			// eine reine Datenklasse handelt, welche beliebige Werte halten
			// darf.
			
			this.maximalerBmi = maximalerBmi;
			this.gruppe = gruppe;
			this.beschreibung = beschreibung;
		}
		
		/**
		 * Die Beschreibung fuer diese Klassifikation.
		 * 
		 * @return Die Beschreibung fuer diese Klassifikation.
		 */
		public String getBeschreibung() {
			return beschreibung;
		}
		
		/**
		 * Die Gruppe fuer diese Klassifikation.
		 * 
		 * @return Die Gruppe fuer diese Klassifikation.
		 */
		public String getGruppe() {
			return gruppe;
		}
		
		/**
		 * Der maximale BMI Wert fuer diese Klassifikation (exklusive).
		 * 
		 * @return Der maximale BMI Wert fuer diese Klassifikation (exklusive).
		 */
		public double getMaximalerBmi() {
			return maximalerBmi;
		}
	}
}
