/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * String Analysieren
 * <p>
 * Ein simples Beispiel welches aufzeigt wie ein {@link java.lang.String String}
 * aufgebaut ist und wie die einzelnen Methoden eines {@link java.lang.String
 * String}s sich in Zusammenhang mit speziellen Unicode-CodePoints verhalten.
 * <p>
 * Die
 * {@link org.bonsaimind.javaforumorgbeispiele.stringanalysieren.Main#main(String[])}
 * Methode akzeptiert eine beliebige Anzahl an Parametern welche (mit
 * Leerzeichen getrennt) zu einem Text
 * {@link java.lang.String#join(CharSequence, CharSequence...) zusammengefasst
 * werden}. Dieser Text wird dann in unterschiedlichen Varianten zerlegt und
 * ausgegeben.
 * <p>
 * <ul>
 * <li>Text so wie er ist.</li>
 * <li>Die rohen Bytes aus welchem der Text besteht(UTF-8 Enkodierung
 * angenommen).</li>
 * <li>Einzelne Characters des Texts.</li>
 * <li>Einzelne CodePoints des Texts.</li>
 * <li>Einzelne Zeichen des Texts.</li>
 * <li>Einzelne Zeichen, unter Beachtung von CodePoints, des Texts.</li>
 * </ul>
 * 
 * @see <a href=
 *      "https://robertzenz.gitlab.io/java-wiederholungen/03-strings.html#enkodierung">Java
 *      Wiederholung zum Thema Strings - Enkodierung</a>
 */

package org.bonsaimind.javaforumorgbeispiele.stringanalysieren;