/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.stringanalysieren;

import java.nio.charset.StandardCharsets;

/**
 * Hauptklasse welche die Hauptprogrammlogik enthaelt.
 * <p>
 * {@code final} damit sich niemand von der Klasse ableitet.
 */
public final class Main {
	/**
	 * {@code private} Konstruktor damit niemand die Klasse instanziert.
	 */
	private Main() {
	}
	
	/**
	 * Haupteinstiegpunkt des Programms.
	 * 
	 * @param args Der Text welcher analysiert werden soll. Mehrere Argumente
	 *        werden mit Leerzeichen " " zu einem Text zussamengefasst.
	 */
	public static final void main(String[] args) {
		// Zusammenfassen der Argumente.
		String text = String.join(" ", args);
		
		// Ausgeben der Analyse.
		
		System.out.println("Text:");
		System.out.println(text);
		System.out.println();
		
		System.out.println("Bytes (UTF-8):");
		System.out.println(toByteString(text));
		System.out.println();
		
		System.out.println("Characters:");
		System.out.println(toCharacterString(text));
		System.out.println();
		
		System.out.println("CodePoints:");
		System.out.println(toCodePointString(text));
		System.out.println();
		
		System.out.println("Substrings (Naiv):");
		System.out.println(toSubstringStringNaive(text));
		System.out.println();
		
		System.out.println("Substrings (CodePoints beachtend):");
		System.out.println(toSubstringStringCodePointAware(text));
	}
	
	/**
	 * Konvertiert das gegebene {@code byte} in einen Hex-String mit fuehrendem
	 * "0x".
	 * 
	 * @param piece Das {@code byte} zum umwandeln in einen Hex-String.
	 * @return Das gegebene {@code byte} in einen Hex-String mit fuehrendem
	 *         "0x".
	 */
	private static final String convertByteToString(byte piece) {
		// Umwandeln in einen int, da die Hilfsfunktion nur fuer einen int
		// verfuegbar ist.
		int pieceAsInt = (int)piece;
		// Ruecksetzen des Vorzeichens (linkestes Bit).
		pieceAsInt = pieceAsInt & 0xff;
		
		// Wenn der Wert weniger 0x10 ist, haengen wir eine fuehrende Null an
		// damit der String immer aus zwei Zeichen besteht.
		if (pieceAsInt < 0x10) {
			return "0x0" + Integer.toHexString(pieceAsInt);
		} else {
			return "0x" + Integer.toHexString(pieceAsInt);
		}
	}
	
	/**
	 * Konvertiert den gegebenen Unicode CodePoint in seine
	 * String-Repraesentation, zum Beispiel "U+A0C".
	 * 
	 * @param codePoint Der Unicode CodePoint welcher umgewandelt werden soll.
	 * @return Den gegebenen Unicode CodePoint in seiner String-Repraesentation,
	 *         zum Beispiel "U+A0C".
	 */
	private static final String convertCodePointToString(int codePoint) {
		// Einen neuen StringBuilder fuer das zusammen bauen der
		// Repraesentation.
		//
		// Hinweis: Ob hier ein StringBuilder von Noeten ist oder nicht, wird
		// Leistungstechnisch keine Rolle spielen.
		StringBuilder codePointStringBuilder = new StringBuilder()
				.append("U+");
		
		// Eine etwaige fuehrende Null wird nur bei den nicht fuehrenden Werten
		// abgeschnitten.
		boolean firstPrintedCodePoint = true;
		
		// Wir iterieren ueber alle vier Bytes des int.
		for (int offset : new int[] { 24, 16, 8, 0 }) {
			// Extrahieren des Bytes in dem wir nach rechts shiften und dann
			// alles andere auszer dem Byte Nullen.
			int piece = (codePoint >>> offset) & 0xff;
			
			// Nur in den String aufnehmen wenn der Wert groeszer Null ist.
			if (piece > 0) {
				// Wenn der Wert kleiner 0x10 muss ein vorangehende Null
				// angehaengt werden, auszer es ist der ertse Nicht-Null Wert.
				if (piece < 0x10 && !firstPrintedCodePoint) {
					// Vorangehende Null anstellen.
					codePointStringBuilder.append("0");
				}
				
				// Wir haben an dieser Stelle auf jeden Fall einen Wert
				// verarbeitet.
				firstPrintedCodePoint = false;
				
				// Umwandeln des Werts einen Hex-String.
				String hexString = Integer.toHexString(piece).toUpperCase();
				
				// Anhaengen des Hex-Strings.
				codePointStringBuilder.append(hexString);
			}
		}
		
		// Reoturnieren der Unicode Repraesentation.
		return codePointStringBuilder.toString();
	}
	
	/**
	 * Entfernt ein nachfolgendes Leerzeichen aus dem gegebenen
	 * {@link StringBuilder}.
	 * 
	 * @param stringBuilder Der {@link StringBuilder} von welchem ein
	 *        nachfolgendes Leerzeichen entfernt werden soll.
	 */
	private static final void removeTrailingSpace(StringBuilder stringBuilder) {
		// Pruefen ob das letzte Zeichen ein Leerzeichen ist.
		if (stringBuilder.lastIndexOf(" ") == stringBuilder.length() - 1) {
			// Und wenn ja, entfernen wir dieses.
			stringBuilder.delete(stringBuilder.length() - 1, stringBuilder.length());
		}
	}
	
	/**
	 * Konvertiert den gegebenen {@link String} in einen String bestehend aus
	 * der Hexadezimalen Darstellung der einzelnen Bytes des {@link String}s.
	 * 
	 * @param text Der {@link String} welcher in seine
	 *        Byte-String-Repraesentation konvertiert werden soll.
	 * @return Die hexadezimale Darstellung der einzelnen Bytes des gegebenen
	 *         {@link String}s.
	 */
	private static final String toByteString(String text) {
		// Einen neuen StringBuilder fuer das zusammen bauen der
		// Repraesentation.
		StringBuilder byteString = new StringBuilder();
		
		// Wir iterieren ueber alle Bytes.
		for (byte piece : text.getBytes(StandardCharsets.UTF_8)) {
			// Konvertieren des Bytes in unsere gewollte String-Darstellung.
			String pieceAsString = convertByteToString(piece);
			
			// Anhaengen der String-Darstellung des Bytes.
			byteString
					.append(pieceAsString)
					.append(" ");
		}
		
		// Entfernen des letzten Leerzeichens.
		removeTrailingSpace(byteString);
		
		// Retournieren der gebauten Repraesentation.
		return byteString.toString();
	}
	
	/**
	 * Konvertiert den gegebenen {@link String} in einen String bestehend aus
	 * den einzelnen {@link Character}s.
	 * 
	 * @param text Der {@link String} welcher in seine Character-Repraesentation
	 *        konvertiert werden soll.
	 * @return Die einzelnen {@link Character}s des gegebenen {@link String}s.
	 */
	private static final String toCharacterString(String text) {
		// Einen neuen StringBuilder fuer das zusammen bauen der
		// Repraesentation.
		StringBuilder characterString = new StringBuilder();
		
		// Wir iterieren ueber alle einzelnen chars.
		for (char character : text.toCharArray()) {
			// Anhaengen des chars an unsere Repraesentation.
			characterString
					.append("'")
					.append(character)
					.append("' ");
		}
		
		// Entfernen des letzten Leerzeichens.
		removeTrailingSpace(characterString);
		
		// Retournieren der gebauten Repraesentation.
		return characterString.toString();
	}
	
	/**
	 * Konvertiert den gegebenen {@link String} in einen String bestehend aus
	 * der Unicode Darstellung der einzelnen CodePoints des {@link String}s.
	 * 
	 * @param text Der {@link String} welcher in seine
	 *        Unicode-CodePoints-Repraesentation konvertiert werden soll.
	 * @return Die Unicode Darstellung der einzelnen CodePoints des gegebenen
	 *         {@link String}s.
	 */
	private static final String toCodePointString(String text) {
		// Einen neuen StringBuilder fuer das zusammen bauen der
		// Repraesentation.
		StringBuilder codePointString = new StringBuilder();
		
		// Wir iterieren ueber alle einzelnen CodePoints.
		for (int codePoint : text.codePoints().toArray()) {
			// Konvertieren des CodePoints in to Unicode Darstellung.
			String codePointAsString = convertCodePointToString(codePoint);
			
			// Anhaengen des CodePoints an unsere Repraesentation.
			codePointString
					.append(codePointAsString)
					.append(" ");
		}
		
		// Entfernen des letzten Leerzeichens.
		removeTrailingSpace(codePointString);
		
		// Retournieren der gebauten Repraesentation.
		return codePointString.toString();
	}
	
	/**
	 * Konvertiert den gegebenen {@link String} in einen String bestehend aus
	 * den einzelnen Zeichen als Sub-{@link String}s.
	 * <p>
	 * Diese Implementierung beachtet CodePoints korrekt.
	 * 
	 * @param text Der {@link String} welcher in seine einzelnen Zeichen
	 *        konvertiert werden soll.
	 * @return Die Unicode Darstellung der einzelnen CodePoints des gegebenen
	 *         {@link String}s.
	 */
	private static final String toSubstringStringCodePointAware(String text) {
		// Einen neuen StringBuilder fuer das zusammen bauen der
		// Repraesentation.
		StringBuilder codePointString = new StringBuilder();
		
		// Wir iterieren ueber den String basierend auf einem Index. Dieser
		// Index wird korrekt hochgezaehlte basierend auf dem aktuellen
		// CodePoint.
		for (int index = 0; index < text.length(); index += Character.charCount(text.codePointAt(index))) {
			// Herausschneiden des Zeichens als Sub-String.
			String substring = text.substring(index, index + Character.charCount(text.codePointAt(index)));
			
			// Anhaengen des Zeichens an unsere Repraesentation.
			codePointString
					.append("\"")
					.append(substring)
					.append("\" ");
		}
		
		// Entfernen des letzten Leerzeichens.
		removeTrailingSpace(codePointString);
		
		// Retournieren der gebauten Repraesentation.
		return codePointString.toString();
	}
	
	/**
	 * Konvertiert den gegebenen {@link String} in einen String bestehend aus
	 * den einzelnen Zeichen als Sub-{@link String}s.
	 * <p>
	 * Diese Implementierung nimmt naiv an dass jeder Index im {@link String}
	 * auch einem Zeichen entspricht.
	 * 
	 * @param text Der {@link String} welcher in seine einzelnen Zeichen
	 *        konvertiert werden soll.
	 * @return Die Unicode Darstellung der einzelnen CodePoints des gegebenen
	 *         {@link String}s.
	 */
	private static final String toSubstringStringNaive(String text) {
		// Einen neuen StringBuilder fuer das zusammen bauen der
		// Repraesentation.
		StringBuilder substringString = new StringBuilder();
		
		// Wir iterieren ueber den String basierend auf einem Index. Dieser
		// Index wird naiv immer um Eins hochgezählt.
		for (int index = 0; index < text.length(); index++) {
			// Herausschneiden des Zeichens als Sub-String.
			String substring = text.substring(index, index + 1);
			
			// Anhaengen des Zeichens an unsere Repraesentation.
			substringString
					.append("\"")
					.append(substring)
					.append("\" ");
		}
		
		// Entfernen des letzten Leerzeichens.
		removeTrailingSpace(substringString);
		
		// Retournieren der gebauten Repraesentation.
		return substringString.toString();
	}
}
