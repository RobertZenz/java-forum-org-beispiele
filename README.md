Java-Forum.org Beispiele
========================

Beispiele fuer Themen auf [java-forum.org](https://www.java-forum.org).

Die Beispiele selbst sind gut dokumentiert und koennen direkt in eine beliebige IDE uebernommen beziehungsweise importiert werden. Sie sind absichtlich nicht perfekt sondern enthalten viele Hinweise was man noch anders machen oder erweitern koennte.

Alle Beispiele stehen unter einer 2-Clause-BSD Lizenz.


Code Style
----------

Die Beispiele verwenden meinen persoenlichen Stil. Dieser widerspricht an einigen Stellen den Konventionen und ist an anderen Stellen sehr explizit, dies ist so gewollt. Die Beispiele sind auch nur als Beispiele gedacht um die Strukturierung und den Aufbau solcher Programme zu demonstrieren, und nicht als Copy & Paste fertige Loesungen.


Javadoc
-------

Die Klassen und Methoden sind durchgehend mit Javadoc versehen. In der Realitaet macht dies selten Sinn, da die Methodennamen bereits so gewaehlt werden sollten dass man leicht erkennt was die Methode tut. Daher ist es meistens Zeitverschwendung alle Methoden redundant zu dokumentieren. Zum Beispiel:

```java
/**
 * Die Weite.
 * 
 * @return Die Weite.
 */
public int getWidth() {
    return width;
}
```

Zeigt diese sehr gut. Im Idealfall koennte und wuerde man auf die Dokumentation hier verzichten wenn man keine weiteren Informationen hat welche in der Dokumentation Sinn machen wuerden.
