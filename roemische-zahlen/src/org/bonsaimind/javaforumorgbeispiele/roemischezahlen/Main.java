/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.roemischezahlen;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.Queue;

/**
 * Hauptklasse welche die Hauptprogrammlogik enthaelt.
 * <p>
 * {@code final} damit sich niemand von der Klasse ableitet.
 */
public final class Main {
	/**
	 * {@code private} Konstruktor damit niemand die Klasse instanziert.
	 */
	private Main() {
	}
	
	/**
	 * Haupteinstiegpunkt des Programms.
	 * 
	 * @param args Die Argumente welche dem Programm uebergeben wurden. Werden
	 *        in diesem Fall nicht verwendet (muessen aber angegeben werden
	 *        wegen der Signatur).
	 */
	public static final void main(String[] args) {
		try {
			// Beispiel-Verwendungen.
			
			Umrechnung umrechnung = new EinfacheUmrechung(
					EinfacheUmrechung.Feature.VALIDIERUNG,
					EinfacheUmrechung.Feature.VALIDIERUNG_REIHENFOLGE);
			
			System.out.println(umrechnung.umrechnen("MDCCCCLXXXIIII"));
			
			umrechnung = new SubtraktionsRegelUmrechung(
					SubtraktionsRegelUmrechung.Feature.VALIDIERUNG,
					SubtraktionsRegelUmrechung.Feature.VALIDIERUNG_REIHENFOLGE,
					SubtraktionsRegelUmrechung.Feature.DOPPELTE_SUBTRAKTIVE_STELLUNG);
			
			System.out.println(umrechnung.umrechnen("MCMLXXXIV"));
			System.out.println(umrechnung.umrechnen("MCMLXXXIIV"));
		} catch (UngueltigeZahlException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Die {@link EinfacheUmrechung} ist eine {@link Umrechnung} Implementierung
	 * welche die einfachsten Regeln fuer das Umrechnen von Roemischen in
	 * Arabische Zahlen umsetzt.
	 * <p>
	 * Beispiele fuer die Umrechnung:
	 * <ul>
	 * <li>X -> 10</li>
	 * <li>XII -> 12</li>
	 * <li>IIX -> 12 (wirft eine Ausnahme wenn
	 * {@link Feature#VALIDIERUNG_REIHENFOLGE} eingeschaltet ist)</li>
	 * <li>MCXI -> 1111</li>
	 * </ul>
	 */
	public static class EinfacheUmrechung implements Umrechnung {
		/** Die {@link Feature}s welche eingeschaltet sind. */
		protected EnumSet<Feature> eingeschalteteFeatures = null;
		
		/**
		 * Erstellt eine neue {@link EinfacheUmrechung} Instanz mit den
		 * gegebenen {@link Feature}s.
		 * 
		 * @param eingeschalteteFeatures Die {@link Feature}s welche
		 *        eingeschaltet sein sollen.
		 */
		public EinfacheUmrechung(Feature... eingeschalteteFeatures) {
			super();
			
			// Pruefen ob wir ueberhaupt ein Array uebergeben bekommen haben.
			if (eingeschalteteFeatures != null && eingeschalteteFeatures.length > 0) {
				this.eingeschalteteFeatures = EnumSet.copyOf(Arrays.asList(eingeschalteteFeatures));
			} else {
				this.eingeschalteteFeatures = EnumSet.noneOf(Feature.class);
			}
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public long umrechnen(String roemischeZahl) throws UngueltigeZahlException {
			// Das Resultat welches wir errechnet haben.
			int resultat = 0;
			
			// Die letzte Roemische Ziffer welche verarbeitet wurde.
			RoemischeZiffer letzteZiffer = null;
			
			// Zur Vereinfachung der Logik wir gehen den String durch basierend
			// auf dem Anzahl der Zeichen darin.
			//
			// Hinweis: In diesem Fall gibt es zwei andere Moeglichkeiten:
			// Schleife ueber alle chars oder Schleife ueber alle CodePoints.
			// Ersteres hat Probleme wenn es Zeichen im String gibt welche mehr
			// als zwei Byte verwenden, letztere hat diese Probleme nicht wuerde
			// aber eine zusaetzlich Rueckumwandlung in einen String brauchen
			// damit wir die entsprechende Ziffer suchen koennen. Daher gehen
			// wir hier direkt ueber alle Zeichen basierend auf Substring.
			for (int index = 0; index < roemischeZahl.length(); index++) {
				// Holen der Roemischen Ziffer basierend auf dem Zeichen an der
				// aktuellen Stelle.
				RoemischeZiffer ziffer = RoemischeZiffer.nachZeichen(roemischeZahl, index);
				
				// Pruefen ob die Validierung eingeschaltet ist, und wenn ja
				// pruefen ob das aktuelle Zeichen eine gueltige Roemische
				// Ziffer ist.
				if (eingeschalteteFeatures.contains(Feature.VALIDIERUNG)
						&& ziffer == null) {
					throw new UngueltigeZahlException(String.format("Keine gueltige Roemische Ziffer <%s> auf Position <%s>.",
							roemischeZahl.substring(index, index + 1),
							Integer.toString(index)));
				}
				
				// Pruefen ob es eine Ziffer gibt, wir kommen hierher wenn die
				// Validierung abgeschaltet ist.
				if (ziffer != null) {
					// Pruefen ob die Validierung der Reihenfolge eingeschaltet
					// ist, und wenn ja pruefen ob die letzte Ziffer welche
					// verarbeitet wurde groeszer war als die aktuelle.
					if (eingeschalteteFeatures.contains(Feature.VALIDIERUNG_REIHENFOLGE)
							&& letzteZiffer != null
							&& ziffer.getWert() > letzteZiffer.getWert()) {
						throw new UngueltigeZahlException(String.format("Keine gueltige Roemische Zahl da <%s> auf <%s> folgt auf Position <%s>.",
								ziffer.name(),
								letzteZiffer.name(),
								Integer.toString(index)));
					}
					
					// Nach allen Pruefungen koennen wir den Wert zu unserem
					// Resultat hinzurechnen.
					resultat = resultat + ziffer.getWert();
					
					// Aktualisieren der letzten verarbeiteten Ziffer.
					letzteZiffer = ziffer;
				}
			}
			
			// Retournieren des Resultats.
			return resultat;
		}
		
		/**
		 * Das {@link Feature} ist ein enum welcher alle (einschaltbaren)
		 * Features dieser {@link Umrechnung} enthaelt.
		 */
		public static enum Feature {
			/**
			 * Aktiviert die Validierung.
			 * <p>
			 * Wenn ein Zeichen in der Roemischen Zahl gefunden wird welches
			 * nicht in eine Roemische Ziffer umgewandelt werden kann, wird eine
			 * {@link UngueltigeZahlException} geworfen anstatt diese zu
			 * ignorieren.
			 * <p>
			 * Beispiele fuer ungueltige Roemische Zahlen welche eine Ausnahme
			 * ausloesen wuerden mit diesem Feature:
			 * <ul>
			 * <li>MXY</li>
			 * <li>VR</li>
			 * <li>IIIL</li>
			 * <li>12X</li>
			 * </ul>
			 */
			VALIDIERUNG,
			
			/**
			 * Aktiviert die Validierung der Reihenfolge der Ziffern.
			 * <p>
			 * Wenn die Roemische Zahl nicht in der Reihung "Groeszer nach
			 * Kleiner" ist, wird eine {@link UngueltigeZahlException} geworfen
			 * anstatt sie dennoch umzurechnen.
			 * <p>
			 * Beispiele fuer ungueltige Roemische Zahlen welche eine Ausnahme
			 * ausloesen wuerden mit diesem Feature:
			 * <ul>
			 * <li>MXM</li>
			 * <li>MXVIV</li>
			 * <li>IX</li>
			 * <li>IM</li>
			 * </ul>
			 */
			VALIDIERUNG_REIHENFOLGE;
		}
	}
	
	/**
	 * Die {@link RoemischeZiffer} ist ein enum welcher alle verwendbaren
	 * Roemischen Ziffern abbildet.
	 */
	public enum RoemischeZiffer {
		/**
		 * Die Roemische Ziffer "C" mit dem Wert 100.
		 */
		C(5, 100),
		
		/**
		 * Die Roemische Ziffer "D" mit dem Wert 500.
		 */
		D(6, 500),
		
		/**
		 * Die Roemische Ziffer "I" mit dem Wert 1.
		 */
		I(1, 1),
		
		/**
		 * Die Roemische Ziffer "L" mit dem Wert 50.
		 */
		L(4, 50),
		
		/**
		 * Die Roemische Ziffer "M" mit dem Wert 1000.
		 */
		M(7, 1000),
		
		/**
		 * Die Roemische Ziffer "V" mit dem Wert 5.
		 */
		V(2, 5),
		
		/**
		 * Die Roemische Ziffer "X" mit dem Wert 10.
		 */
		X(3, 10);
		
		/** Der Index der Ziffer im Zahlensystem. */
		private int index = 0;
		/** Der Wert der Ziffer. */
		private int wert = 0;
		
		/**
		 * Erstelle eine neue {@link RoemischeZiffer} mit den gegebenen Werten.
		 * 
		 * @param index Der Index der Roemischen Ziffer im Zahlensystem.
		 * @param wert Der Wert der Roemischen Ziffer.
		 */
		private RoemischeZiffer(int index, int wert) {
			this.index = index;
			this.wert = wert;
		}
		
		/**
		 * Liefert die {@link RoemischeZiffer} fuer das gegebene {@link String
		 * Zeichen}, {@code null} wenn dieses keiner {@link RoemischeZiffer}
		 * entspricht.
		 * <p>
		 * <i>Hinweis:</i> Bei derartigen Hilfsmethoden ist immer die Frage ob
		 * man {@code null} retournieren oder stattdessen eine Ausnahme werfen
		 * sollte. Die grundlegende Frage ist, ob der aufrufende Code den Wert
		 * vorher validieren und pruefen soll, oder ob dies anhand dieser
		 * Methode passieren soll.
		 * 
		 * @param zeichen Das Zeichen der Roemischen Ziffer.
		 * @return Die {@link RoemischeZiffer} fuer das gegebene {@link String
		 *         Zeichen}, {@code null} wenn dieses keiner
		 *         {@link RoemischeZiffer} entspricht.
		 */
		public static final RoemischeZiffer nachZeichen(String zeichen) {
			// Wir iterieren durch alle Roemischen Ziffern und retournieren dass
			// welches dem gegebenen Zeichen entspricht.
			for (RoemischeZiffer roemischeZiffer : values()) {
				if (roemischeZiffer.getZeichen().equals(zeichen)) {
					return roemischeZiffer;
				}
			}
			
			// Wenn keine passt, geben wir null zurueck um dies zu
			// signalisieren.
			return null;
		}
		
		/**
		 * Liefert die {@link RoemischeZiffer} fuer das gegebene Zeichen an der
		 * gegebenen Stelle in der Zahl, {@code null} wenn dieses keiner
		 * {@link RoemischeZiffer} entspricht oder der gegebene Index nicht im
		 * gegebenen {@link String} liegt.
		 * <p>
		 * <i>Hinweis:</i> Bei derartigen Hilfsmethoden ist immer die Frage ob
		 * man {@code null} retournieren oder stattdessen eine Ausnahme werfen
		 * sollte. Die grundlegende Frage ist, ob der aufrufende Code den Wert
		 * vorher validieren und pruefen soll, oder ob dies anhand dieser
		 * Methode passieren soll.
		 * 
		 * @param roemischeZahl Die gesamte Zahl welche das Zeichen enthaelt,
		 *        kann nicht {@code null} sein, darf aber leer sein.
		 * @param index Der Index des Zeichens in der gegebenen Roemischen Zahl,
		 *        kann negativ oder auszerhalb des Strings liegen, in welchem
		 *        Fall {@code null} retourniert wird.
		 * @return Die {@link RoemischeZiffer} fuer das gegebene das gegebene
		 *         Zeichen an der gegebenen Stelle in der Zahl, {@code null}
		 *         wenn dieses keiner {@link RoemischeZiffer} entspricht oder
		 *         der gegebene Index nicht im gegebenen {@link String} liegt.
		 */
		public static final RoemischeZiffer nachZeichen(String roemischeZahl, int index) {
			// Pruefen ob der Index gueltig sein kann.
			if (index >= 0 && index < roemischeZahl.length()) {
				// Holen des Zeichens an dieser Stelle.
				String zeichen = roemischeZahl.substring(index, index + 1);
				
				return RoemischeZiffer.nachZeichen(zeichen);
			}
			
			// Wenn der Index nicht gueltig ist retournieren wir null.
			return null;
		}
		
		/**
		 * Retourniert den Index fuer diese {@link RoemischeZiffer}.
		 * 
		 * @return Der Index fuer diese {@link RoemischeZiffer}.
		 */
		public int getIndex() {
			return index;
		}
		
		/**
		 * Retourniert den Wert fuer diese {@link RoemischeZiffer}.
		 * 
		 * @return Der Wert fuer diese {@link RoemischeZiffer}.
		 */
		public int getWert() {
			return wert;
		}
		
		/**
		 * Retourniert das Zeichen fuer diese {@link RoemischeZiffer}.
		 * 
		 * @return Das Zeichen fuer diese {@link RoemischeZiffer}.
		 */
		public String getZeichen() {
			return name();
		}
	}
	
	/**
	 * Die {@link SubtraktionsRegelUmrechung} ist eine {@link Umrechnung}
	 * Implementierung welche die Subtraktions-Regeln fuer das Umrechnen von
	 * Roemischen in Arabische Zahlen umsetzt.
	 * <p>
	 * Beispiele fuer die Umrechnung:
	 * <ul>
	 * <li>X -> 10</li>
	 * <li>XII -> 12</li>
	 * <li>IIX -> 8</li>
	 * <li>MCXI -> 1111</li>
	 * <li>MXCIXIV -> 1103</li>
	 * <li>IM -> 999 (wirft eine Ausnahme auszer
	 * {@link Feature#KEINE_SUBTRAKTIONS_LIMITIERUNG} ist eingeschaltet)</li>
	 * </ul>
	 */
	public static class SubtraktionsRegelUmrechung implements Umrechnung {
		/** Die {@link Feature}s welche eingeschaltet sind. */
		protected EnumSet<Feature> eingeschalteteFeatures = null;
		
		/**
		 * Erstellt eine neue {@link SubtraktionsRegelUmrechung} Instanz mit den
		 * gegebenen {@link Feature}s.
		 * 
		 * @param eingeschalteteFeatures Die {@link Feature}s welche
		 *        eingeschaltet sein sollen.
		 */
		public SubtraktionsRegelUmrechung(Feature... eingeschalteteFeatures) {
			super();
			
			// Pruefen ob wir ueberhaupt ein Array uebergeben bekommen haben.
			if (eingeschalteteFeatures != null && eingeschalteteFeatures.length > 0) {
				this.eingeschalteteFeatures = EnumSet.copyOf(Arrays.asList(eingeschalteteFeatures));
			} else {
				this.eingeschalteteFeatures = EnumSet.noneOf(Feature.class);
			}
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public long umrechnen(String roemischeZahl) throws UngueltigeZahlException {
			// Das Resultat welches wir errechnen.
			int resultat = 0;
			
			// Die folgende Logik funktioniert grob nach dem folgenden Prinzip:
			//
			// 1. Die aktuelle Ziffer wird auf den Stapel geschoben.
			// 2. Die nun aktuelle Ziffer wird geprueft ob diese der Ziffer auf
			// dem Stapel entspricht und die doppelte Subtraktion eingeschalten
			// ist. Wenn ja, wird auch diese auf den Stapel geschoben.
			// 3. Im naechsten Durchlauf werden alle Ziffern auf dem Stapel
			// addiert. Wenn die Ziffern vom Stapel kleiner sind als die
			// folgende Ziffer, werden diese von der Ziffer abgezogen. Ansonsten
			// werden diese zum Ergebnis addiert und die nun aktuelle Ziffer
			// wird auf den Stapel geschoben.
			
			// Der Stapel.
			Queue<RoemischeZiffer> stapel = new ArrayDeque<>();
			// Die zuletzt verarbeitete Ziffer.
			RoemischeZiffer letzteZiffer = null;
			
			// Zur Vereinfachung der Logik wir gehen den String durch basierend
			// auf dem Anzahl der Zeichen darin.
			//
			// Hinweis: In diesem Fall gibt es zwei andere Moeglichkeiten:
			// Schleife ueber alle chars oder Schleife ueber alle CodePoints.
			// Ersteres hat Probleme wenn es Zeichen im String gibt welche mehr
			// als zwei Byte verwenden, letztere hat diese Probleme nicht wuerde
			// aber eine zusaetzlich Rueckumwandlung in einen String brauchen
			// damit wir die entsprechende Ziffer suchen koennen. Daher gehen
			// wir hier direkt ueber alle Zeichen basierend auf Substring.
			for (int index = 0; index < roemischeZahl.length(); index++) {
				// Holen der Roemischen Ziffer basierend auf dem Zeichen an der
				// aktuellen Stelle.
				RoemischeZiffer ziffer = RoemischeZiffer.nachZeichen(roemischeZahl, index);
				
				// Pruefen ob die Validierung eingeschaltet ist, und wenn ja
				// pruefen ob das aktuelle Zeichen eine gueltige Roemische
				// Ziffer ist.
				if (eingeschalteteFeatures.contains(Feature.VALIDIERUNG)
						&& ziffer == null) {
					throw new UngueltigeZahlException(String.format("Keine gueltige Roemische Ziffer <%s> auf Position <%s>.",
							roemischeZahl.substring(index, index + 1),
							Integer.toString(index)));
				}
				
				// Pruefen ob es eine Ziffer gibt, wir kommen hierher wenn die
				// Validierung abgeschaltet ist.
				if (ziffer != null) {
					// Pruefen ob der Stapel leer ist, wenn ja kommt die
					// aktuelle Ziffer auf den Stapel.
					if (stapel.isEmpty()) {
						stapel.add(ziffer);
					} else {
						// Pruefen ob die doppelte Subtraktion eingeschaltet
						// ist, und wenn ja und die aktuelle Ziffer der
						// vorherigen entspricht kommt diese ebenfalls auf den
						// Stapel.
						if (eingeschalteteFeatures.contains(Feature.DOPPELTE_SUBTRAKTIVE_STELLUNG)
								&& stapel.size() < 2
								&& stapel.peek() == ziffer) {
							stapel.add(ziffer);
						} else {
							// Holen der vorhergehenden Ziffer vom Stapel.
							RoemischeZiffer vorhergehendeZiffer = stapel.poll();
							int vorhergehenderWert = vorhergehendeZiffer.getWert();
							
							// Wenn der Stapel nicht leer ist, kann es such nur
							// um eine Verdoppelung des aktuellen Wertes
							// handeln.
							if (!stapel.isEmpty()) {
								vorhergehenderWert = vorhergehenderWert + stapel.poll().getWert();
							}
							
							// Pruefen ob die Validierung der Reihenfolge
							// eingeschaltet ist, und wenn ja pruefen ob die
							// letzte Ziffer welche verarbeitet wurde groeszer
							// war als die aktuelle.
							if (eingeschalteteFeatures.contains(Feature.VALIDIERUNG_REIHENFOLGE)
									&& letzteZiffer != null
									&& ziffer.getWert() > letzteZiffer.getWert()) {
								throw new UngueltigeZahlException(String.format("Keine gueltige Roemische Zahl da <%s> auf <%s> folgt auf Position <%s>.",
										ziffer.name(),
										letzteZiffer.name(),
										Integer.toString(index)));
							}
							
							// Pruefen ob die aktuelle Ziffer groeszer ist als
							// die vorhergehende, in welchem Fall wir die
							// Subtraktionsregel anwenden muessen.
							if (ziffer.getIndex() > vorhergehendeZiffer.getIndex()) {
								// Pruefen ob die subtrations Limitierung
								// abgeschaltet wurde, falls nicht pruefen ob
								// man die vorhergehende Ziffer von der
								// aktuellen tatsaechlicvh abziehen darf.
								if (!eingeschalteteFeatures.contains(Feature.KEINE_SUBTRAKTIONS_LIMITIERUNG)
										&& ziffer.getIndex() > (vorhergehendeZiffer.getIndex() + 2)) {
									throw new UngueltigeZahlException(String.format("Keine gueltige Roemische Zahl da <%s> auf <%s> folgt auf Position <%s>.",
											ziffer.name(),
											vorhergehendeZiffer.name(),
											Integer.toString(index)));
								}
								
								// Hinzurechnen der subtrahierten Zahl zum
								// Resultat.
								resultat = resultat + ziffer.getWert() - vorhergehenderWert;
								
								// Aktualisieren der letzten verarbeiteten
								// Ziffer.
								letzteZiffer = ziffer;
							} else {
								// Addieren des aktuellen Werts zum Resultat.
								resultat = resultat + vorhergehenderWert;
								
								// Aktualisieren der letzten verarbeiteten
								// Ziffer.
								letzteZiffer = vorhergehendeZiffer;
								
								// Aktuelle Ziffer auf den Stapel fuer die
								// weitere
								// Verarbeitung schieben.
								stapel.add(ziffer);
							}
						}
					}
				}
			}
			
			// Wenn sich noch Ziffern auf dem Stapel befinden muessen diese zum
			// Resultat addiert werden.
			while (!stapel.isEmpty()) {
				resultat = resultat + stapel.poll().getWert();
			}
			
			// Retournieren des Resultats.
			return resultat;
		}
		
		/**
		 * Das {@link Feature} ist ein enum welcher alle (einschaltbaren)
		 * Features dieser {@link Umrechnung} enthaelt.
		 */
		public static enum Feature {
			/**
			 * Erlaubt doppelte subtraktive Stellungen.
			 * <p>
			 * In der "normalen" Umrechnung sind nur einfache Vorstellungen
			 * erlaubt, zum Beispiel "IV" oder "VX". Mit diesem Feature werden
			 * doppelte Stellungen, zum Beispiel "IIV" und "IIX" erlaubt.
			 */
			DOPPELTE_SUBTRAKTIVE_STELLUNG,
			
			/**
			 * Erlaubt beliebige subtraktive Stellungen.
			 * <p>
			 * In der "normalen" Umrechnung sind nur Vorstellungen erlaubt
			 * welche ein oder zwei Werte unter dem Minuend liegen, zum Beispiel
			 * "IV" oder "IX". Mit diesem Feature werden beliebige Stellungen
			 * erlaubt, zum Beispiel "IM".
			 */
			KEINE_SUBTRAKTIONS_LIMITIERUNG,
			
			/**
			 * Aktiviert die Validierung.
			 * <p>
			 * Wenn ein Zeichen in der Roemischen Zahl gefunden wird welches
			 * nicht in eine Roemische Ziffer umgewandelt werden kann, wird eine
			 * {@link UngueltigeZahlException} geworfen anstatt diese zu
			 * ignorieren.
			 * <p>
			 * Beispiele fuer ungueltige Roemische Zahlen welche eine Ausnahme
			 * ausloesen wuerden mit diesem Feature:
			 * <ul>
			 * <li>MXY</li>
			 * <li>VR</li>
			 * <li>IIIL</li>
			 * <li>12X</li>
			 * </ul>
			 */
			VALIDIERUNG,
			
			/**
			 * Aktiviert die Validierung der Reihenfolge der Ziffern.
			 * <p>
			 * Wenn die Roemische Zahl nicht in der Reihung "Groeszer nach
			 * Kleiner" ist, wird eine {@link UngueltigeZahlException} geworfen
			 * anstatt sie dennoch umzurechnen.
			 * <p>
			 * Beispiele fuer ungueltige Roemische Zahlen welche eine Ausnahme
			 * ausloesen wuerden mit diesem Feature:
			 * <ul>
			 * <li>MXM</li>
			 * <li>MXVIV</li>
			 * <li>IX</li>
			 * <li>IM</li>
			 * </ul>
			 */
			VALIDIERUNG_REIHENFOLGE;
		}
	}
	
	/**
	 * Die {@link Umrechnung} Schnittstelle erlaubt es einen {@link String} als
	 * Roemische Zahl zu verarbeiten und den Wert dieser zu erhalten.
	 */
	public static interface Umrechnung {
		/**
		 * Rechnet die gegebene Roemische Zahl in eine Arabische Zahl um, und
		 * gibt diesen Wert als {@code long} zurueck.
		 * <p>
		 * Zu beachten hierbei ist dass die Definition was eine "gueltige
		 * Roemischen Zahl" ist der Implementierung ueberlassen wird.
		 * 
		 * @param roemischeZahl Die Roemische Zahl als {@link String} welche
		 *        umgerechnet werden soll.
		 * @return Den Wert der gegebenen Roemischen Zahl.
		 * @throws UngueltigeZahlException Wenn die gegebene {@link String
		 *         Roemische Zahl} keine (gueltige) Roemische Zahl ist.
		 */
		public long umrechnen(String roemischeZahl) throws UngueltigeZahlException;
	}
	
	/**
	 * Die {@link UngueltigeZahlException} ist eine {@link Exception}
	 * Erweiterung welche angibt dass eine ungueltige Roemische Zahl angegeben
	 * wurde.
	 */
	public static class UngueltigeZahlException extends Exception {
		/**
		 * Erstelle eine neue {@link UngueltigeZahlException} Instanz.
		 */
		public UngueltigeZahlException() {
			super();
		}
		
		/**
		 * Erstelle eine neue {@link UngueltigeZahlException} Instanz.
		 * 
		 * @param message Die Detail-Nachricht.
		 */
		public UngueltigeZahlException(String message) {
			super(message);
		}
		
		/**
		 * Erstelle eine neue {@link UngueltigeZahlException} Instanz.
		 * 
		 * @param message Die Detail-Nachricht.
		 * @param cause Die Ursache. Kann {@code null} sein wenn es keine
		 *        Ursache gibt.
		 */
		public UngueltigeZahlException(String message, Throwable cause) {
			super(message, cause);
		}
		
		/**
		 * Erstelle eine neue {@link UngueltigeZahlException} Instanz.
		 * 
		 * @param cause Die Ursache. Kann {@code null} sein wenn es keine
		 *        Ursache gibt.
		 */
		public UngueltigeZahlException(Throwable cause) {
			super(cause);
		}
		
		/**
		 * Erstelle eine neue {@link UngueltigeZahlException} Instanz.
		 * 
		 * @param message Die Detail-Nachricht.
		 * @param cause Die Ursache. Kann {@code null} sein wenn es keine
		 *        Ursache gibt.
		 * @param enableSuppression Ob Unterdrueckung aktiv sein soll.
		 * @param writableStackTrace Ob eine Stapelspur inkludiert werden soll.
		 */
		protected UngueltigeZahlException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
			super(message, cause, enableSuppression, writableStackTrace);
		}
	}
}
