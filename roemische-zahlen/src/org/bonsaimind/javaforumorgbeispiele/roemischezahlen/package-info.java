/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Roemische Zahlen
 * <p>
 * Ein komplexeres Beispiel welche die Implementierung von Logik mit einer
 * Basis-Schnittstelle demonstriert. Das verarbeiten von Roemischen Zahlen kann
 * je nach angewendeten Regeln relativ komplex werden, diese Komplexitaet lagern
 * wir in unterschiedliche Implementierugen fuer die selbe Schnittstelle aus.
 * <p>
 * In diesem Fall verzichten wir auf Benutzerinteraktion und
 * {@link org.bonsaimind.javaforumorgbeispiele.roemischezahlen.Main#main(String[])}
 * enthaelt lediglich Verwendungsbeispiele.
 * 
 * @see <a href=
 *      "https://de.wikipedia.org/wiki/R%C3%B6mische_Zahlschrift">Wikipedia.de:
 *      Römische Zahlschrift</a>
 */

package org.bonsaimind.javaforumorgbeispiele.roemischezahlen;