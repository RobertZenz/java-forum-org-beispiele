/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Swing Asynchroner Fortschritt
 * <p>
 * Demonstriert das synchronisieren von Fortschritt einer laenger laufendern
 * Operatione innerhalb einer Swing-GUI ohne diese zu blockieren.
 * <p>
 * Alle Aenderungen an einer Swing-Komponente sollten nur vom "Event Disptach
 * Thread" aus erfolgen, welcher quasi den "Swing Hauptthread" darstellt.
 * Aenderungen aus anderen Threads <i>koennen</i> fehlerfrei funktionieren, dies
 * kann aber nicht garantiert werden. Will man also eine Aenderung durchfuehren,
 * muss man sich auf den EDT synchronisieren, dies kann man entweder ueber
 * {@link javax.swing.SwingUtilities#invokeAndWait(Runnable)} oder
 * {@link javax.swing.SwingUtilities#invokeLater(Runnable)} machen. Letztere
 * Methode stellt die Aenderung ein diese wird von Swing bei "naechstbester
 * Gelegenheit" ausgefuehrt. In der Praxis bedeutet dies wenn der EDT keine
 * anderen Ereignisse mehr zum abarbeiten hat.
 * <p>
 * Wird die Simulation <i>synchron</i> gestartet wird die GUI einfrieren, da der
 * EDT/Haupt-Thread von Swing durch die Operation blockiert wird. Dies
 * <i>koennte</i> man umgehen in dem man Swing dazu zwingt die Komponenten in
 * diesem Moment dennoch neu zu zeichnen, dies wuerde aber die Operation selbst
 * ausbremsen da sie wiederum auf Swing und das Zeichnen der Komponenten warten
 * muss. Ein Abbrechen der Simulation ist in diesem Zustand nicht moeglich da
 * Swing die Ereignisse (Mausklick) nicht abarbeiten kann da der Thread
 * blockiert ist.
 * <p>
 * Wird die Simulation <i>asynchron</i> gestartet kann man den Fortschritt auf
 * der GUI verfolgen und auch mit dieser normal interagieren da der Swing-Thread
 * nicht blockiert wird.
 */

package org.bonsaimind.javaforumorgbeispiele.swingasynchronerfortschritt;