/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingasynchronerfortschritt;

import java.util.Random;

/**
 * Der {@link ProgressSimulator} simuliert eine laenger laufender Operation in
 * diesem dieser von 0 bis 100 zaehlt und dabei immer wieder eine zufaellige
 * Zeitspanne schlaeft.
 */
public class ProgressSimulator {
	/**
	 * Der Wert welcher verwendet wird um zu signalisieren dass die Simulation
	 * gestoppt/abgebrochen wurde und nicht fertig lief.
	 */
	public static final int SIMULATION_STOPPED_PROGRESS_VALUE = -1;
	
	/** Der {@link ProgressListener} an welchen Fortschritt berichtet wird. */
	protected ProgressListener progressListener = null;
	/**
	 * Das {@link Random} Objekt welches verwendet wird um Zufallszahlen fuer
	 * die Pausen zu generieren.
	 */
	protected Random random = new Random();
	/**
	 * Ob die Simulation gestoppt/abgebrochen werden soll.
	 * <p>
	 * <i>Hinweis:</i> {@code volatile} wird verwendet wenn eine Variable ueber
	 * mehrere Threads hinweg gelesen werden koennen soll wenn keine
	 * "vernuenftigte" Synchronisierung ({@code synchronized}) hergestellt
	 * werden kann. Effektiv verhindert {@code volatile} dass ein
	 * Thread-spezifischer Zwischenspeicher fuer diese Variable verwendet wird.
	 * Dies hat aber ebenfalls zusaetzliche Laufzeitkosten, welcher aber in
	 * diesem Beispiel (und in den meisten Anwendungen) vernachlaessigbar sind.
	 */
	protected volatile boolean stop = false;
	
	/**
	 * Erstellt eine neue {@link ProgressSimulator} Instanz.
	 * 
	 * @param progressListener Der {@link ProgressListener} an welchen
	 *        Fortschritt berichtet werden soll. Kann nicht {@code null} sein.
	 */
	public ProgressSimulator(ProgressListener progressListener) {
		super();
		
		this.progressListener = progressListener;
	}
	
	/**
	 * Beginnt die Simulation einer laenger laufenden Operation.
	 */
	public void simulate() {
		// Zuruecksetzen der stop Variable falls diese im letzten Lauf gesetzt
		// wurde.
		stop = false;
		
		// Hochzaehlen von 0 bis 99. 100 wird gesondert berichtet damit danach
		// keine Pause mehr folgt.
		for (int progressPercent = 0; progressPercent < 100; progressPercent++) {
			// Berichten des Fortschritts and den Listener.
			progressListener.progress(progressPercent);
			
			try {
				// Eine zufaellige Zeitspanne von 0 bis 150 Millisekunden
				// schlafen um die Operation zu simulieren.
				Thread.sleep(random.nextInt(150));
			} catch (InterruptedException e) {
				// Fuer dieses Beispiel behandeln wir die Ausnahme hier nicht
				// sondern werfen sie einfach weiter.
				throw new IllegalStateException(e);
			}
			
			// Pruefen ob die Simulation gestoppt werden soll.
			if (stop) {
				// Berichten des Abbruchs an den Listener.
				progressListener.progress(SIMULATION_STOPPED_PROGRESS_VALUE);
				
				// Beenden der Operation.
				return;
			}
		}
		
		// Benachrichtigen des Listeners dass die Operation abegschlossen ist.
		progressListener.progress(100);
	}
	
	/**
	 * Stoppt die Simulation wenn eine laeuft.
	 */
	public void stopSimulation() {
		// Setzen der Variable.
		stop = true;
	}
	
	/**
	 * Der {@link ProgressListener} ist eine Schnittstelle welche verwendet wird
	 * um ueber Fortschritt der Simulation benachrichtig zu werden.
	 */
	public static interface ProgressListener {
		/**
		 * Wird aufgerufen wenn Fortschritt stattgefunden hat.
		 * 
		 * @param newProgressInPercent Der neue Fortschritt, eine Wert zwischen
		 *        {@code 0} und {@code 100} (beide inklusive), oder
		 *        {@link ProgressSimulator#SIMULATION_STOPPED_PROGRESS_VALUE}
		 *        wenn die Simulation gestoppt/abgebrochen wurde.
		 */
		public abstract void progress(int newProgressInPercent);
	}
}
