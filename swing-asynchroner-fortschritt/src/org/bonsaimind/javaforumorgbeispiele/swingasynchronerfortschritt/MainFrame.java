/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingasynchronerfortschritt;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;

/**
 * {@link MainFrame} ist eine {@link JFrame}-Erweiterung welche das Hauptfenster
 * implementiert.
 */
public class MainFrame extends JFrame {
	/** Die {@link JProgressBar} welche den Fortschritt anzeigt. */
	protected JProgressBar progressBar = null;
	/** Die {@link ProgressSimulator} Instanz welche verwendet wird. */
	protected ProgressSimulator progressSimulator = new ProgressSimulator(this::handleProgress);
	/**
	 * Der {@link JButton} welcher fuer den asynchronen Start verwendet wird.
	 */
	protected JButton startAsynchronousButton = null;
	/** Der {@link JButton} welcher fuer den synchronen Start verwendet wird. */
	protected JButton startSynchronousButton = null;
	/** Der {@link JButton} welcher fuer das Stoppen verwendet wird. */
	protected JButton stopButton = null;
	
	/**
	 * Erstellt eine neue {@link MainFrame} Instanz.
	 */
	public MainFrame() {
		super();
		
		// Erstellen des Knopfes fuer den synchronen Start.
		startSynchronousButton = new JButton();
		startSynchronousButton.setText("Synchron Starten");
		startSynchronousButton.addActionListener(this::handleStartSynchronousButtonAction);
		
		// Erstellen des Knopfes fuer den asynchronen Start.
		startAsynchronousButton = new JButton();
		startAsynchronousButton.setText("Asynchron Starten");
		startAsynchronousButton.addActionListener(this::handleStartAsynchronousButtonAction);
		
		// Erstellen des Knopfes fuer das Stoppen.
		stopButton = new JButton();
		stopButton.setEnabled(false);
		stopButton.setText("Stoppen");
		stopButton.addActionListener(this::handleStopButtonAction);
		
		// Erstellen des Fortschrittbalkens.
		progressBar = new JProgressBar();
		progressBar.setMaximum(100);
		progressBar.setMinimum(0);
		
		// Aufsetzen und konfigurieren des Fensters.
		setTitle("Swing Asynchroner Fortschritt");
		setLayout(new GridBagLayout());
		add(startSynchronousButton, createGridBagConstraints(0, 0));
		add(startAsynchronousButton, createGridBagConstraints(1, 0));
		add(stopButton, createGridBagConstraints(2, 0));
		add(progressBar, createGridBagConstraints(0, 1, 2, 1));
		
		// Da wir keine (Mindes-)Groesze gesetzt haben, muessen wir einmal
		// dafuer sorgen dass der Layout-Manager die ideale Grosze des Fensters
		// berechnet und setzt.
		pack();
	}
	
	/**
	 * Erstellt ein neues {@link GridBagConstraints} mit dem gegebenen
	 * {@code x}/{@code y}-Wert.
	 * 
	 * @param x Der {@code x}-Wert.
	 * @param y Der {@code y}-Wert.
	 * @return Ein neue {@link GridBagConstraints} mit dem gegebenen
	 *         {@code x}/{@code y}-Wert.
	 */
	protected GridBagConstraints createGridBagConstraints(int x, int y) {
		return createGridBagConstraints(x, y, x, y);
	}
	
	/**
	 * Erstellt ein neues {@link GridBagConstraints} so dass die Komponente
	 * ueber die gegebenen "Zellen" gespannt wird..
	 * 
	 * @param startX Der @{code x}-Wert bei welchem die Komponent beginnen soll
	 *        (inklusive).
	 * @param startY Der @{code y}-Wert bei welchem die Komponent beginnen soll
	 *        (inklusive).
	 * @param endX Der @{code x}-Wert bei welchem die Komponent enden soll
	 *        (inklusive).
	 * @param endY Der @{code y}-Wert bei welchem die Komponent enden soll
	 *        (inklusive).
	 * @return Ein neue {@link GridBagConstraints} mit dem gegebenen Werten.
	 */
	protected GridBagConstraints createGridBagConstraints(int startX, int startY, int endX, int endY) {
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.gridheight = endY - startY + 1;
		constraints.gridwidth = endX - startX + 1;
		constraints.gridx = startX;
		constraints.gridy = startY;
		constraints.insets = new Insets(8, 8, 8, 16);
		constraints.ipadx = 8;
		constraints.ipady = 8;
		constraints.weightx = 1d;
		constraints.weighty = 1d;
		
		if (startX != endX
				&& startY != endY) {
			constraints.fill = GridBagConstraints.BOTH;
		} else if (startX != endX) {
			constraints.fill = GridBagConstraints.HORIZONTAL;
		} else if (startY != endY) {
			constraints.fill = GridBagConstraints.VERTICAL;
		} else {
			constraints.fill = GridBagConstraints.NONE;
		}
		
		return constraints;
	}
	
	/**
	 * Handhabt die Benachrichtigung dass Fortschritt stattgefunden hat.
	 * 
	 * @param newProgressInPercent Der neue Fortschrittswert in Prozent.
	 */
	protected void handleProgress(int newProgressInPercent) {
		// Pruefen ob wir auf dem EDT/Swing-Thread sind, und wenn ja die
		// Operation direkt ausfuehren anstatt diese erst in die Warteschlange
		// zu stellen.
		if (SwingUtilities.isEventDispatchThread()) {
			// Direktes ausfuehrend.
			updateProgressBarAndControlButtons(newProgressInPercent);
		} else {
			// Einstellen der Aenderungen damit diese "bei naechster
			// Gelegenheit" von Swing ausgefuehrt wird.
			//
			// Die Reihenfolge ist hier garantiert.
			SwingUtilities.invokeLater(() -> {
				updateProgressBarAndControlButtons(newProgressInPercent);
			});
		}
	}
	
	/**
	 * Wenn {@link #startAsynchronousButton} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleStartAsynchronousButtonAction(ActionEvent actionEvent) {
		// Zustand der Knoepfe umsetzen.
		setControlButtonStateFor(true);
		
		// Erstellen eines neuen Threads in welchem die Simulation laufen wird.
		Thread thread = new Thread(progressSimulator::simulate);
		// Wir setzen den Thread als "Daemon" damit dieser die JVM nicht am
		// beenden hindert.
		thread.setDaemon(true);
		// Starten des Threads.
		thread.start();
	}
	
	/**
	 * Wenn {@link #startSynchronousButton} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleStartSynchronousButtonAction(ActionEvent actionEvent) {
		// Zustand der Knoepfe umsetzen.
		setControlButtonStateFor(true);
		
		// Wir fuehren die Simulation direkt hier im Swing-Thread aus (auf
		// welchem wir uns befinden muessen da wir in dem Ereignis einer
		// Swing-Komponente sind(.
		progressSimulator.simulate();
	}
	
	/**
	 * Wenn {@link #stopButton} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleStopButtonAction(ActionEvent actionEvent) {
		// Stoppen der Simulation.
		progressSimulator.stopSimulation();
	}
	
	/**
	 * Setzte den Zustand der Knoepfe basierend auf dem gegebenen Wert.
	 * 
	 * @param isSimulationRunning Ob die Simulation laeuft oder nicht.
	 */
	protected void setControlButtonStateFor(boolean isSimulationRunning) {
		// Starten soll nur moeglich sein wenn die Simulation nicht laeuft.
		startAsynchronousButton.setEnabled(!isSimulationRunning);
		startSynchronousButton.setEnabled(!isSimulationRunning);
		// Stoppen soll nur moeglich sein wenn die Simulation laeuft.
		stopButton.setEnabled(isSimulationRunning);
	}
	
	/**
	 * Aktualisiert den Fortschrittsbalken und die restliche GUI basierend auf
	 * dem gegebenen Wert.
	 * 
	 * @param newProgressInPercent Der neue Fortschrittswert in Prozent.
	 */
	protected void updateProgressBarAndControlButtons(int newProgressInPercent) {
		// Wenn der Forschrittswert in den Fortschrittsbalken passt uebernehmen
		// wir diesen.
		if (newProgressInPercent >= progressBar.getMinimum()
				&& newProgressInPercent <= progressBar.getMaximum()) {
			// Uebernehmen des Werts.
			progressBar.setValue(newProgressInPercent);
		}
		
		// Falls der Wert groeszer 100 ist muss die Operation abgeschlossen
		// sein, auszer der Wert entspricht dem speziellen Abbruchwert.
		if (newProgressInPercent >= 100
				|| newProgressInPercent == ProgressSimulator.SIMULATION_STOPPED_PROGRESS_VALUE) {
			// Wiederherstellen des Zustands der Knoepfe.
			setControlButtonStateFor(false);
		}
	}
}
