/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingwortliste;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * {@link MainFrame} ist eine {@link JFrame}-Erweiterung welche das Hauptfenster
 * implementiert.
 */
public class MainFrame extends JFrame {
	/** Die Standardliste an Woerter welche wir nach dem oeffnen anzeigen. */
	protected static final List<String> DEFAULT_WORD_LIST = Arrays.asList(
			"Kommunist",
			"Reise",
			"Klaustrophobie",
			"Steine",
			"Figuren",
			"Kaktus",
			"Sonnenschirm",
			"Zug",
			"Adam",
			"Abwickeln");
	
	/**
	 * Der {@link JFileChooser} welchen wir verwenden um Dateien fuer das
	 * Laden/Speichern auszuwaehlen.
	 */
	protected JFileChooser fileChooser = null;
	/**
	 * Das {@link DefaultTableModel} welches wir verwenden um die
	 * {@link #wordTable} zu fuellen.
	 */
	protected DefaultTableModel wordModel = null;
	/** Der {@link JTable} welcher die Wortliste anzeigt. */
	protected JTable wordTable = null;
	
	/**
	 * Erstellt eine neue {@link MainFrame} Instanz.
	 */
	public MainFrame() {
		super();
		
		// Erstellen des JFileChoosers welchen wir fuer das auswaehlen von
		// Dateien verwenden.
		fileChooser = new JFileChooser();
		fileChooser.setAcceptAllFileFilterUsed(true);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		
		// Erstellen und konfigurieren des Knopfes welchen wir zum laden von der
		// Wortliste verwenden werden.
		JButton loadButton = new JButton();
		loadButton.setText("Laden");
		loadButton.addActionListener(this::handleLoadButtonAction);
		
		// Erstellen und konfigurieren des Knopfes welchen wir zum speichern von
		// der Wortliste verwenden werden.
		JButton saveButton = new JButton();
		saveButton.setText("Speichern");
		saveButton.addActionListener(this::handleSaveButtonAction);
		
		// Den Laden- und Speichernknopf fassen wir in diesem JPanel zusammen.
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new FlowLayout());
		topPanel.add(loadButton);
		topPanel.add(saveButton);
		
		// Erstellen des Models fuer das befuellen der JTable mit der Wortliste.
		wordModel = new DefaultTableModel(
				new String[] { "Wort" },
				0);
		
		// Wir befuellen dieses neue Model mit der Standardliste an Woertern.
		for (String defaultWord : DEFAULT_WORD_LIST) {
			wordModel.addRow(new Object[] { defaultWord });
		}
		
		// Erstellen und konfigurieren des JTable welche die Wortliste anzeigt.
		wordTable = new JTable();
		wordTable.setModel(wordModel);
		
		// JTables muessen in einer JScrollPane sein damit diese korrekt
		// angezeigt und verwendet werden koennen da sie von sich aus keine
		// Funktionalitaet zum scrollen mitbringen.
		JScrollPane tableScrollPane = new JScrollPane();
		tableScrollPane.setViewportView(wordTable);
		
		// Erstellen und konfigurieren des Knopfes welchen wir zum hinzufuegen
		// eines Worts in die Wortliste verwenden werden.
		JButton addWordButton = new JButton();
		addWordButton.setText("Wort hinzufuegen");
		addWordButton.addActionListener(this::handleAddWordButtonAction);
		
		// Erstellen und konfigurieren des Knopfes welchen wir zum entfernen des
		// aktuell selektierten Worts aus der Wortliste verwenden werden.
		JButton removeWordButton = new JButton();
		removeWordButton.setText("Wort entfernen");
		removeWordButton.addActionListener(this::handleRemoveWordButtonAction);
		
		// Den Hinzufuegen- und Entfernenknopf fassen wir in diesem JPanel
		// zusammen.
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new FlowLayout());
		bottomPanel.add(addWordButton);
		bottomPanel.add(removeWordButton);
		
		// Aufsetzen und konfigurieren des Fensters.
		setTitle("Swing Wortliste");
		setLayout(new BorderLayout());
		add(topPanel, BorderLayout.NORTH);
		add(tableScrollPane, BorderLayout.CENTER);
		add(bottomPanel, BorderLayout.SOUTH);
		
		// Da wir keine (Mindest-)Groesze gesetzt haben, muessen wir einmal
		// dafuer sorgen dass der Layout-Manager die ideale Grosze des Fensters
		// berechnet und setzt.
		pack();
	}
	
	/**
	 * Wenn {@code addWordButton} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleAddWordButtonAction(ActionEvent actionEvent) {
		// Hinzufuegen einer leeren Zeile zu dem Model.
		wordModel.addRow(new Object[] { "" });
	}
	
	/**
	 * Wenn {@code loadButton} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleLoadButtonAction(ActionEvent actionEvent) {
		// Oeffnen des "Datei oeffnen" Dialogs.
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			// Holen und umwandeln der selektierten Datei.
			File selectedFile = fileChooser.getSelectedFile();
			Path selectedFilePath = selectedFile.toPath();
			
			try {
				// Einlesen aller Zeilen aus der Datei in eine Liste.
				//
				// Hinweis: Die Methode verwendet den Zeilenumbruch der
				// aktuellen Platform.
				List<String> wordList = Files.readAllLines(selectedFilePath, StandardCharsets.UTF_8);
				
				// Wir entfernen alle Zeilen aus Model bevor wir die neuen
				// hinzufuegen.
				while (wordModel.getRowCount() > 0) {
					wordModel.removeRow(0);
				}
				
				// Hinzufuegen aller Woerter aus der Datei als einzelne Zeilen.
				for (String word : wordList) {
					wordModel.addRow(new Object[] { word });
				}
			} catch (IOException e) {
				// Ausgeben des Fehlers auf die Konsole damit wir die volle
				// Stapelspur sehen koennen.
				e.printStackTrace();
				
				// Anzeigen des Fehlers in einem Dialog.
				JOptionPane.showMessageDialog(
						this,
						e.getClass().getSimpleName() + ": " + e.getMessage(),
						e.getClass().getSimpleName(),
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	/**
	 * Wenn {@code removeWordButton} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleRemoveWordButtonAction(ActionEvent actionEvent) {
		// Hollen aller selektierten Zeilen.
		int[] selectedRows = wordTable.getSelectedRows();
		// Wir sortieren das Array da wir sichergehen muessen die letzten zuerst
		// zu loeschen.
		Arrays.sort(selectedRows);
		
		// Umgekehrtes iterieren ueber die selektierten Zeilen da wird die
		// hintersten/letzten zuerst entfernen muessen da sich durch das
		// entfernen die Zeilenanzahl veraendert.
		for (int index = selectedRows.length - 1; index >= 0; index--) {
			// Holen es Index fuer diese selektierte Zeile.
			int selectedRowIndex = selectedRows[index];
			
			// Entfernen der Zeile nach Index aus dem Model.
			wordModel.removeRow(selectedRowIndex);
		}
	}
	
	/**
	 * Wenn {@code saveButton} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleSaveButtonAction(ActionEvent actionEvent) {
		// Oeffnen des "Datei speichern" Dialogs.
		if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			// Holen und umwandeln der selektierten Datei.
			File selectedFile = fileChooser.getSelectedFile();
			Path selectedFilePath = selectedFile.toPath();
			
			// Erstellen einer neuen Liste aus Woertern.
			List<String> wordList = new ArrayList<>();
			
			// Alle Zeilen aus dem Model dieser Liste hinzufuegen.
			for (int rowIndex = 0; rowIndex < wordModel.getRowCount(); rowIndex++) {
				String word = (String)wordModel.getValueAt(rowIndex, 0);
				
				wordList.add(word);
			}
			
			try {
				// Schreiben dieser Liste als Zeilen in die Datei.
				//
				// Hinweis: Die Methode verwendet den Zeilenumbruch der
				// aktuellen Platform.
				Files.write(selectedFilePath, wordList, StandardCharsets.UTF_8);
			} catch (IOException e) {
				// Ausgeben des Fehlers auf die Konsole damit wir die volle
				// Stapelspur sehen koennen.
				e.printStackTrace();
				
				// Anzeigen des Fehlers in einem Dialog.
				JOptionPane.showMessageDialog(
						this,
						e.getClass().getSimpleName() + ": " + e.getMessage(),
						e.getClass().getSimpleName(),
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
