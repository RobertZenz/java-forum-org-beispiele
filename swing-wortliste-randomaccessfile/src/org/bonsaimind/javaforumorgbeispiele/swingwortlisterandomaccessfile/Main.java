/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingwortlisterandomaccessfile;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

/**
 * Hauptklasse welche die Hauptprogrammlogik enthaelt.
 * <p>
 * {@code final} damit sich niemand von der Klasse ableitet.
 */
public final class Main {
	/**
	 * {@code private} Konstruktor damit niemand die Klasse instanziert.
	 */
	private Main() {
	}
	
	/**
	 * Haupteinstiegpunkt des Programms.
	 * 
	 * @param args Die Argumente welche dem Programm uebergeben wurden. Werden
	 *        in diesem Fall nicht verwendet (muessen aber angegeben werden
	 *        wegen der Signatur).
	 */
	public static final void main(String[] args) {
		// Synchronisieren auf den Swing-Haupt-Thread.
		SwingUtilities.invokeLater(Main::createAndShowMainFrame);
	}
	
	/**
	 * Erstellt und zeigt das {@link MainFrame} Hauptfenster an.
	 */
	private static final void createAndShowMainFrame() {
		try {
			// Setzen des Look & Feels fuer das System.
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			// Falls der Aufruf nicht funktioniert, koennen wir das Programm
			// dennoch starten, aber es wird eben das Standard Metal LaF haben.
			// Dies ist kein Problem an sich und wir koennen den hier
			// aufgetretenen Fehler einfach ignorieren.
		}
		
		// Erstellen einer neuen Instanz des Hauptfensters.
		MainFrame mainFrame = new MainFrame();
		
		// Setzen der Operation welche passieren soll wenn das Fenster
		// geschlossen wird. Es hier zu setzen hat den Vorteil dass wir das
		// Fenster dann auch in anderen Kontexten, oder mit mehreren Instanzen,
		// verwenden koennen ohne dass das Programm dann beendet wird, oder wir
		// irgendeine gesonderte Abfrage dafuer einbauen muessen.
		//
		// Die Operation "EXIT_ON_CLOSE" bedeutet dass die JVM beendet wird wenn
		// das Fenster geschlossen wird. Dies passiert durch den Aufruf von
		// System.exit(int).
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Anzeigen des Hauptfensters.
		mainFrame.setVisible(true);
	}
}
