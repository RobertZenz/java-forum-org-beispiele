/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingwortlisterandomaccessfile;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * {@link MainFrame} ist eine {@link JFrame}-Erweiterung welche das Hauptfenster
 * implementiert.
 */
public class MainFrame extends JFrame {
	/** Die Standardliste an Woerter welche wir nach dem oeffnen anzeigen. */
	protected static final List<String> DEFAULT_WORD_LIST = Arrays.asList(
			"Kommunist",
			"Reise",
			"Klaustrophobie",
			"Steine",
			"Figuren",
			"Kaktus",
			"Sonnenschirm",
			"Zug",
			"Adam",
			"Abwickeln");
	
	/**
	 * Der {@link JFileChooser} welchen wir verwenden um Dateien fuer das
	 * Laden/Speichern auszuwaehlen.
	 */
	protected JFileChooser fileChooser = null;
	/**
	 * Das {@link DefaultTableModel} welches wir verwenden um die
	 * {@link #wordTable} zu fuellen.
	 */
	protected DefaultTableModel wordModel = null;
	/** Der {@link JTable} welcher die Wortliste anzeigt. */
	protected JTable wordTable = null;
	
	/**
	 * Erstellt eine neue {@link MainFrame} Instanz.
	 */
	public MainFrame() {
		super();
		
		// Erstellen des JFileChoosers welchen wir fuer das auswaehlen von
		// Dateien verwenden.
		fileChooser = new JFileChooser();
		fileChooser.setAcceptAllFileFilterUsed(true);
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		
		// Erstellen und konfigurieren des Knopfes welchen wir zum laden von der
		// Wortliste verwenden werden.
		JButton loadButton = new JButton();
		loadButton.setText("Laden");
		loadButton.addActionListener(this::handleLoadButtonAction);
		
		// Erstellen und konfigurieren des Knopfes welchen wir zum speichern von
		// der Wortliste verwenden werden.
		JButton saveButton = new JButton();
		saveButton.setText("Speichern");
		saveButton.addActionListener(this::handleSaveButtonAction);
		
		// Den Laden- und Speichernknopf fassen wir in diesem JPanel zusammen.
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new FlowLayout());
		topPanel.add(loadButton);
		topPanel.add(saveButton);
		
		// Erstellen des Models fuer das befuellen der JTable mit der Wortliste.
		wordModel = new DefaultTableModel(
				new String[] { "Wort" },
				0);
		
		// Wir befuellen dieses neue Model mit der Standardliste an Woertern.
		for (String defaultWord : DEFAULT_WORD_LIST) {
			wordModel.addRow(new Object[] { defaultWord });
		}
		
		// Erstellen und konfigurieren des JTable welche die Wortliste anzeigt.
		wordTable = new JTable();
		wordTable.setModel(wordModel);
		
		// JTables muessen in einer JScrollPane sein damit diese korrekt
		// angezeigt und verwendet werden koennen da sie von sich aus keine
		// Funktionalitaet zum scrollen mitbringen.
		JScrollPane tableScrollPane = new JScrollPane();
		tableScrollPane.setViewportView(wordTable);
		
		// Erstellen und konfigurieren des Knopfes welchen wir zum hinzufuegen
		// eines Worts in die Wortliste verwenden werden.
		JButton addWordButton = new JButton();
		addWordButton.setText("Wort hinzufuegen");
		addWordButton.addActionListener(this::handleAddWordButtonAction);
		
		// Erstellen und konfigurieren des Knopfes welchen wir zum entfernen des
		// aktuell selektierten Worts aus der Wortliste verwenden werden.
		JButton removeWordButton = new JButton();
		removeWordButton.setText("Wort entfernen");
		removeWordButton.addActionListener(this::handleRemoveWordButtonAction);
		
		// Den Hinzufuegen- und Entfernenknopf fassen wir in diesem JPanel
		// zusammen.
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new FlowLayout());
		bottomPanel.add(addWordButton);
		bottomPanel.add(removeWordButton);
		
		// Aufsetzen und konfigurieren des Fensters.
		setTitle("Swing Wortliste");
		setLayout(new BorderLayout());
		add(topPanel, BorderLayout.NORTH);
		add(tableScrollPane, BorderLayout.CENTER);
		add(bottomPanel, BorderLayout.SOUTH);
		
		// Da wir keine (Mindest-)Groesze gesetzt haben, muessen wir einmal
		// dafuer sorgen dass der Layout-Manager die ideale Grosze des Fensters
		// berechnet und setzt.
		pack();
	}
	
	/**
	 * Wenn {@code addWordButton} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleAddWordButtonAction(ActionEvent actionEvent) {
		// Hinzufuegen einer leeren Zeile zu dem Model.
		wordModel.addRow(new Object[] { "" });
	}
	
	/**
	 * Wenn {@code loadButton} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleLoadButtonAction(ActionEvent actionEvent) {
		// Oeffnen des "Datei oeffnen" Dialogs.
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			// Holen der selektierten Datei.
			File selectedFile = fileChooser.getSelectedFile();
			
			// Wir entfernen alle Zeilen aus Model bevor wir die neuen
			// hinzufuegen.
			while (wordModel.getRowCount() > 0) {
				wordModel.removeRow(0);
			}
			
			try (RandomAccessFile randomAccessFile = new RandomAccessFile(selectedFile, "r")) {
				// Lesen der Anzahl der Woerter in der Datei.
				int wordCount = randomAccessFile.readInt();
				
				// Jedes einzelnes Wort lesen.
				for (int count = 0; count < wordCount; count++) {
					// Lesen der Laenge des Wortes.
					int wordLength = randomAccessFile.readInt();
					// Lesen der UTF-8 Bytes des Wortes.
					byte[] wordAsUtf8Bytes = new byte[wordLength];
					randomAccessFile.read(wordAsUtf8Bytes);
					
					// Erstellen eines Strings aus den Bytes.
					String word = new String(wordAsUtf8Bytes, StandardCharsets.UTF_8);
					
					// Hinzufuegen des Wortes zum Model.
					wordModel.addRow(new Object[] { word });
				}
			} catch (IOException e) {
				// Ausgeben des Fehlers auf die Konsole damit wir die volle
				// Stapelspur sehen koennen.
				e.printStackTrace();
				
				// Anzeigen des Fehlers in einem Dialog.
				JOptionPane.showMessageDialog(
						this,
						e.getClass().getSimpleName() + ": " + e.getMessage(),
						e.getClass().getSimpleName(),
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	/**
	 * Wenn {@code removeWordButton} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleRemoveWordButtonAction(ActionEvent actionEvent) {
		// Hollen aller selektierten Zeilen.
		int[] selectedRows = wordTable.getSelectedRows();
		// Wir sortieren das Array da wir sichergehen muessen die letzten zuerst
		// zu loeschen.
		Arrays.sort(selectedRows);
		
		// Umgekehrtes iterieren ueber die selektierten Zeilen da wird die
		// hintersten/letzten zuerst entfernen muessen da sich durch das
		// entfernen die Zeilenanzahl veraendert.
		for (int index = selectedRows.length - 1; index >= 0; index--) {
			// Holen es Index fuer diese selektierte Zeile.
			int selectedRowIndex = selectedRows[index];
			
			// Entfernen der Zeile nach Index aus dem Model.
			wordModel.removeRow(selectedRowIndex);
		}
	}
	
	/**
	 * Wenn {@code saveButton} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleSaveButtonAction(ActionEvent actionEvent) {
		// Oeffnen des "Datei speichern" Dialogs.
		if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
			// Holen der selektierten Datei.
			File selectedFile = fileChooser.getSelectedFile();
			
			try (RandomAccessFile randomAccessFile = new RandomAccessFile(selectedFile, "rw")) {
				// Leeren/Abschneiden der Datei.
				randomAccessFile.setLength(0l);
				
				// Schreiben der Anzahl der Woerter.
				randomAccessFile.writeInt(wordModel.getRowCount());
				
				// Alle Zeilen aus dem Model in die Datei schreiben.
				//
				// Hinweis: Wir schreiben 4-Byte Laenge und das Wort selbst als
				// UTF-8 Bytes. In RandomAccessFile gibt es die Methode
				// writeUTF(String) welches dies ebenfalls tun wuerde,
				// allerdings schreibt diese nur eine 2-Byte (short) Laenge.
				// Damit waere die Laenge der Worte auf ~32.000 Zeichen
				// limitiert. Desweiteren gaebe es noch writeBytes(String)
				// welches ebenfalls die UTF-8 Bytes schreibt, aber da wir die
				// Laenge im vorhinein wissen muessen, koennen wir die
				// Umwandlung direkt machen.
				for (int rowIndex = 0; rowIndex < wordModel.getRowCount(); rowIndex++) {
					// Das Wort an dieser Stelle des Models.
					String word = (String)wordModel.getValueAt(rowIndex, 0);
					// Umgewandelt in UTF-8 Bytes.
					byte[] wordAsUtf8Bytes = word.getBytes(StandardCharsets.UTF_8);
					
					// Schreiben der Laenge des Wortes.
					randomAccessFile.writeInt(wordAsUtf8Bytes.length);
					// Schreiben der UTF-8 Bytes des Wortes.
					randomAccessFile.write(wordAsUtf8Bytes);
				}
			} catch (IOException e) {
				// Ausgeben des Fehlers auf die Konsole damit wir die volle
				// Stapelspur sehen koennen.
				e.printStackTrace();
				
				// Anzeigen des Fehlers in einem Dialog.
				JOptionPane.showMessageDialog(
						this,
						e.getClass().getSimpleName() + ": " + e.getMessage(),
						e.getClass().getSimpleName(),
						JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}
