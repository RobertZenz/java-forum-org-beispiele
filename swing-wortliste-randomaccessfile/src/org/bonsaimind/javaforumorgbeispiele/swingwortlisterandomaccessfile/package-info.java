/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Swing Wortliste
 * <p>
 * Demonstriert das verwalten einer einfachen Wortliste. Dabei koennen Woerter
 * hinzugefuegt, editiert und entfernt werden.
 * <p>
 * Zusaetzlich kann die gesamte Liste in einer Datei gespeichert oder auch aus
 * dieser geladen werden. Diese Datei wird mit Hilfe von
 * {@link java.io.RandomAccessFile} geschrieben und auch gelesen. Der exakte
 * Aufbau dieser Datei ist dabei:
 * 
 * <pre>
 * <code>
 * 0x00: Anzahl Woerter in Datei als int
 * 0x04: Wortlaenge int Bytes als int (nur Wort, ohne diese Laenge)
 * 0x08: Wort als UTF-8 enkodiert. 
 * 0x..: Naechstes Wort wie auf 0x04 aufgebaut.
 * </code>
 * </pre>
 */

package org.bonsaimind.javaforumorgbeispiele.swingwortlisterandomaccessfile;