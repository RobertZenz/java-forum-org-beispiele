/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.eingabensortieren;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Hauptklasse welche alle anderen Klassen und die Hauptprogrammlogik enthaelt.
 * <p>
 * {@code final} damit sich niemand von der Klasse ableitet.
 */
public final class Main {
	/**
	 * {@code private} Konstruktor damit niemand die Klasse instanziert.
	 */
	private Main() {
	}
	
	/**
	 * Haupteinstiegpunkt des Programms.
	 * 
	 * @param args Die Argumente welche dem Programm uebergeben wurden. Werden
	 *        in diesem Fall nicht verwendet (muessen aber angegeben werden
	 *        wegen der Signatur).
	 */
	public static final void main(String[] args) {
		// Instanzieren eines neuen Scanners mit welchem wir von stdin die
		// Benutzereingaben lesen werden.
		//
		// Hinweis: Scanner implementiert Closeable und daher wird die
		// Deklaration in vielen IDEs markiert da der Scanner nicht wieder
		// geschlossen wird. In unserem Beispiel spielt es aber keine Rolle, da
		// wir zum einen stdin nicht schlieszen wollen, zum anderen wir ohnehin
		// beenden wenn wir den Scanner nicht mehr brauchen.
		Scanner scanner = new Scanner(System.in);
		
		// Die Liste an Zeilen welche vom Benutzer eingegeben wurden.
		List<String> zeilen = new ArrayList<>();
		
		// Der Halter fuer die zuletzt eingebene Zeile.
		String eingabe = null;
		
		// Wir lesen die naechste Zeile vom Scanner und weisen diese direkt
		// "eingabe" zu. Wenn der zugewiesen Wert eine leere Zeile ist, brechen
		// wir das lesen ab und geben die sortierte Liste aus.
		while (!(eingabe = scanner.nextLine()).isEmpty()) {
			// Hinzufuegen der Eingabe zu der Liste an Zeilen.
			zeilen.add(eingabe);
		}
		
		// Sortieren der Liste an Zeilen.
		//
		// Hinweis: Da Generics nur beim kompilieren relevant sind, weisz die
		// ArrayList nicht welcher Typ sich zur Laufzeit in ihre befindet. Daher
		// kann die Liste die Objekte auch nicht direkt sortieren da sie
		// keinerlei Informationen ueber den Typ hat. Wir muessen daher die
		// Methode uebergeben mit welcher wir die Liste sortieren wollen.
		zeilen.sort(String::compareTo);
		
		// Fuer jede einzelne Zeile.
		for (String zeile : zeilen) {
			// Geben wir diese auf stdout aus.
			System.out.println(zeile);
		}
	}
}
