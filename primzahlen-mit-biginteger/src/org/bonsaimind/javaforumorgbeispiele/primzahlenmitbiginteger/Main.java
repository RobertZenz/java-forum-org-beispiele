/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2024, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.primzahlenmitbiginteger;

import java.math.BigInteger;

/**
 * Hauptklasse welche alle anderen Klassen und die Hauptprogrammlogik enthaelt.
 * <p>
 * {@code final} damit sich niemand von der Klasse ableitet.
 */
public final class Main {
	/**
	 * {@code private} Konstruktor damit niemand die Klasse instanziert.
	 */
	private Main() {
	}
	
	/**
	 * Haupteinstiegpunkt des Programms.
	 * 
	 * @param args Die Argumente welche dem Programm uebergeben wurden. Werden
	 *        in diesem Fall nicht verwendet (muessen aber angegeben werden
	 *        wegen der Signatur).
	 */
	public static final void main(String[] args) {
		// Die Zahl bei welcher die Suche begonnen werden soll (inklusive).
		BigInteger ersteZahl = new BigInteger("1");
		// Die Zahl bei welcher die Suche enden soll (inklusive).
		BigInteger letzteZahl = new BigInteger("1000");
		
		// Die aktuelle Zahl welche wir testen.
		BigInteger aktuelleZahl = ersteZahl;
		
		// Solange laufen bis die aktuelle Zahl groeszer ist als daie letzte
		// Zahl welche getestet werden soll.
		while (aktuelleZahl.compareTo(letzteZahl) <= 0) {
			// Pruefen ob die Zahl eine Primzahl ist.
			if (istWahrscheinlichPrimzahl(aktuelleZahl)
					&& istPrimzahl(aktuelleZahl)) {
				// Ausgeben der Zahl wenn sie eine Primzahl ist.
				System.out.println(aktuelleZahl);
			}
			
			// Hochzaehlen um 1.
			aktuelleZahl = aktuelleZahl.add(BigInteger.ONE);
		}
	}
	
	/**
	 * Prueft ob die gegebene {@link BigInteger Zahl} eine Primzahl ist.
	 * 
	 * @param zahl Die {@link BigInteger Zahl} welche geprueft werden soll.
	 * @return {@code true} wenn die gegebene {@link BigInteger Zahl} eine
	 *         Primzahl ist, ansonsten {@code false}.
	 */
	private static final boolean istPrimzahl(BigInteger zahl) {
		// Der Divisor mit welchen getestet wird. Dieser wird hochgezaehlt.
		BigInteger divisor = BigInteger.TWO;
		// Der maximale Divisor-Wert welchen wir testen muessen.
		BigInteger wurzelAusZahl = zahl.sqrt();
		
		// Solange hochzaehlen bis der Divisor das Maximum erreicht hat.
		while (divisor.compareTo(wurzelAusZahl) <= 0) {
			// Pruefen ob die Division restfrei moeglich ist, wenn ja, ist es
			// keine Primzahl.
			if (zahl.remainder(divisor).equals(BigInteger.ZERO)) {
				// Returnieren dass dies keien Primzahl ist.
				return false;
			}
			
			// Divisor um 1 erhoehen.
			divisor = divisor.add(BigInteger.ONE);
		}
		
		// Diese Zahl ist eine Primzahl.
		return true;
	}
	
	/**
	 * Fuehrt eine billige Pruefung durch ob die gegebene {@link BigInteger
	 * Zahl} eine Primzahl sein koennte.
	 * 
	 * @param zahl Die {@link BigInteger Zahl} welche geprueft werden soll.
	 * @return {@code true} wenn es <i>wahrscheinlich</i> um eine Primzahl
	 *         handelt. {@code false} wenn es <i>ganz sicher keine</i> Primzahl
	 *         ist.
	 */
	private static final boolean istWahrscheinlichPrimzahl(BigInteger zahl) {
		// Pruefen ob es wahrscheinlich eine Primzahl ist.
		return zahl.isProbablePrime(100);
	}
}
