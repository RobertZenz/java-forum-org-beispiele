/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * Swing Live Editor
 * <p>
 * Der Swing Live Editor besteht aus einem Model und einer Swing-GUI welche
 * editiert werden kann. Die Swing-GUI wird dabei aus dem Model gebaut.
 * <p>
 * Im Paket {@code .model} gibt es das Model, dabei handelt es sich um aktives
 * Model, also wenn Aenderungen passieren werden registrierte Listener
 * aufgerufen damit diese auf die Aenderungen reagieren koennen. Dies erlaubt es
 * zum Beispiel die zugehoerige GUI einfacher zu gestalten da diese nie abfragen
 * muss ob das Model sicher veraendert hat, sondern es ausreicht einen Listener
 * zu registrieren welche aufgerufen wird wenn Aenderungen stattgefunden haben.
 * <p>
 * Im Paket {@code .view} befindet sich die Swing-GUI welche die Model-Klassen
 * verwendet um eine GUI anzuzeigen und auch direkt zu editieren.
 * {@link org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.MainFrame}
 * und
 * {@link org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.MainPanel}
 * sind hierbei die Klassen welche die GUI einrahmen. Das
 * {@link org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.runtime.GuiPanel}
 * erstellt die GUI aus dem Model und
 * {@link org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.editor.GuiEditorPanel}
 * dient als "GlassPane" darueber um diese zur Laufzeit editieren zu koennen.
 */

package org.bonsaimind.javaforumorgbeispiele.swingliveeditor;