/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.base.AbstractActiveModel;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.components.Button;

/**
 * Die {@link Gui} ist eine {@link AbstractActiveModel} Erweiterung welche die
 * Oberflaeche selbst abbildet, zusammen mit den Komponenten darauf.
 * <p>
 * Im Gegensatz zum {@link Button} handelt es sich hier um ein etwas komplexeres
 * Objekt, welches aber im Groszen und Ganzen einfach nur eine Liste an Buttons
 * haelt.
 */
public class Gui extends AbstractActiveModel<Gui> {
	/** Die {@link List} von {@linkn Button}s welche hinzugefuegt wurden. */
	protected List<Button> buttons = new ArrayList<>();
	/**
	 * Ein fester Verweis auf {@link Gui#invokeChangeListeners()}. Diesen
	 * brauchen wir da wir uns selbst als Listener auf den {@link Button}s
	 * registrieren welche hinzugefuegt werden. Um diesen Listener wieder
	 * entfernen zu koennen brauchen wir einen festern Verweis auf die Methode.
	 */
	protected final Runnable CHANGE_LISTENER = this::invokeChangeListeners;
	
	/**
	 * Erstellt eine neue {@link Gui} Instanz.
	 */
	public Gui() {
		super();
	}
	
	/**
	 * Fuegt den gegebenen {@link Button} zur {@link Gui} hinzu.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @param button Der {@link Button} welcher hinzugefuegt werden soll.
	 * @return Diese {@link Gui} Instanz.
	 */
	public Gui addButton(Button button) {
		buttons.add(button);
		
		// Wir registrieren uns selbst als Listener damit wir Aenderungen im
		// Button weiterreichen koennen.
		button.registerChangeListener(CHANGE_LISTENER);
		
		invokeChangeListeners();
		
		return this;
	}
	
	/**
	 * Retourniert eine unveraenderbare {@link List} von allen {@link Button}s
	 * welche zu dieser {@link Gui} gehoeren.
	 * <p>
	 * Die Liste ist unveraenderbar, spiegelt aber den tatsaechlichen Zustand,
	 * mit allen Aenderungen, wider.
	 * <p>
	 * <i>Hinweis:</i> Wir retournieren hier eine unveraenderliche Liste damit
	 * der interne Zustand von uns nicht veraendert werden kann ohne dass wir
	 * dies mitbekommen. Wuerden wir hier direkt die interne Instanz
	 * retournieren, waere es fuer den Aufrufer moeglich {@link Button}s
	 * hinzuzufuegen ohne dass wir die Moeglichkeit haben unsere Listener zu
	 * benachrichtigen noch koennten wir uns selbst als Listener auf diesen
	 * {@link Button}s registrieren. Alternativ koennte man auch eine Kopie
	 * zurueckliefern. Eventuell koennte man auch darueber nachdenken diese eine
	 * Instanz als Feld ziwscheunzuspeichern damit man nicht immer ein neues
	 * Objekt fuer jeden Aufruf erzeugt.
	 * 
	 * @return Eine unveraenderliche {@link List} mit allen {@link Button}s.
	 */
	public List<Button> getButtons() {
		return Collections.unmodifiableList(buttons);
	}
	
	/**
	 * Entfernt den gegebenen {@link Button} von der {@link Gui}.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @param button Der {@link Button} welcher entfernt werden soll.
	 * @return Diese {@link Gui} Instanz.
	 */
	public Gui removeButton(Button button) {
		if (buttons.remove(button)) {
			// Wenn der Button erfolgreich entfernt wurde, muessen wir unseren
			// Listener ebenfalls entfernen damit wir nicht mehr bei Aenderungen
			// benachrichtigt werden.
			button.removeChangeListener(CHANGE_LISTENER);
			
			invokeChangeListeners();
		}
		
		return this;
	}
}
