/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.base;

import java.util.ArrayList;
import java.util.List;

/**
 * Das {@link AbstractActiveModel} stellt eine abstrackte Basisklasse zur
 * Verfuegung um ein katives Model zu implementieren.
 * <p>
 * Ein aktives Model ist ein Model welches in der Lage ist andere Akteure und
 * Komponnent zu benachrichtigen wenn es sich veraendert hat. Dies hat den
 * Vorteil dass man sich relativ wenig Gedanken machen muss wann man auf
 * Aenderungen reagieren muss, denn man wird immer benachrichtigt
 * beziheungsweise aufgerufen wenn es Aenderungen gibt.
 * 
 * @param <EXTENDING_TYPE> Der Typ der ableitenden Klasse.
 */
public abstract class AbstractActiveModel<EXTENDING_TYPE extends AbstractActiveModel<EXTENDING_TYPE>> {
	/**
	 * Die {@link List} an Listenern welche notifiziert werden sollen bei
	 * Aenderungen.
	 */
	protected List<Runnable> changeListeners = new ArrayList<>();
	
	/**
	 * Erstellt eine neue {@link AbstractActiveModel} Instanz.
	 * <p>
	 * <i>Hinweis:<i> Der Konstruktor ist hier {@code protected} um direkt zu
	 * signalisieren dass diese Klasse nicht instanziert werden koennen soll.
	 */
	protected AbstractActiveModel() {
		super();
	}
	
	/**
	 * Registriert einen Listener welcher notifiziert wird bei Aenderungen.
	 * 
	 * @param listener Der Listener welcher notifiziert wird bei Aenderungen.
	 * @return Diese Instanz.
	 */
	public EXTENDING_TYPE registerChangeListener(Runnable listener) {
		changeListeners.add(listener);
		
		return (EXTENDING_TYPE)this;
	}
	
	/**
	 * Entfernt den gegebenen Listener so dass dieser nicht mehr notifiziert
	 * wird.
	 * 
	 * @param listener Der Listener welcher entfernt werden soll.
	 * @return Diese Instanz.
	 */
	public EXTENDING_TYPE removeChangeListener(Runnable listener) {
		changeListeners.remove(listener);
		
		return (EXTENDING_TYPE)this;
	}
	
	/**
	 * Notifiziert alle registrierten Listener dass eine Aenderung erfolgt ist.
	 */
	protected void invokeChangeListeners() {
		System.out.format("Invoking ChangeListeners of <%s>.\n",
				getClass().getName());
		
		for (Runnable changeListener : changeListeners) {
			changeListener.run();
		}
	}
}
