/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.components;

import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.base.AbstractActiveModel;

/**
 * Der @{link Button} ist eine {@link AbstractActiveModel} Erweiterung welche
 * einen Button auf der Oberflaeche darstellt.
 * <p>
 * Dies ist ein simples POJO (Plain Old Java Object) welches einfach nur die
 * Informationen haelt welche zu einem Button gehoeren. Zusaetzlich ist es ein
 * aktives Model welches die registrierten Listener aufruft wenn sich eien der
 * Eigenschaften aendert.
 */
public class Button extends AbstractActiveModel<Button> {
	/** Die Hoehe. */
	protected int height = 30;
	/** Der Text. */
	protected String text = "Button";
	/** Die Weite. */
	protected int width = 100;
	/** Der Position in der Horizontalen. */
	protected int x = 0;
	/** Der Position in der Vertikalen. */
	protected int y = 0;
	
	/**
	 * Erstellt eine neue {@link Button} Instanz.
	 */
	public Button() {
		super();
	}
	
	/**
	 * Die Hoehe.
	 * 
	 * @return Die Hoehe.
	 */
	public int getHeight() {
		return height;
	}
	
	/**
	 * Der Text.
	 * 
	 * @return Der Text.
	 */
	public String getText() {
		return text;
	}
	
	/**
	 * Die Weite.
	 * 
	 * @return Die Weite.
	 */
	public int getWidth() {
		return width;
	}
	
	/**
	 * Die Position in der Horizontalen.
	 * 
	 * @return Die Position in der Horizontalen.
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Die Position in der Vertikalen.
	 * 
	 * @return Die Position in der Vertikalen.
	 */
	public int getY() {
		return y;
	}
	
	/**
	 * Setzt eine neue Hoehe.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @param height Die neue Hoehe.
	 * @return Diese {@link Button} Instanz.
	 */
	public Button setHeight(int height) {
		this.height = height;
		
		invokeChangeListeners();
		
		return this;
	}
	
	/**
	 * Setzt einen neuen Text.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @param text Der neue Text.
	 * @return Diese {@link Button} Instanz.
	 */
	public Button setText(String text) {
		this.text = text;
		
		invokeChangeListeners();
		
		return this;
	}
	
	/**
	 * Setzt eine neue Weite.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @param width Die neue Weite.
	 * @return Diese {@link Button} Instanz.
	 */
	public Button setWidth(int width) {
		this.width = width;
		
		invokeChangeListeners();
		
		return this;
	}
	
	/**
	 * Setzt eine neue Position in der Horizontalen.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @param x Die neue Position in der Horizontalen.
	 * @return Diese {@link Button} Instanz.
	 */
	public Button setX(int x) {
		this.x = x;
		
		invokeChangeListeners();
		
		return this;
	}
	
	/**
	 * Setzt eine neue Position in der Vertikalen.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @param y Die neue Position in der Vertikalen.
	 * @return Diese {@link Button} Instanz.
	 */
	public Button setY(int y) {
		this.y = y;
		
		invokeChangeListeners();
		
		return this;
	}
}
