/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model;

import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.base.AbstractActiveModel;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.components.Button;

/**
 * Der {@link GuiEditor} ist eine {@link AbstractActiveModel} Erweiterung welche
 * den Editor der Oberflaeche abbildet.
 */
public class GuiEditor extends AbstractActiveModel<GuiEditor> {
	/**
	 * Der horizontale Abstand vom aktuell gezogenen {@link Button} zum
	 * Mauszeiger.
	 */
	protected int dragOffsetX = 0;
	/**
	 * Der vertikale Abstand vom aktuell gezogenen {@link Button} zum
	 * Mauszeiger.
	 */
	protected int dragOffsetY = 0;
	/** Ob die Oberflaeche gerade editiert wird. */
	protected boolean enabled = false;
	/** Die {@link Gui} welche von uns editiert werden soll. */
	protected Gui gui = null;
	/** Der aktuell ausgewaehlte {@link Button}. */
	protected Button selectedButton = null;
	/**
	 * Ob der {@link #getSelectedButton() aktuell selektierte Button} gerade
	 * gezogen wird.
	 */
	protected boolean selectedButtonBeingDragged = false;
	/**
	 * Ob der {@link #getSelectedButton() aktuell selektierte Button} gerade
	 * editiert wird.
	 */
	protected boolean selectedButtonBeingEdited = false;
	
	/**
	 * Erstellt eine neue {@link GuiEditor} Instanz.
	 * 
	 * @param gui Die {@link Gui} welche editiert werden soll.
	 */
	public GuiEditor(Gui gui) {
		super();
		
		this.gui = gui;
		
		// Wir registrieren uns als Listener auf der Gui zweimal, einmal damit
		// wir unseren Zustand nachziehen wenn sich die Gui veraendert hat, und
		// einmal um selbst alle regsitrierten Listener zu notifizieren dass es
		// Aenderungen gab.
		gui.registerChangeListener(this::refreshFromModel);
		gui.registerChangeListener(this::invokeChangeListeners);
	}
	
	/**
	 * Schaltet den Editor aus und wechselt damit zum Laufzeitmodus.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @return Diese {@link GuiEditor} Instanz.
	 */
	public GuiEditor disable() {
		enabled = false;
		
		invokeChangeListeners();
		
		return this;
	}
	
	/**
	 * Zieht den {@link #getSelectedButton() aktuell selektierten Button} an die
	 * gegebenen Koordinaten unter Beruecksichtigung des Offsets zum Mauszeiger.
	 * 
	 * @param x Die neue horizontale Position.
	 * @param y Die neue vertikale Position.
	 * @return Diese {@link GuiEditor} Instanz.
	 */
	public GuiEditor dragCurrentlySelectedButtonTo(int x, int y) {
		if (selectedButton != null) {
			if (!selectedButtonBeingDragged) {
				startDraggingSelectedButton(x, y);
			}
			
			System.out.format("Dragging Button <%s> to <%d><%d>.\n",
					selectedButton.getText(),
					Integer.valueOf(x - dragOffsetX),
					Integer.valueOf(y - dragOffsetY));
			
			selectedButton.setX(x - dragOffsetX);
			selectedButton.setY(y - dragOffsetY);
		}
		
		return this;
	}
	
	/**
	 * Schaltet den Editor ein und wechselt damit zum Editiermodus.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @return Diese {@link GuiEditor} Instanz.
	 */
	public GuiEditor enable() {
		enabled = true;
		
		invokeChangeListeners();
		
		return this;
	}
	
	/**
	 * Retourniert den {@link Button} an den gegebenen Koordinaten, falls es
	 * dort einen gibt, ansonsten {@code null}.
	 * 
	 * @param x Die horizontale Koordinate.
	 * @param y Die vertikale Koordinate.
	 * @return Der {@link Button} an den gegebenen Koordinaten, {@code null}
	 *         wenn es dort keinen gibt.
	 */
	public Button getButtonAt(int x, int y) {
		for (Button button : gui.getButtons()) {
			if (x >= button.getX()
					&& x <= (button.getX() + button.getWidth())
					&& y >= button.getY()
					&& y <= (button.getY() + button.getHeight())) {
				
				System.out.format("Found Button <%s> at <%d><%d>.\n",
						button.getText(),
						Integer.valueOf(x),
						Integer.valueOf(y));
				
				return button;
			}
		}
		
		return null;
	}
	
	/**
	 * Retourniert die {@link Gui} welche editiert wird.
	 * 
	 * @return Die {@link Gui} welche editiert wird.
	 */
	public Gui getGui() {
		return gui;
	}
	
	/**
	 * Retourniert den aktuell selektierten {@link Button}.
	 * 
	 * @return Der aktuell selektierte {@link Button}.
	 */
	public Button getSelectedButton() {
		return selectedButton;
	}
	
	/**
	 * Retourniert ob die {@link #getGui() gesetzte Gui} gerade editiert wird.
	 * 
	 * @return Ob die {@link #getGui() gesetzte Gui} gerade editiert wird.
	 */
	public boolean isEnabled() {
		return enabled;
	}
	
	/**
	 * Retourniert ob der {@link #getSelectedButton() aktuell selektierte
	 * Button} gerade gezogen wird.
	 * 
	 * @return Ob der {@link #getSelectedButton() aktuell selektierte Button}
	 *         gerade gezogen wird.
	 */
	public boolean isSelectedButtonBeingDragged() {
		return selectedButtonBeingDragged;
	}
	
	/**
	 * Retourniert ob der {@link #getSelectedButton() aktuell selektierte
	 * Button} gerade editiert wird.
	 * 
	 * @return Ob der {@link #getSelectedButton() aktuell selektierte Button}
	 *         gerade editiert wird.
	 */
	public boolean isSelectedButtonBeingEdited() {
		return selectedButtonBeingEdited;
	}
	
	/**
	 * Setzt den {@link Button} welcher selektiert werden soll.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @param button Der {@link Button} welcher selektiert werden soll.
	 * @return Diese {@link GuiEditor} Instanz.
	 */
	public GuiEditor setSelectedButton(Button button) {
		// Wenn es der selbe Button ist, ersparen wir uns jegliche Aktion.
		if (selectedButton == button) {
			return this;
		}
		
		// Da sich die Selektion aendert, muessen wir auch etwaiges ziehen und
		// editieren abbrechen.
		stopDraggingOfSelectedButton();
		stopEditingOfSelectedButton();
		
		// Nur Debug-Ausgabe.
		if (button != null) {
			System.out.format("Selecting Button <%s>.\n",
					button.getText());
		} else {
			System.out.format("Unselecting.\n");
		}
		
		// Uebernehmen des Buttons.
		selectedButton = button;
		
		invokeChangeListeners();
		
		return this;
	}
	
	/**
	 * Beginnt den {@link #getSelectedButton() aktuell selektierten Button} zu
	 * ziehen.
	 * <p>
	 * Die gegebenen Koordinaten werden fuer alle weiteren Zieh-Operationen
	 * verwendet.
	 * <p>
	 * Wenn aktuell kein {@link #getSelectedButton() Button selektiert ist} wird
	 * keine Operation ausgefuehrt.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @param mouseCursorPositionX Die absolute Position der Maus in der
	 *        horizontalen, wird verwendet um den noetigen Offset fuer alle
	 *        weiteren Zieh-Operationen zu berechnen.
	 * @param mouseCursorPositionY Die absolute Position der Maus in der
	 *        vertikalen, wird verwendet um den noetigen Offset fuer alle
	 *        weiteren Zieh-Operationen zu berechnen.
	 * @return Diese {@link GuiEditor} Instanz.
	 */
	public GuiEditor startDraggingSelectedButton(int mouseCursorPositionX, int mouseCursorPositionY) {
		// Pruefen ob es einen selektieren Button gibt, wenn nicht brauchen wir
		// auch nichts zu tun.
		if (selectedButton != null) {
			// Falls der Button gerade editiert wird, muessen wir das Editieren
			// beenden.
			stopEditingOfSelectedButton();
			
			System.out.format("Starting dragging of Button <%s>.\n",
					selectedButton.getText());
			
			selectedButtonBeingDragged = true;
			// Der Offset zum Mauszeiger fuer alle zukuenftigen Operationen.
			dragOffsetX = mouseCursorPositionX - selectedButton.getX();
			dragOffsetY = mouseCursorPositionY - selectedButton.getY();
			
			invokeChangeListeners();
		}
		
		return this;
	}
	
	/**
	 * Beginnt das editieren des {@link #getSelectedButton() aktuell
	 * selektierten Button}.
	 * <p>
	 * Wenn aktuell kein {@link #getSelectedButton() Button selektiert ist} wird
	 * keine Operation ausgefuehrt.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @return Diese {@link GuiEditor} Instanz.
	 */
	public GuiEditor startEditingOfSelectedButton() {
		// Pruefen ob es einen selektieren Button gibt, wenn nicht brauchen wir
		// auch nichts zu tun.
		if (selectedButton != null) {
			// Falls der Button gerade gezogen wird, muessen wir dies beenden.
			stopDraggingOfSelectedButton();
			
			System.out.format("Starting editing of Button <%s>.\n",
					selectedButton.getText());
			
			selectedButtonBeingEdited = true;
			
			invokeChangeListeners();
		}
		
		return this;
	}
	
	/**
	 * Beendet das ziehen des {@link #getSelectedButton() aktuell selektierten
	 * Button}.
	 * <p>
	 * Wenn der {@link #getSelectedButton() aktuell selektierte Button} gerade
	 * nicht gezogen wird, wird keine Operation ausgefuehrt.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @return Diese {@link GuiEditor} Instanz.
	 */
	public GuiEditor stopDraggingOfSelectedButton() {
		// Pruefen ob der aktuell selektierte Button gezogen wird.
		if (selectedButtonBeingDragged) {
			System.out.format("Stopping dragging of Button <%s>.\n",
					selectedButton.getText());
			
			selectedButtonBeingDragged = false;
			
			invokeChangeListeners();
		}
		
		return this;
	}
	
	/**
	 * Beendet das editieren des {@link #getSelectedButton() aktuell
	 * selektierten Button}.
	 * <p>
	 * Wenn der {@link #getSelectedButton() aktuell selektierte Button} gerade
	 * nicht editiert wird, wird keine Operation ausgefuehrt.
	 * <p>
	 * Notifiziert alle registrieren Listener dass eine Aenderung stattgefunden
	 * hat.
	 * 
	 * @return Diese {@link GuiEditor} Instanz.
	 */
	public GuiEditor stopEditingOfSelectedButton() {
		// Pruefen ob der aktuell selektierte Button editiert wird.
		if (selectedButtonBeingEdited) {
			System.out.format("Stopping editing of Button <%s>.\n",
					selectedButton.getText());
			
			selectedButtonBeingEdited = false;
			
			invokeChangeListeners();
		}
		
		return this;
	}
	
	/**
	 * Wird aufgerufen wenn sich die verwendete {@link #gui} geaendert hat, und
	 * bringt den eigenen Zustand auf den letzten Stand.
	 */
	protected void refreshFromModel() {
		// Pruefen ob der aktuell selektierte Button eventuell
		// entfernt/geloescht wurde.
		if (selectedButton != null && !gui.getButtons().contains(selectedButton)) {
			selectedButton = null;
		}
	}
}
