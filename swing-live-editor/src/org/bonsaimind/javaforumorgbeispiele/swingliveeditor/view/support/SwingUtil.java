/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.support;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.swing.SwingUtilities;

/**
 * {@link SwingUtil} ist eine Hilfsklasse fuer die Interaktion mit Swing.
 * <p>
 * <i>Hinweis:</i> Die Klasse ist {code final} um ein ableiten zu verhindern, da
 * diese nur statische Hilfsmethoden bereitstellen soll.
 */
public final class SwingUtil {
	/** Die Namen der bereits wartenden {@link Runnable}s. */
	private static Set<String> enqueuedNames = Collections.synchronizedSet(new HashSet<>());
	
	/**
	 * {@code private} Konstruktor um kein instanzieren zu ermoeglichen.
	 */
	private SwingUtil() {
	}
	
	/**
	 * Fuehrt den gegebenen {@link Runnable} <b>einmal</b> "spaeter" auf dem
	 * Swing-Thread aus, egal wie oft diese Funktion mit den selben Parametern
	 * aufgerufen wird.
	 * <p>
	 * Spaeter ist in diesem Falle zu verstehen als "wenn Swing keine
	 * unmittelbaren Ereignisse/Nachrichten mehr abarbeiten muss". Dies ist
	 * meistens dann der Fall wenn es keine unmittelbare Interaktion mit der GUI
	 * mehr gibt.
	 * <p>
	 * <i>Hinweis:</i> Swing muss, wie alle GUIs, von deren Thread aus verwendet
	 * werden. Das verwenden oder veraendern von Swing-Klassen auszerhalb des
	 * Swing-Hauptthreads kann zu Fehlern und Fehlverhalten fuehren da Swing
	 * nicht Thread-Safe ist. Damit dies nicht passiert muss man sich auf den
	 * Swing-Thread synchronisieren. Da es durchaus sein kann dass es mehrere
	 * Aenderungen relativ gleichzeitig gibt, erlauben wir hier das angeben
	 * eines Namens welcher den {@link Runnable} eindeutig identifiziert. Damit
	 * koennen wir dann dafuer sorgen dass diese Aktion nur einmal von Swing
	 * ausgefuehrt wird, unabhaengig wie oft die Funktion aufgerufen wird.
	 * 
	 * @param name Der (eindeutige) Name des {@link Runnable}/der Aktion.
	 * @param runnable Der {@link Runnable}/die Aktion welche auf dem
	 *        Swing-Thread ausgefuehrt werden soll.
	 */
	public static final void invokeLater(String name, Runnable runnable) {
		// Wir pruefen ob der Name bereits in der Liste der wartenden Aktionen
		// enthalten ist, wenn Ja, brauchen wir diesen nicht noch einmal
		// einstellen.
		if (enqueuedNames.add(name)) {
			SwingUtilities.invokeLater(new NameQueueAwareRunnable(name, runnable));
		}
	}
	
	/**
	 * Der {@link NameQueueAwareRunnable} ist eine {@link Runnable}
	 * Implementierung welche den Namen vor der Ausfuehrung aus der Liste der
	 * wartenden Namen entfernt.
	 */
	private static final class NameQueueAwareRunnable implements Runnable {
		/** Der {@link Runnable}/die Aktion welche ausgefuehrt werden soll. */
		private Runnable innerRunnable = null;
		/** Der Name des {@link Runnable}/der Aktion. */
		private String name = null;
		
		/**
		 * Erstellt eine neue {@link NameQueueAwareRunnable} Instanz.
		 * 
		 * @param name Der Name des {@link Runnable}/der Aktion.
		 * @param innerRunnable Der {@link Runnable}/die Aktion welche
		 *        ausgefuehrt werden soll.
		 */
		public NameQueueAwareRunnable(String name, Runnable innerRunnable) {
			super();
			
			this.name = name;
			this.innerRunnable = innerRunnable;
		}
		
		/**
		 * {@inheritDoc}
		 */
		@Override
		public void run() {
			// Wir entfernen den Namen aus der Liste der wartenden Aktionen
			// bevor wir die Aktion ausfuehren damit ein Fehler bei der
			// Ausfuehrung nicht dazu fuehrt dass diese Aktion nie wieder
			// eingestellt werden kann.
			enqueuedNames.remove(name);
			
			// Aktion ausfuehren.
			innerRunnable.run();
		}
	}
}
