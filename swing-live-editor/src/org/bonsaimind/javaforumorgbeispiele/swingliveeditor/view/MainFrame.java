/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.Gui;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.GuiEditor;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.components.Button;

/**
 * {@link MainFrame} ist eine {@link JFrame}-Erweiterung welche das Hauptfenster
 * implementiert.
 */
public class MainFrame extends JFrame {
	/**
	 * Der {@link GuiEditor} ist das Model welches verwendet wird um den
	 * Editierungs-Modus abzubilden.
	 */
	protected GuiEditor editor = null;
	/**
	 * Der {@link Gui} ist das Model welche die Eigenschaften der Oberflaeche
	 * enthaelt.
	 */
	protected Gui gui = null;
	/**
	 * Das {@link MainPanel} kombiniert alles zu einer Laufzeit-Oberflaeche
	 * welche editiert werden kann.
	 */
	protected MainPanel mainPanel = null;
	
	/**
	 * Erstellt eine neue {@link MainFrame} Instanz.
	 */
	public MainFrame() {
		super();
		
		// Instanzieren einer neuen GUI.
		gui = new Gui();
		
		// Wir befuellen die GUI mit ein paar Buttons damit wir direkt etwas zum
		// anzeigen und editieren haben.
		gui
				.addButton(new Button().setText("Button 1"))
				.addButton(new Button().setText("Button 2").setX(250).setY(250))
				.addButton(new Button().setText("Button 3").setX(300).setY(100));
		
		// Instanzieren eines neuen Editors fuer unsere GUI.
		editor = new GuiEditor(gui);
		
		// Instanzieren und aufsetzen eines JToggleButtons welchen wir verwenden
		// zum umschalten zwischen Laufzeit- und Editier-Modus.
		JToggleButton editingToggleButton = new JToggleButton();
		editingToggleButton.setText("Editor");
		editingToggleButton.addActionListener(this::handleToggleEditorAction);
		
		// Instanzieren eines JPanels welches unseren Umschalt-Knopf enthaelt.
		JPanel controlPanel = new JPanel();
		controlPanel.setLayout(new GridBagLayout());
		controlPanel.add(editingToggleButton, createControlButtonGridBagConstraints(0));
		
		// Instanzieren eines neuen MainPanel mit unserem Editor.
		mainPanel = new MainPanel(editor);
		
		// Konfigurieren unseres Panels.
		setLayout(new BorderLayout());
		setSize(720, 480);
		setTitle("Swing Live Editor");
		
		// Hinzufuegen der Komponenten.
		add(controlPanel, BorderLayout.WEST);
		add(mainPanel, BorderLayout.CENTER);
	}
	
	/**
	 * Erstellt ein neue {@link GridBagConstraints} mit dem gegebenen
	 * {@code y}-Wert.
	 * 
	 * @param y Der {@code y}-Wert.
	 * @return Ein neue {@link GridBagConstraints} mit dem gegebenen
	 *         {@code y}-Wert.
	 */
	protected GridBagConstraints createControlButtonGridBagConstraints(int y) {
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.NORTHWEST;
		constraints.fill = GridBagConstraints.NONE;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.gridx = 0;
		constraints.gridy = y;
		constraints.insets = new Insets(8, 8, 8, 16);
		constraints.ipadx = 8;
		constraints.ipady = 8;
		constraints.weightx = 1d;
		constraints.weighty = 1d;
		
		return constraints;
	}
	
	/**
	 * Eventhandler um zwischen Laufzeit- und Editier-Modus umzuschalten.
	 * 
	 * @param actionEvent Das {@link ActionEvent} des Ereignisses.
	 */
	protected void handleToggleEditorAction(ActionEvent actionEvent) {
		if (((JToggleButton)actionEvent.getSource()).isSelected()) {
			editor.enable();
		} else {
			editor.disable();
		}
	}
}
