/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.runtime;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;

import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.Gui;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.components.Button;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.support.SwingUtil;

/**
 * Das {@link GuiPanel} ist eine {@link JPanel} Erweiterung welche die
 * Oberflaeche basierend auf einer {@link Gui} aufbaut.
 */
public class GuiPanel extends JPanel {
	/** Die bereits auf der Oberflaeche hinzugefuegten {@link JButton}s. */
	protected List<JButton> addedJButtons = new ArrayList<>();
	/** Die {@link Gui} welche verwendet wird um die Oberflaeche aufzubauen. */
	protected Gui gui = null;
	
	/**
	 * Erstelle eine neue {@link GuiPanel} Instanz mit der gegebenen {@link Gui}
	 * als Basis.
	 * 
	 * @param gui Die {@link Gui} welche verwendet wird um die Oberflaeche
	 *        aufzubauen.
	 */
	public GuiPanel(Gui gui) {
		super();
		
		this.gui = gui;
		
		// Wir gresitrieren ist auf der gegebene Gui damit wir bei Aenderuengen
		// benachrichtig werden und entsprechend die Oberflaeche auf Stand
		// bringen koennen.
		gui.registerChangeListener(this::refreshFromModel);
		
		// Wir verwenden ein null-Layout, also nur abolsute Koordinaten und
		// Positionierungen.
		setLayout(null);
	}
	
	/**
	 * Baut die Oberflaeche neu auf basierend auf der aktuellen {@link Gui}.
	 * 
	 * @return Diese {@link GuiPanel} Instanz.
	 */
	public GuiPanel refreshFromModel() {
		// Wir verwenden unser eigenes Util hier damit die Methode nur einmal
		// eingestellt wird zur Ausfuehrung.
		//
		// Desweiteren muessen wir uns fuer Aenderungen an der Swing-GUI auf den
		// Swing-Thread synchonisieren. Wenn wir vom Model benachrichtigt werden
		// koennen wir uns ohne Pruefung nicht sicher sein ob wir tatsaechlich
		// auf dem Swing-Thread sind. Wir koennten dies pruefen, aber in diesem
		// Fall lassen wir uns einfach "spaeter" wieder auf dem korrekten Thread
		// aufrufen. Dies hat auch den Vorteil dass viele Aenderungen zu nur
		// einer zusammengefasst werden.
		SwingUtil.invokeLater(
				getClass().getName() + ".rebuiltUi",
				this::rebuiltUi);
		
		return this;
	}
	
	/**
	 * Baut die GUI neu auf basierend auf der aktuellen {@link Gui}.
	 * 
	 * @return Diese {@link GuiPanel} Instanz.
	 */
	protected GuiPanel rebuiltUi() {
		// Wir holen uns einmal die Liste fuer bessere Lesbarkeit.
		List<Button> buttons = gui.getButtons();
		
		// Wir iterieren durch zwei Listen gleichzeitig. Wenn es noch keinen
		// passenden JButton fuer den Button gibt, erzeugen wir einen neuen und
		// fuegen diesen in die Liste ein.
		for (int buttonIndex = 0; buttonIndex < buttons.size(); buttonIndex++) {
			Button currentButton = buttons.get(buttonIndex);
			JButton currentJButton = null;
			
			if (addedJButtons.size() > buttonIndex) {
				// Es gibt bereits einen JButton fuer diesen Index, wir koennen
				// diesen verwenden.
				currentJButton = addedJButtons.get(buttonIndex);
			} else {
				// Es gibt noch keinen JButton, also muessen wir einen neuen
				// erzeugen.
				currentJButton = new JButton();
				// Wir fuegen hier eine Dummy-Operation ein damit die Buttons
				// zur Laufzeit auch etwas tun. Diese Operation kann man auch
				// durch eine vom Model vergebene ersetzen. Oder besser, man
				// sollte.
				currentJButton.addActionListener((actionEvent) -> {
					System.out.println("Clicked \"" + ((JButton)actionEvent.getSource()).getText() + "\".");
				});
				
				add(currentJButton);
				
				addedJButtons.add(currentJButton);
			}
			
			// Alle Eigenschaften vom Button auf den JButton uebernehmen.
			currentJButton.setLocation(currentButton.getX(), currentButton.getY());
			currentJButton.setText(currentButton.getText());
			currentJButton.setSize(currentButton.getWidth(), currentButton.getHeight());
		}
		
		// Falls wir zu viele JButtons haben (weil zum Beispiel ein Button
		// geloescht wurde), dann muessen wir diese natuerlich sowohl aus der
		// Liste als auch aus der GUI entfernen.
		while (addedJButtons.size() > buttons.size()) {
			JButton removedButton = addedJButtons.remove(addedJButtons.size() - 1);
			
			remove(removedButton);
		}
		
		return this;
	}
}
