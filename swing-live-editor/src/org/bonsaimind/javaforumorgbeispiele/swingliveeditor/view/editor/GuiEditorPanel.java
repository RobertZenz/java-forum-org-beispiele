/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.editor;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.event.ActionEvent;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JTextField;

import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.GuiEditor;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.components.Button;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.editor.listeners.ButtonDraggingMouseListener;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.editor.listeners.ButtonDraggingMouseMotionListener;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.editor.listeners.ButtonEditingMouseListener;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.editor.listeners.ButtonSelectingMouseListener;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.editor.listeners.EnterEscapeKeyListener;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.editor.listeners.PopupShowingMouseListener;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.support.SwingUtil;

/**
 * Das {@link GuiEditorPanel} ist eine {@link JPanel} Erweiterung welchedie
 * Oberflaeche zu einem {@link GuiEditor} bereitstellt.
 * <p>
 * Dieses {@link JPanel} wird als "GlassPane" verwendet, also ist durchsichtig
 * und kann auch Ereignisse an die darunter liegenden Komponenent weiterreichen.
 * 
 * @see <a href=
 *      "https://docs.oracle.com/javase/tutorial/uiswing/components/rootpane.html">Oracle.com:
 *      How to Use Root Panes</a>
 */
public class GuiEditorPanel extends JPanel {
	/** Die {@link Color} welche fuer den Selektions-Rahmen verwendet wird. */
	private static final Color SELECTION_RECTANGLE_COLOR = Color.GREEN;
	/** Die Dicke welche fuer den Selektions-Rahmen verwendet wird. */
	private static final int SELECTION_RECTANGLE_THICKNESS = 3;
	
	/**
	 * Das {@link JMenuItem} im {@link #popupMenu} welches einen neuen
	 * {@link Button} einfuegt.
	 */
	protected JMenuItem addNewButtonMenuItem = null;
	/**
	 * Das {@link JMenuItem} im {@link #popupMenu} welches den
	 * {@link GuiEditor#getSelectedButton() aktuell selektieren Button}
	 * editiert.
	 */
	protected JMenuItem editSelectedButtonMenuItem = null;
	/** Der zu Grunde liegende {@link GuiEditor}. */
	protected GuiEditor guiEditor = null;
	/** Das {@link JPopupMenu} welches diverse Operationen bereitstellt. */
	protected JPopupMenu popupMenu = null;
	/**
	 * Das {@link JMenuItem} im {@link #popupMenu} welches den
	 * {@link GuiEditor#getSelectedButton() aktuell selektieren Button}
	 * entfernt.
	 */
	protected JMenuItem removeSelectedButtonMenuItem = null;
	/**
	 * Das {@link JTextField} welches ueber den
	 * {@link GuiEditor#getSelectedButton() aktuell selektieren Button} gelegt
	 * wird wenn dieser editiert wird.
	 */
	protected JTextField selectedButtonEditTextField = null;
	
	/**
	 * Erstelle eine neue {@link GuiEditorPanel} Instanz mit dem gegebenen
	 * {@link GuiEditor} als Basis.
	 * 
	 * @param guiEditor Der {@link GuiEditor} welche fuer das editieren
	 *        verwendet wird.
	 */
	public GuiEditorPanel(GuiEditor guiEditor) {
		super();
		
		this.guiEditor = guiEditor;
		// Wir registrieren und auf dem Gui-Editor als Listener damit wir ueber
		// Aenderungen notifiziert werden.
		this.guiEditor.registerChangeListener(this::refreshFromModel);
		
		// Allgemeines aufsetzen damit wir als GlassPane fungieren koennen.
		setBackground(null);
		setOpaque(false);
		setVisible(this.guiEditor.isEnabled());
		
		// Erstellen des JTextFields fuer das editieren von Buttons.
		selectedButtonEditTextField = new JTextField();
		// Registrieren eines neuen KeyListeners welcher es uns erlaubt das
		// editieren mit Tasten zu beenden.
		selectedButtonEditTextField.addKeyListener(new EnterEscapeKeyListener(
				this::commitEditingOfSelectedButton,
				guiEditor::stopEditingOfSelectedButton));
		
		// Erstellen des Popup-Menues.
		
		editSelectedButtonMenuItem = new JMenuItem();
		editSelectedButtonMenuItem.setText("Edit Button");
		editSelectedButtonMenuItem.addActionListener(this::handleEditSelectedButtonAction);
		
		removeSelectedButtonMenuItem = new JMenuItem();
		removeSelectedButtonMenuItem.setText("Remove Button");
		removeSelectedButtonMenuItem.addActionListener(this::handleRemoveSelectedButtonAction);
		
		addNewButtonMenuItem = new JMenuItem();
		addNewButtonMenuItem.setText("Add New Button");
		addNewButtonMenuItem.addActionListener(this::handleAddNewButtonAction);
		
		popupMenu = new JPopupMenu();
		popupMenu.add(editSelectedButtonMenuItem);
		popupMenu.add(removeSelectedButtonMenuItem);
		popupMenu.add(new JSeparator());
		popupMenu.add(addNewButtonMenuItem);
		
		// MouselIstener fuer das selektieren von Buttons.
		addMouseListener(new ButtonSelectingMouseListener(guiEditor));
		// MouseListener welcher es erlaubt per Doppelklick einen Button zu
		// editieren.
		addMouseListener(new ButtonEditingMouseListener(guiEditor));
		// MouseListener welcher es erlaubt einen Button zu ziehen.
		addMouseListener(new ButtonDraggingMouseListener(guiEditor));
		// Registrieren eines eigenen Listeners um das Popup-Menue anzuzeigen.
		// Wir koennten dies auch ueber setComponentPopupMenu(JPopupMenu)
		// machen, dann wird aber bei einem Rechtsklick nicht der Button unter
		// der Maus selektiert.
		addMouseListener(new PopupShowingMouseListener(popupMenu, this));
		// MouseMotionListener welcher es erlaubt einen Button zu ziehen.
		addMouseMotionListener(new ButtonDraggingMouseMotionListener(guiEditor));
	}
	
	/**
	 * Baut die Oberflaeche neu auf basierend auf dem aktuellen
	 * {@link GuiEditor}.
	 * 
	 * @return Diese {@link GuiEditorPanel} Instanz.
	 */
	public GuiEditorPanel refreshFromModel() {
		// Wir verwenden unser eigenes Util hier damit die Methode nur einmal
		// eingestellt wird zur Ausfuehrung.
		//
		// Desweiteren muessen wir uns fuer Aenderungen an der Swing-GUI auf den
		// Swing-Thread synchonisieren. Wenn wir vom Model benachrichtigt werden
		// koennen wir uns ohne Pruefung nicht sicher sein ob wir tatsaechlich
		// auf dem Swing-Thread sind. Wir koennten dies pruefen, aber in diesem
		// Fall lassen wir uns einfach "spaeter" wieder auf dem korrekten Thread
		// aufrufen. Dies hat auch den Vorteil dass viele Aenderungen zu nur
		// einer zusammengefasst werden.
		SwingUtil.invokeLater(
				getClass().getName() + ".rebuiltUi",
				this::refreshUi);
		
		return this;
	}
	
	/**
	 * Uebernimmt den neuen Text auf den {@link GuiEditor#getSelectedButton()
	 * aktuell selektieren Button}.
	 */
	protected void commitEditingOfSelectedButton() {
		// Nur wenn der Button bereits editiert wird.
		if (guiEditor.isSelectedButtonBeingEdited()) {
			// Uebernehmen den Text auf den Button.
			guiEditor.getSelectedButton().setText(selectedButtonEditTextField.getText());
			
			// Beenden des editierens.
			guiEditor.stopEditingOfSelectedButton();
		}
	}
	
	/**
	 * Zeichnet ein Rechteck um den angegebenen {@link Button}.
	 * 
	 * @param button Der {@link Button} um welchen das Rechteck gezeichnet
	 *        werden soll.
	 * @param graphics Die {@link Graphics} auf welchem das Recht gezeichen
	 *        werden soll.
	 */
	protected void drawSelectionRectangleAround(Button button, Graphics graphics) {
		// Setzen der Farbe welche wir gleich verwenden werden.
		graphics.setColor(SELECTION_RECTANGLE_COLOR);
		// Zeichnen des Rahmens als vier Rechtecke rund um den Button.
		graphics.fillRect(
				button.getX() - SELECTION_RECTANGLE_THICKNESS,
				button.getY() - SELECTION_RECTANGLE_THICKNESS,
				button.getWidth() + (SELECTION_RECTANGLE_THICKNESS * 2),
				SELECTION_RECTANGLE_THICKNESS);
		graphics.fillRect(
				button.getX() - SELECTION_RECTANGLE_THICKNESS,
				button.getY() - SELECTION_RECTANGLE_THICKNESS,
				SELECTION_RECTANGLE_THICKNESS,
				button.getHeight() + (SELECTION_RECTANGLE_THICKNESS * 2));
		graphics.fillRect(
				button.getX() - SELECTION_RECTANGLE_THICKNESS,
				button.getY() + button.getHeight(),
				button.getWidth() + (SELECTION_RECTANGLE_THICKNESS * 2),
				SELECTION_RECTANGLE_THICKNESS);
		graphics.fillRect(
				button.getX() + button.getWidth(),
				button.getY() - SELECTION_RECTANGLE_THICKNESS,
				SELECTION_RECTANGLE_THICKNESS,
				button.getHeight() + (SELECTION_RECTANGLE_THICKNESS * 2));
	}
	
	/**
	 * Wenn {@link #addNewButtonMenuItem} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleAddNewButtonAction(ActionEvent actionEvent) {
		Button newButton = new Button()
				.setText("New Button");
		newButton.setX(getMousePosition().x - (newButton.getWidth() / 2));
		newButton.setY(getMousePosition().y - (newButton.getHeight() / 2));
		
		guiEditor.getGui().addButton(newButton);
	}
	
	/**
	 * Wenn {@link #editSelectedButtonMenuItem} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleEditSelectedButtonAction(ActionEvent actionEvent) {
		// Nur wenn es einen selektierten Button gibt.
		if (guiEditor.getSelectedButton() != null) {
			guiEditor.startEditingOfSelectedButton();
		}
	}
	
	/**
	 * Wenn {@link #removeSelectedButtonMenuItem} geklickt wird.
	 * 
	 * @param actionEvent Das {@link ActionEvent} fuer das Ereignis.
	 */
	protected void handleRemoveSelectedButtonAction(ActionEvent actionEvent) {
		// Nur wenn es einen selektierten Button gibt.
		if (guiEditor.getSelectedButton() != null) {
			guiEditor.getGui().removeButton(guiEditor.getSelectedButton());
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void paintComponent(Graphics graphics) {
		// Originale Zeichenmethode aufrufen.
		super.paintComponent(graphics);
		
		// Wenn es einen selektierten Button gibt, dann zeichnen wir einen
		// Rahmen um diesen.
		if (guiEditor.getSelectedButton() != null) {
			drawSelectionRectangleAround(guiEditor.getSelectedButton(), graphics);
		}
	}
	
	/**
	 * Aktualisiert die GUI basierend auf dem Zustand des aktuellen
	 * {@link GuiEditor}.
	 * 
	 * @return Diese {@link GuiEditorPanel} Instanz.
	 */
	protected GuiEditorPanel refreshUi() {
		// Wenn wir nicht editieren, muessen wir dieses Panel auf unsichtbar
		// schalten.
		if (guiEditor.isEnabled()) {
			setVisible(true);
		} else {
			setVisible(false);
		}
		
		// Ein/Abschalten der Popup-MenuItems.
		if (guiEditor.getSelectedButton() != null) {
			editSelectedButtonMenuItem.setEnabled(true);
			removeSelectedButtonMenuItem.setEnabled(true);
		} else {
			editSelectedButtonMenuItem.setEnabled(false);
			removeSelectedButtonMenuItem.setEnabled(false);
		}
		
		// Aendern des Cursors falls wir gerade einen Button ziehen.
		if (guiEditor.isSelectedButtonBeingDragged()) {
			setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
		} else {
			setCursor(Cursor.getDefaultCursor());
		}
		
		// Wenn der aktuelle Button gerade editiert wird.
		if (guiEditor.isSelectedButtonBeingEdited()) {
			// Falls das TextField noch nicht angezeigt wird.
			if (selectedButtonEditTextField.getParent() == null) {
				// Hinzufuegen des TextFields.
				add(selectedButtonEditTextField);
				
				// Direkt ueber den Button setzen.
				selectedButtonEditTextField.setLocation(
						guiEditor.getSelectedButton().getX(),
						guiEditor.getSelectedButton().getY());
				selectedButtonEditTextField.setSize(
						guiEditor.getSelectedButton().getWidth(),
						guiEditor.getSelectedButton().getHeight());
				selectedButtonEditTextField.setText(guiEditor.getSelectedButton().getText());
				
				// Fokus setzen.
				selectedButtonEditTextField.requestFocus();
				selectedButtonEditTextField.selectAll();
			}
		} else {
			// Falls wir gerade nicht einen Button editieren, entfernen wir das
			// TextField auf jeden Fall da wir es nicht auf der Oberflaeche
			// wollen.
			remove(selectedButtonEditTextField);
		}
		
		// Erzwingen dass die Komponente neu gezeichnet wird.
		invalidate();
		repaint();
		
		return this;
	}
}
