/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.editor.listeners;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Der {@link EnterEscapeKeyListener} ist eine {@link KeyListener}
 * Implementierung welche Aktionen bei Enter und Escape aufruft.
 */
public class EnterEscapeKeyListener implements KeyListener {
	/** Die {@link Runnable Aktion} welche bei Enter aufgerufen wird. */
	protected Runnable enterAction = null;
	/** Die {@link Runnable Aktion} welche bei Escape aufgerufen wird. */
	protected Runnable escapeAction = null;
	
	/**
	 * Erstellt eine neue {@link EnterEscapeKeyListener} Instanz.
	 * 
	 * @param enterAction Die {@link Runnable Aktion} welche bei Enter
	 *        aufgerufen wird.
	 * @param escapeAction Die {@link Runnable Aktion} welche bei Escape
	 *        aufgerufen wird.
	 */
	public EnterEscapeKeyListener(Runnable enterAction, Runnable escapeAction) {
		super();
		
		this.enterAction = enterAction;
		this.escapeAction = escapeAction;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void keyPressed(KeyEvent keyEvent) {
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void keyReleased(KeyEvent keyEvent) {
		switch (keyEvent.getKeyCode()) {
			case KeyEvent.VK_ENTER:
				enterAction.run();
				break;
			
			case KeyEvent.VK_ESCAPE:
				escapeAction.run();
				break;
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void keyTyped(KeyEvent keyEvent) {
	}
}