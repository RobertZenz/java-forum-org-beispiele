/*
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.editor.listeners;

import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.GuiEditor;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.components.Button;

/**
 * Der {@link ButtonDraggingMouseMotionListener} ist eine
 * {@link MouseMotionListener} Implementierung welche einen {@link Button}
 * zieht.
 */
public class ButtonDraggingMouseMotionListener implements MouseMotionListener {
	/** Der {@link GuiEditor} welcher verwendet wird. */
	protected GuiEditor editor = null;
	
	/**
	 * Erstellt eine neue {@link ButtonDraggingMouseMotionListener} Instanz.
	 * 
	 * @param editor Der {@link GuiEditor} welcher verwendet wird.
	 */
	public ButtonDraggingMouseMotionListener(GuiEditor editor) {
		super();
		
		this.editor = editor;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mouseDragged(MouseEvent mouseEvent) {
		if (editor.getSelectedButton() != null) {
			editor.dragCurrentlySelectedButtonTo(mouseEvent.getX(), mouseEvent.getY());
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void mouseMoved(MouseEvent mouseEvent) {
	}
}
