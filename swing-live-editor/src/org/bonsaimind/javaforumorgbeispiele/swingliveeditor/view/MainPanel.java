/**
 * BSD 2-Clause License
 * 
 * Copyright (c) 2023, Robert 'Bobby' Zenz
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.model.GuiEditor;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.editor.GuiEditorPanel;
import org.bonsaimind.javaforumorgbeispiele.swingliveeditor.view.runtime.GuiPanel;

/**
 * Das {@link MainPanel} ist eine {@link JPanel} Ableitung welche ein
 * {@link GuiEditorPanel} und ein {@link GuiPanel} kombiniert.
 */
public class MainPanel extends JPanel {
	/**
	 * Instanziert eine neue {@link MainPanel} Instanz.
	 * 
	 * @param guiEditor The {@link GuiEditor} welcher verwendet wird.
	 */
	public MainPanel(GuiEditor guiEditor) {
		super();
		
		// Erstellen einer neuen GuiEditorPanel Instanz.
		GuiEditorPanel guiEditorPanel = new GuiEditorPanel(guiEditor);
		
		// Erstellen einer neuen GuiPanel Instanz.
		GuiPanel guiPanel = new GuiPanel(guiEditor.getGui());
		guiPanel.refreshFromModel();
		
		// Zusammenfuegen der beiden Panels.
		setLayout(new GridBagLayout());
		add(guiEditorPanel, createComponentFillingConstraints());
		add(guiPanel, createComponentFillingConstraints());
	}
	
	/**
	 * Erstellt {@link GridBagConstraints} welche die gesamte Komponente
	 * fuellen.
	 * 
	 * @return {@link GridBagConstraints} welche die gesamte Komponente fuellen.
	 */
	protected GridBagConstraints createComponentFillingConstraints() {
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.insets = new Insets(0, 0, 0, 0);
		constraints.ipadx = 0;
		constraints.ipady = 0;
		constraints.weightx = 1d;
		constraints.weighty = 1d;
		
		return constraints;
	}
}
